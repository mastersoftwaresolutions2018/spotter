<?php

//namespace RestApi\Controller;
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
//use RestApi\Controller\ApiController;
use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;

class CustomerApiController extends AppController
{
    
    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Auth');
        $this->Auth->allow(['login','validateToken','getBooks','getBookDetails','searchBook','addReview','removeReview','editReview']);

    }

    public function validateLogin($data=null)
    {
    	$error = array();
        if (!isset($data['username']) || empty($data['username'])) {
            $error['messageText'] = "Please enter your username.";
            $error['messageCode'] = 201;
            $error['successCode'] = 0;
        }elseif (!isset($data['password']) || empty($data['password'])) {
            $error['messageText'] = "Please enter User password.";
            $error['messageCode'] = 202;
            $error['successCode'] = 0;
        }else {
        	/*$userTable = TableRegistry::get('Users');
        	$user      = $userTable->find()->where(['username' => trim($data['username']), 'password' => trim($data['password']), 'role_id' => 2])->count();
        	if($user){*/	
            	$error['messageCode'] = 200;
        	/*}else{
        		$error['messageText'] = "Wrong Username or Password.";
	            $error['messageCode'] = 203;
	            $error['successCode'] = 0;
        	}*/
        }
        return $error;
    }
    public function login()
    {
    	$error = array();
    	if ($this->request->is('post')) {
            $data = $this->request->getData();            
        	$error = $this->validateLogin($data);
            if($error['messageCode']==200){            	
		        $user 	= $this->Auth->identify();
		        $userId = $user['id'];		        
			    $token1 = \Cake\Utility\Text::uuid();
		        if ($user) {
		            $tokenTable = TableRegistry::get('AccessTokens');
		            $token  	= $tokenTable->find()->where(['user_id'=> $userId])->hydrate(false)->toArray();
		            if(count($token)){
		            	$modified  	= date('Y-m-d H:i:s');
				        $query     	= $tokenTable->query();
				        $result1   	= $query->update()->set(['access_token' => $token1,'created'=> $modified])->where(['user_id' => $userId]) ->execute();

				        if ($result1) {
                            $error['status']            = true;
                            $error['result']            = "You have logged in successfully";
                            $error['statusDescription'] = "";
				            $error['token'] 		= $token1;
				            $error['userinfo']		= $user;
			            }else {
                            $error['status']    = false;
				            $error['result'] 	= null;
				            $error['statusDescription'] 	= "Some error occured.";
				        }
		            }else{
		            	$modified  				= date('Y-m-d H:i:s');
		            	$newtoken              	= $tokenTable->newEntity();
			            $newtoken->user_id 		= $userId;
			            $newtoken->access_token = $token1;
			            $newtoken->created 		= $modified;
			            $result1                = $tokenTable->save($newtoken);

			            if ($result1->id) {
			            	$error['status']            = true;
                            $error['result']            = "You have logged in successfully";
                            $error['statusDescription'] = "";
			            }else {
                            $error['status']    = false;
                            $error['result']    = null;
                            $error['statusDescription']     = "Some error occured. Please try again.";
				        }
		            }			            
		        }else {
                    $error['status']    = false;
                    $error['result']    = null;
                    $error['statusDescription']     = "Invalid username or password.";
		        }
            }            
        }else{
            $error['status']    = false;
            $error['result']    = null;
            $error['statusDescription']     = "Please enter required information.";
        }
        echo json_encode($error); die;
        
    }
    public function validateData($data=null)
    {
        $error = array();
        if (!isset($data['user_id']) || empty($data['user_id'])) {
            $error['messageText'] = "Please enter User Id.";
            $error['messageCode'] = 206;
            $error['successCode'] = 0;
        }elseif (!isset($data['token']) || empty($data['token'])) {
            $error['messageText'] = "Please enter token.";
            $error['messageCode'] = 205;
            $error['successCode'] = 0;
        }else {
            /*$userTable = TableRegistry::get('Users');
            $user      = $userTable->find()->where(['username' => trim($data['username']), 'password' => trim($data['password']), 'role_id' => 2])->count();
            if($user){*/    
                $error['messageCode'] = 200;
                $error['messageText'] = "Success";
            /*}else{
                $error['messageText'] = "Wrong Username or Password.";
                $error['messageCode'] = 203;
                $error['successCode'] = 0;
            }*/
        }
        return $error;
    }
    public function validateToken($data = null)
    {
    	$result = array();
        $json   = false;
    	if ($this->request->is('post')) {
            if(!@$data){
                $data       = $this->request->getData();
                $json = true;
            }
            
            $response   = $this->validateData($data);
            if($response['messageCode'] == 200){
                $tokenTable = TableRegistry::get('AccessTokens');
    		    $token  	= $tokenTable->find()->where(['user_id'=> $data['user_id'],'access_token'=> $data['token']])->hydrate(false)->first(); 
    		    if(count($token)){
    		    	$numMinutes = 60;
    	            $time = new FrozenTime($token['created']);
    	            $time = $time->modify ("+{$numMinutes} minutes");
    	            $time = $time->format('Y-m-d H:i:s');
    	            $now  = date('Y-m-d H:i:s');
    	            if ( $now > $time ) {
    	            	$result['messageText'] 	= "Invalid token.";
    		    		$result['messageCode'] 	= 201;
    	            }else{
    	            	$result['messageText'] 	= "Valid token.";
    		    		$result['messageCode'] 	= 200;
    	            }
    		    }else{		    	
    	            	$result['messageText'] 	= "Invalid Information.";
    		    		$result['messageCode'] 	= 202;
    		    }
            }else{
                $result['messageText']  = $response['messageText'];
                $result['messageCode']  = $response['messageCode'];
            }	
            if($json){
                echo json_encode($result); die;
            }else{
               return $result; 
            }	    
		    
        }else{
        	if($json){
                echo json_encode($result); die;
            }else{
               return $result; 
            }
        }
    }
    public function getBooks()
    {   
        $response   = array();
    	if ($this->request->is('post')) {
        	$data 		= $this->request->getData();
        	$bookTable 	= TableRegistry::get('Books');
        	if(@$data['page'] && !empty(@$data['page']) && @$data['limit'] && !empty(@$data['limit']) ){
        		$books  	= $bookTable->find()->select(['id','name'])->limit(@$data['limit'])->page(@$data['page'])->hydrate(false)->toArray();
            	$this->apiResponse['page'] = @$data['page'];
            	$this->apiResponse['rows'] = count($books);
        	}else{
        		$books  	= $bookTable->find()->select(['id','name'])->hydrate(false)->toArray();
        	}    	
            
    	// Set the response
            if(count($books) < 1){
            	$response['messageCode']   = 208;
            	$response['books']         = "No book found";
            }else{
            	$response['messageCode']   = 200;
            	$response['books']         = $books;
            } 
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        }
        echo json_encode($response); die;    	       
    }
    public function getBookDetails($value='')
    {
        $response = array();
        if ($this->request->is('post')) {
        	$data 		= $this->request->getData();
        	$bookTable 	= TableRegistry::get('Books');
        	if(@$data['id'] && !empty(@$data['id'])){
        		$books  	= $bookTable->find()->where(['Books.id'=>@$data['id']])->contain(['categories'])->hydrate(false)->toArray();
        		if(count($books)){
    	        	$response['messageCode'] = 200;
    	        	$response['books']       = $books;
        		}else{
        			$response['messageCode'] = 208;
        			$response['books']       = "No book found";
        		}
        	}else{
        		$response['messageCode']  = 201;
        		$response['books']        = "Please enter book id.";
        	}
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        }    
        echo json_encode($response); die; 	
    }
    public function searchBook()
    {
        $response = array();
    	if ($this->request->is('post')) {
        	$data 		= $this->request->getData();
        	$bookTable 	= TableRegistry::get('Books');
        	$condition 	= array();
        	if(!empty(@$data)){
        		$condition[] = [
                            'AND' => [
                                'Books.name LIKE' => trim(@$data['name']) . '%',
                                'Books.author_name LIKE' => trim(@$data['author_name']) . '%',
                                'Books.publisher_name LIKE' => trim(@$data['publisher_name']) . '%',
                                'Books.is_deleted'=> 0
                            ],
                        ];
    	    	if(@$data['category_id'] && !empty($data['category_id'])){
    	    		$condition[] = [
    	                        'AND' => [                            
    	                            'Books.category_id' => trim(@$data['category_id']),
    	                        ],
    	                    ];
    	    	}
    	    	$books  	= $bookTable->find()->where($condition)->hydrate(false)->toArray();
    	    	if(count($books)){
    	    		$response['messageCode'] = 200;
    		    	$response['books']       = $books;
    	    	}else{
    	    		$response['messageCode'] = 208;
    	    		$response['books']       = "No book found related to your search.";
    	    	}
        	}else{
        		$response['messageCode']  = 201;
    	    	$response['books']        = "No input entered.";
        	}
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        }   
        echo json_encode($response); die; 
    }
    private function checkReviewData($data= null)
    {
        $error = array();
        if (!isset($data['book_id']) || empty($data['book_id'])) {
            $error['messageText'] = "Please enter Book Id.";
            $error['messageCode'] = 204;
            $error['successCode'] = 0;
        }elseif (!isset($data['rating']) || empty($data['rating'])) {
            $error['messageText'] = "Please enter rating.";
            $error['messageCode'] = 205;
            $error['successCode'] = 0;
        }elseif ( !is_numeric($data['rating']) ) {
            $error['messageText'] = "Please enter rating in numeric format.";
            $error['messageCode'] = 205;
            $error['successCode'] = 0;
        }elseif ( $data['rating'] > 7 ) {
            $error['messageText'] = "Please enter rating from 1 to 7.";
            $error['messageCode'] = 205;
            $error['successCode'] = 0;
        }elseif (!isset($data['description']) || empty($data['description'])) {
            $error['messageText'] = "Please enter review description.";
            $error['messageCode'] = 206;
            $error['successCode'] = 0;
        }else {
            $bookTable = TableRegistry::get('Books');
            $book      = $bookTable->find()->where(['id' => trim($data['book_id'])])->count();
            if($book){    
                $error['messageCode'] = 200;
                $error['messageText'] = "Success";
            }else{
                $error['messageText'] = "Book does not exist.";
                $error['messageCode'] = 207;
                $error['successCode'] = 0;
            }
        }
        return $error;
    }
    public function addReview()
    {
        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $validateUser   = $this->validateToken($data);
            if($validateUser['messageCode'] == 200){
                $isDataValid    = $this->checkReviewData($data);
                if($isDataValid['messageCode'] == 200){
                    $reviewTable        = TableRegistry::get('Reviews');
                    $review             = $reviewTable->newEntity();
                    $review->user_id    = $data['user_id'];
                    $review->book_id    = $data['book_id'];
                    $review->rating     = $data['rating'];
                    $review->description= $data['description'];
                    $result1            = $reviewTable->save($review);
                    if ($result1->id) {
                        $response['messageCode']    = 200;
                        $response['successMessage'] = "Review added successfully.";
                        $this->apiResponse['reviewId']      = $result1->id;
                    }else{
                        $response['messageCode']    = 208;
                        $response['successMessage'] = "Some error occured.";
                    }
                    
                }else{
                    $response['messageCode']    = $isDataValid['messageCode'];
                    $response['error'] = $isDataValid['messageText'];
                }
            }else{
                $response['messageCode']    = 201;
                $response['error']          = "Invalid User information.";
            }
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        }   
        echo json_encode($response); die;     
    }
    public function removeReview()
    {
        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $validateUser   = $this->validateToken($data);
            if($validateUser['messageCode'] == 200){
                $reviewTable    = TableRegistry::get('Reviews');
                $review         = $reviewTable->get($data['review_id']);
                if($review->user_id == $data['user_id']){
                    $review->is_deleted = 1;
                    if($reviewTable->save($review)){
                        $response['messageCode']    = 200;
                        $response['successMessage'] = "Review deleted successfully.";
                        $response['reviewId']       = $review->id;
                    }else{
                        $response['messageCode']    = 208;
                        $response['successMessage'] = "Some error occured.";
                    }
                }else{
                    $response['messageCode']= 204;
                    $response['error']      = "You are not authorized to delete this review.";  
                }
            }else{
                $response['messageCode']    = 201;
                $response['error']          = "Invalid User information.";
            } 
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        }   
        echo json_encode($response); die;    
    }


    public function editReview($value='')
    {
        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $validateUser   = $this->validateToken($data);
            if($validateUser['messageCode'] == 200){
                $review     = $reviewTable->get($data['review_id']);
                $review     = $reviewTable->patchEntity($review, $this->request->getData());

                if ($reviewTable->save($review)) {
                    $response['messageCode']    = 200;
                    $response['successMessage'] = "Review updated successfully.";
                    $response['reviewId']       = $review->id;
                }
            }else{
                $response['messageCode']= 201;
                $response['error']      = "Invalid User information.";
            }
        }else{
            $response['messageCode']   = 201;
            $response['books']         = "Invalid request";
        } 
        echo json_encode($response); die; 
    }
}