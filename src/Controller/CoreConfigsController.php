<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CoreConfigs Controller
 *
 * @property \App\Model\Table\CoreConfigsTable $CoreConfigs
 *
 * @method \App\Model\Entity\CoreConfig[] paginate($object = null, array $settings = [])
 */
class CoreConfigsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Configs']
        ];
        $coreConfigs = $this->paginate($this->CoreConfigs);

        $this->set(compact('coreConfigs'));
        $this->set('_serialize', ['coreConfigs']);
    }

    /**
     * View method
     *
     * @param string|null $id Core Config id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $coreConfig = $this->CoreConfigs->get($id, [
            'contain' => ['Configs']
        ]);

        $this->set('coreConfig', $coreConfig);
        $this->set('_serialize', ['coreConfig']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {        
        if ($this->request->is('post')) {
             foreach ($this->request->data() as $key => $config_data) {                
                $config = $this->CoreConfigs->newEntity();
                $config->config_id = $key;
                $config->config_data = trim($config_data);               
                $this->CoreConfigs->save($config);
            }
        }

        $add_workout_schedule = $this->CoreConfigs->find()->where(['config_id' => 'add_workout_schedule'])->select(['config_data'])->hydrate(false)->first();
        $send_invite = $this->CoreConfigs->find()->where(['config_id' => 'send_invite'])->select(['config_data'])->hydrate(false)->first();
        $accept_friend_request = $this->CoreConfigs->find()->where(['config_id' => 'accept_friend_request'])->select(['config_data'])->hydrate(false)->first();
        $recieve_message = $this->CoreConfigs->find()->where(['config_id' => 'recieve_message'])->select(['config_data'])->hydrate(false)->first();

        $this->set(compact('add_workout_schedule','send_invite','accept_friend_request','recieve_message'));        
    }

    /**
     * Edit method
     *
     * @param string|null $id Core Config id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $coreConfig = $this->CoreConfigs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $coreConfig = $this->CoreConfigs->patchEntity($coreConfig, $this->request->getData());
            if ($this->CoreConfigs->save($coreConfig)) {
                $this->Flash->success(__('The core config has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The core config could not be saved. Please, try again.'));
        }
        $configs = $this->CoreConfigs->Configs->find('list', ['limit' => 200]);
        $this->set(compact('coreConfig', 'configs'));
        $this->set('_serialize', ['coreConfig']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Core Config id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $coreConfig = $this->CoreConfigs->get($id);
        if ($this->CoreConfigs->delete($coreConfig)) {
            $this->Flash->success(__('The core config has been deleted.'));
        } else {
            $this->Flash->error(__('The core config could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
