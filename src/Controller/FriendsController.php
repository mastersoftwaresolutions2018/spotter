<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class FriendsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id=null) {
        $this->loadModel('Users');
        $this->set('title', 'Friends'); 
        $page      = 0;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = $_GET['page'];
        }
        if ((isset($_GET['query']))){ 
            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }
            $condition[] = [                        
                        
                        'Friends.status' => 1,
                        'OR' => [
                            'Fusers.firstname LIKE' => trim($_GET['query']) . '%',
                            'FFriends.firstname LIKE' => trim($_GET['query']) . '%',
                        ],
                        'AND' =>[
                            'OR' => [
                                'ffriend_id'=> $id,
                                'user_id'=> $id,
                            ],
                        ]
                    ];
        }else{
            $condition[] = [
                        'Friends.status' => 1,
                        'OR' => [
                            'ffriend_id'=> $id,
                            'user_id'=> $id,
                        ]
                    ]; 
        } 
                  

        $this->paginate = [
            'contain'   => ['Fusers','FFriends'],
            'conditions'=> $condition
        ];
        $users  = $this->paginate($this->Friends);
        $userId = $id;
        $this->set(compact(['users','userId']));
        $this->set('_serialize', ['users']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */ 
    public function block($id = null)
    {
        $user               = $this->ReportedUsers->get($id);
        $user->is_block     = 1;
        $user->updated_at   = date('Y-m-d H:i:s');
        if ($this->ReportedUsers->save($user)) {
            $this->Flash->success(__('User has been blocked successfully.'));
        } else {
            $this->Flash->error(__('User could not be blocked. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}