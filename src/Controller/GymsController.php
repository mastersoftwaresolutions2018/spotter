<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * Gyms Controller
 *
 * @property \App\Model\Table\GymsTable $Gyms
 *
 * @method \App\Model\Entity\Gym[] paginate($object = null, array $settings = [])
 */
class GymsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $gyms = $this->paginate($this->Gyms);

        $this->set(compact('gyms'));
        $this->set('_serialize', ['gyms']);
    }

    /**
     * View method
     *
     * @param string|null $id Gym id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gym = $this->Gyms->get($id, [
            'contain' => []
        ]);

        $this->set('gym', $gym);
        $this->set('_serialize', ['gym']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gym = $this->Gyms->newEntity();
        if ($this->request->is('post')) {
            pr($this->request->getData());
            $bookTable = TableRegistry::get('Gyms');
            $book = $bookTable->newEntity();
            $book = $this->Gyms->patchEntity($book, $this->request->data);
            $result = $bookTable->save($book);
            $gym = $this->Gyms->patchEntity($gym, $this->request->getData());
            if ($result) {
                $this->Flash->success(__('The gym has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gym could not be saved. Please, try again.'));
        }
        $this->set(compact('gym'));
        $this->set('_serialize', ['gym']);
    }

    public function uploadCsv()
    {
        
    }



    public function validateAddGym()
    {
        $error = array();

        $error['statuscode'] = 200; 
       return $error;
    }
    

    public function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {return "";}
        $l   = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }
    private function uploadImage($image)
    {
        $error = 0;
        if (isset($_FILES["cover_photo"])) {
            $tmpFile   = $_FILES["cover_photo"]["tmp_name"];
            $extension = $this->getExtension($_FILES['cover_photo']['name']);
            $tme       = time();
            $fileName  = IMG_PATH. $tme . '.' . $extension;

            $extensionArray = array('jpg', 'jpeg', 'png');

            list($width, $height) = getimagesize($tmpFile);

            if ($width == null && $height == null) {
                $error = 1;
            }
            if (!in_array($extension, $extensionArray)) {
                $error = 1;
            } else {
                if (move_uploaded_file($tmpFile, $fileName)) {
                    $error = IMG_PATHS. $tme . '.' . $extension;
                }
            }
        }
        return $error;
    }
















    /**
     * Edit method
     *
     * @param string|null $id Gym id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gym = $this->Gyms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gym = $this->Gyms->patchEntity($gym, $this->request->getData());
            if ($this->Gyms->save($gym)) {
                $this->Flash->success(__('The gym has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gym could not be saved. Please, try again.'));
        }
        $this->set(compact('gym'));
        $this->set('_serialize', ['gym']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gym id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $gym = $this->Gyms->get($id);
        if ($this->Gyms->delete($gym)) {
            $this->Flash->success(__('The gym has been deleted.'));
        } else {
            $this->Flash->error(__('The gym could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
