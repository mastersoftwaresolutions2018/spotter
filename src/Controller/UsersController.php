<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\Validation\Validation;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['forgotpassword','resetpassword','confirmotp','login','verify']);
    }
    public function index()
    {
        
        $this->set('title', 'Users');
        $page      = 0;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = $_GET['page'];
        }
        if ((isset($_GET['query']))) {
            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }
            $condition[] = [
                        'AND' => [
                            'firstname LIKE' => trim($_GET['query']) . '%',
                            'is_deleted'=> 0,
                            'role_id'=> 2
                        ],
                    ];
        } else {
            $condition[] = [
                        'AND' => [
                            'is_deleted'=> 0,
                            'role_id'=> 2
                        ],
                    ];
        }
        $this->paginate = [
            'contain' => ['Groups', 'Roles']
        ];
        $this->paginate = [
                'page'          => $page,
                'conditions'    => $condition,
                'order' =>  [ 'Users.id' => 'desc' ],
            ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Groups', 'Roles']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
    
    public function verify($id = null)
    {
        $userTable  = TableRegistry::get('Users');
        $user       = $userTable ->find() ->where(['otp' => $id]) ->first();
        if ($user) {
            $user1          = $userTable->get(@$user['id']);
            $user1->otp     = '';
            $user1->active  = 1;
            $result         = $userTable-> save($user1);
            if ($result) {
                die("Your email verified successfully.");
            } else {
                die("Some error occured. Please try again.");
            }
        } else {
            die("Invalid link.");
        }
    }

    private function getifusernameexist($name)
    {
        $getname = TableRegistry::get('Users');
        $getnameexist = $getname->find()
                             ->where(['AND'=>['email'=>$name]])
                             ->count();
        return $getnameexist;
    }
    public function getifusernameexist1()
    {
        $data    = $this->request->data;
        $getname = TableRegistry::get('Users');
        $getnameexist = $getname->find()
                             ->where(['AND'=>['email'=>$data['email']]])
                             ->count();
        echo $getnameexist;
        die;
    }
    private function validateAddUser($data)
    {
        $error = array();
        if (! isset($data['email']) || empty($data['email'])) {
            $error['name'] = 'Email is required.';
        } elseif ($this->getifusernameexist($data['email'])) {
            $error['name'] = 'Email already exists.';
        } elseif (! isset($data['firstname']) || empty($data['firstname'])) {
            $error['name'] = 'First name is required.';
        } elseif (! isset($data['lastname']) || empty($data['lastname'])) {
            $error['name'] = 'Last name is required.';
        } elseif (! isset($data['about']) || empty($data['about'])) {
            $error['name'] = 'About is required.';
        } elseif (! isset($data['gender']) || empty($data['gender'])) {
            $error['name'] = 'Gender is required.';
        } elseif (! isset($data['address']) || empty($data['address'])) {
            $error['name'] = 'Address is required.';
        } elseif (! isset($data['city']) || empty($data['city'])) {
            $error['name'] = 'City is required.';
        } elseif (! isset($data['state']) || empty($data['state'])) {
            $error['name'] = 'State is required.';
        } elseif (! isset($data['country']) || empty($data['country'])) {
            $error['name'] = 'Country is required.';
        } elseif (! isset($data['zipcode']) || empty($data['zipcode'])) {
            $error['name'] = 'Zipcode is required.';
        } elseif (! isset($data['dob']) || empty($data['dob'])) {
            $error['name'] = 'dob is required.';
        }
        if (count($error) > 0) {
             $error['statuscode'] = 201;
        } else {
             $error['statuscode'] = 200;
        }
        return $error;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Add User');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $error = $this->validateAddUser($this->request->getData());
            if ($error['statuscode'] == 200) {
                $data = $this->request->data;
                $data['dob'] = date("Y-m-d", strtotime($data['dob']));
                $user = $this->Users->patchEntity($user, $data);
                //pr($user); die;
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            } else {
                    $this->Flash->error(__($error['name']));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'roles'));
        $this->set('_serialize', ['user']);
    }

    private function getifnameExistTwice($id, $name)
    {
        $getname = TableRegistry::get('Users');
        $getnameexist = $getname->find()
                             ->where(['AND'=>['email'=>$name,'id <>'=>$id]])
                             ->count();
        return $getnameexist;
    }
    public function getifnameExistTwice1()
    {
        $data    = $this->request->data;
        $getname = TableRegistry::get('Users');
        $getnameexist = $getname->find()
                             ->where(['AND'=>['email'=>$data['email'],'id <>'=>$data['id']]])
                             ->count();
        echo $getnameexist;
        die;
    }
    private function validateEditUser($data)
    {
        $error = array();
        if (! isset($data['email']) || empty($data['email'])) {
            $error['name'] = 'Email is required.';
        } elseif ($this->getifnameExistTwice($data['id'], $data['email'])) {
            $error['name'] = 'Email already exists.';
        } elseif (! isset($data['firstname']) || empty($data['firstname'])) {
            $error['name'] = 'First name is required.';
        } elseif (! isset($data['lastname']) || empty($data['lastname'])) {
            $error['name'] = 'Last name is required.';
        } elseif (! isset($data['about']) || empty($data['about'])) {
            $error['name'] = 'About is required.';
        } elseif (! isset($data['gender']) || empty($data['gender'])) {
            $error['name'] = 'Gender is required.';
        } elseif (! isset($data['address']) || empty($data['address'])) {
            $error['name'] = 'Address is required.';
        } elseif (! isset($data['city']) || empty($data['city'])) {
            $error['name'] = 'City is required.';
        } elseif (! isset($data['state']) || empty($data['state'])) {
            $error['name'] = 'State is required.';
        } elseif (! isset($data['country']) || empty($data['country'])) {
            $error['name'] = 'Country is required.';
        } elseif (! isset($data['zipcode']) || empty($data['zipcode'])) {
            $error['name'] = 'Zipcode is required.';
        } elseif (! isset($data['dob']) || empty($data['dob'])) {
            $error['name'] = 'dob is required.';
        }
        if (count($error) > 0) {
             $error['statuscode'] = 201;
        } else {
             $error['statuscode'] = 200;
        }
        return $error;
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Edit User');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $error = $this->validateEditUser($this->request->data);
            if ($error['statuscode'] == 200) {
                $data = $this->request->data;
                $data['dob'] = date("Y-m-d", strtotime($data['dob']));
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->success(__($error['name']));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'roles'));
        $this->set('_serialize', ['user']);
    }
    private function validateEditAdmin($data)
    {
        $error = array();
        if (! isset($data['email']) || empty($data['email'])) {
            $error['name'] = 'Email is required.';
        } elseif (! isset($data['password']) || empty($data['password'])) {
            $error['name'] = 'Password is required.';
        } elseif ($data['password'] != $data['confirmPassword']) {
            $error['name'] = 'Passwords does not match.';
        }
        if (count($error) > 0) {
             $error['statuscode'] = 201;
        } else {
             $error['statuscode'] = 200;
        }
        return $error;
    }
    public function checkAdminpassword()
    {
        $data   = $this->request->data;
        $id     = 1;
        $ep     = $this->Users->get($this->Auth->user('id'))->password;
        $password   = $data['password'];
        $hasher     = new DefaultPasswordHasher();
        $rr = $hasher->check($password, $ep);
        if ($rr) {
             echo "matched";
            die;
        } else {
            echo "unmatched";
            die;
        }
    }
    public function updateAdmin($id = 1)
    {
        $this->set('title', 'Update Admin');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $error = $this->validateEditAdmin($this->request->data);
            if ($error['statuscode'] == 200) {
                $data = $this->request->data;
                unset($data['confirmPassword']);
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been updated.'));
                    return $this->redirect($this->referer());
                } else {
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->success(__($error['name']));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups', 'roles'));
        $this->set('_serialize', ['user']);
    }
    public function forgotpassword()
    {
        $this->set('title', 'Forgot Password');
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $userTable = TableRegistry::get('Users');
            $token     = \Cake\Utility\Text::uuid();
            $modified  = date('Y-m-d h:i:s');
            $query     = $userTable->query();
            $result1   = $this->Users->find('all')->where(['email' => $data['email']])->hydrate(false)->toArray();
            if ($result1) {
                $result    =    $query->update()
                                ->set(['token' => $token, 'updated_at' => $modified])
                                ->where(['email' => $data['email']])
                                ->execute();
                if ($result) {
                    $message = HTTP_ROOT.'users/resetpassword/'.$token;
                    $email = new Email('default');
                    $email->from([Admin_email => 'Spotter'])
                        ->to($data['email'])
                        ->subject('Reset Password')
                        ->send($message);
                }
                $session = $this->request->session();
                $session->write('userf.email', $data['email']);
                $this->Flash->success(__('The link has been sent to your E-mail.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('The user not found. Please enter valid email.'));
                return $this->redirect(['action' => 'forgotpassword']);
            }
        }
    }
    public function confirmotp()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $session = $this->request->session();
            $email = $session->read('userf.email');
            $userTable = TableRegistry::get('Users');
            $modified  = date('Y-m-d h:i:s');
            $query     = $userTable->query();
            $result    = $this->Users->find()->where(['token' => $data['token'], 'email' => $email])->hydrate(false)->toArray();
            if ($result) {
                $this->Flash->success(__('OTP verified.'));

                return $this->redirect(['action' => 'resetpassword']);
            } else {
                $this->Flash->error(__('The OTP is not correct.'));
                return $this->redirect(['action' => 'confirmotp']);
            }
        }
    }
    public function resetpassword($id = null)
    {
        $this->set('title', 'Reset Password');
        if ($this->request->is('post')) {
            $data       = $this->request->data;
            $session    = $this->request->session();
            $email      = $session->read('userf.email');
            $userTable  = TableRegistry::get('Users');
            $modified   = date('Y-m-d h:i:s');
            $query      = $userTable->query();
            $password   = $data['password'];
            $hasher     = new DefaultPasswordHasher();
            $password   = $hasher->hash($password);

            $result     = $this->Users->find()->where(['email' => $email])->hydrate(false)->toArray();
            if ($result) {
                $result1 =   $query->update()->set(['password' => $password, 'updated_at' => $modified])->where(['token' => $data['token']])->execute();
                if ($result1) {
                    $this->Flash->success(__('Password updated successfully.'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->success(__('Some error occured. Please try again later.'));
                    return $this->redirect(['action' => 'login']);
                }
            } else {
                $this->Flash->error(__('Some error occured. Please try again later.'));
                return $this->redirect(['action' => 'login']);
            }
        } else {
            $user     = $this->Users->find()->where(['token' => $id])->hydrate(false)->toArray();
            if (!count($user)) {
                $this->Flash->error(__('No such information found.'));
                return $this->redirect(['action' => 'login']);
            }
            $numMinutes = 30;
            $time = new FrozenTime($user[0]['updated_at']);
            $time = $time->modify("+{$numMinutes} minutes");
            $time = $time->format('Y-m-d H:i:s');
            $now  = date('Y-m-d H:i:s');
            if ($now > $time) {
                $this->Flash->error(__('Link has been exoired. Try again.'));
                return $this->redirect(['action' => 'login']);
            }
        }
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $data   = $this->request->data;
           
            $user1  = $this->Users->find()->where(['email'=> $data['email'],'role_id'=> 1])->count();
            if ($user1) {
                if (Validation::email($this->request->data['username'])) {
                    $this->Auth->config('authenticate', [
                    'Form' => [
                    'fields' => ['username' => 'email']
                    ]
                    ]);
                    $this->Auth->constructAuthenticate();
                    /*$this->request->data['email'] = $this->request->data['username'];
                    unset($this->request->data['username']);*/
                }
                $user   = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error(__('Invalid email or password, try again.'));
                }
            } else {
                $this->Flash->error(__('Invalid email or password, try again.'));
            }
        }
        if ($this->Auth->user()) {
            return $this->redirect(['action' => 'index']);
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Users->get($id);
        $user->is_deleted = 1;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('User has been deleted successfully.'));
        } else {
            $this->Flash->error(__('User could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
