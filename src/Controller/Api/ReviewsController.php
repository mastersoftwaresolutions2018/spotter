<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;  
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

class ReviewsController extends AppController
{
	/*public $paginate = [
        'page' => 1,
        'limit' => 5,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];*/
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);       
    }

	public function index()
	{
		$data     	= $this->request->getData();
        $bookTable  =  TableRegistry::get('Books');
        $book       = $bookTable->find()->where(['id'=> @$data['book_id']])->first();
        $condition  = array();
        if(@$data['book_id']){
            $condition = ['book_id'=>@$data['book_id']];
        }        
        $this->paginate =   [
                                'contain' => ['Users'],
                                'conditions'=>$condition
                            ];
        $reviews = $this->paginate($this->Reviews)->toArray();

        if($reviews){
           	$this->set('data', [
                    'book'=> $book,
                   	'reviews'=> $reviews,
                ]);
            $this->Crud->action()->config('serialize.data', 'data');
            return $this->Crud->execute();
        }else{
            throw new NotFoundException(__('No review for this book.'));
        }
	}

    public function edit($id=null)
    {
        $data       = $this->request->data;
        $id         = @$data['id'];

        $validate   = array();
        if(!@$data['id']){
            throw new NotAcceptableException("Enter review Id to update.");
        }
        $review     = $this->Reviews->get(@$data['id']);
        $this->Reviews->patchEntity($review, $data);
        $result     = $this->Reviews-> save($review);


        if($result){
            $this->set([
                'success' => true,
                'data' => [
                    'Review_id' => $data['id'],
                    'result'=> "Review updated successfully"
                ],
                '_serialize' => ['success', 'data']
            ]);
        }/*else{
            throw new NotAcceptableException("Your data couldn't be saved. Please try again.");
        }*/
            
        return $this->Crud->execute();
    }

    public function view($id=null)
    {
        $data       = $this->request->data;
        if(!$id){
            $id         = @$data['id'];
        }
        $condition      = ['id' => $id];
        $this->paginate =   [
                                'contain' => ['Users','Comments' => function($q) {
                                                        return $q
                                                            ->order(['created' =>'ASC'])
                                                            ->limit(5);
                                            }],
                                'conditions'=>$condition
                            ];

        $reviews =  $this->Reviews->find()->where(['Reviews.id' => $id])
                         ->contain(['Users',
                            'Comments.Users' => function($q) {
                                return $q
                                    ->order(['created' =>'ASC'])
                                    ->limit(5);
                    }
                    ])->first();                    
        //$reviews = $this->paginate($this->Reviews)->toArray();

        $this->set('data', [
                    'reviews'=> $reviews,
                ]);
            $this->Crud->action()->config('serialize.data', 'data');
            return $this->Crud->execute();
        return $this->Crud->execute();
    }
}