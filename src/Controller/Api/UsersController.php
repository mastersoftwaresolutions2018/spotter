<?php
namespace App\Controller\Api;

use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Exception\NotAcceptableException;
use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\UnauthorizedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Crud\Exception\CrudException;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['sendMsg']);
    }
    public $paginate = [
        'page'          => 1,
        'limit'         => 5,
        //'maxLimit' => 15,
        'sortWhitelist' => [
            'id', 'firstname',
        ],
    ];

    public function beforeFilter(Event $event)
    {
        $this->loadModel('Chats');
        $this->loadModel('Workouts');
        $this->loadModel('Friends');
        $this->loadModel('CoreConfigs');
        $this->loadModel('Notifications');
        parent::beforeFilter($event);
    }

    // Get user data according to email
    private function userExists($value = '')
    {

        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['email' => $value])->toArray();
        if (count($user) > 0) {
            return true;
        }
        return false;
    }

    // Validate user data
    private function validateProfile($data)
    {

        $error = array();
        if (!isset($data['email']) || empty($data['email'])) {
            $error['messageText'] = "Email can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif ($this->userExists($data['email'])) {
            $error['messageText'] = "Email already exists.";
            $error['messageCode'] = 409;
            $error['successCode'] = 0;
        } elseif (!isset($data['firstname']) || empty($data['firstname'])) {
            $error['messageText'] = "Firstname can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif (!isset($data['lastname']) || empty($data['lastname'])) {
            $error['messageText'] = "Lastname can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif (!isset($data['password']) || empty($data['password'])) {
            $error['messageText'] = "Password can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif (!isset($data['gender']) || empty($data['gender'])) {
            $error['messageText'] = "Gender can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif (!isset($data['address']) || empty($data['address'])) {
            $error['messageText'] = "Address can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } elseif (!isset($data['workout_place_id']) || empty($data['workout_place_id'])) {
            $error['messageText'] = "Workout Address can not be empty.";
            $error['messageCode'] = 406;
            $error['successCode'] = 0;
        } else {
            $error['messageCode'] = 200;
        }

        return $error;
    }

    // Generate random password according to length
    private function randomPassword($length = null)
    {

        $alphabet    = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass        = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    // Add/Signup [Register new user and send verification email.]
    public function add()
    {

        if ($this->request->is('post')) {
            $data     = $this->request->getData();
            $email1   = $data['email'];
            $validate = $this->validateProfile($data);

            if ($validate['messageCode'] != 200) {
                if ($validate['messageCode'] == 409) {
                    $userTable    = TableRegistry::get('Users');
                    $inActiveuser = $userTable->find()->where(['email' => $email1, 'active' => 0])->first();
                    if ($inActiveuser) {
                        $length     = 20;
                        $otp        = $this->randomPassword($length);
                        $userr      = $userTable->get($inActiveuser['id']);
                        $userr->otp = $otp;
                        $result     = $userTable->save($userr);

                        $url         = HTTP_ROOT . "users/verify/" . $otp;
                        $user_name   = $data['firstname'] . ' ' . $data['lastname'];
                        $userDataArr = array(
                            'url'       => $url,
                            'user_name' => $user_name,
                        );

                        $email = new Email();
                        $email->template('verification', 'verification')
                            ->emailFormat('html')
                            ->to($email1)
                            ->subject('Verification mail')
                            ->from([Admin_email => 'Spotter'])
                            ->viewVars($userDataArr)
                            ->send();

                        $this->set([
                            'status'     => true,
                            'data'       => [
                                'user_id' => $inActiveuser['id'],
                                'result'  => "We have sent email verification link on your email-Id. Please verify your email before login.",
                            ],
                            '_serialize' => ['success', 'data'],
                        ]);
                    } else {
                        throw new NotAcceptableException($validate['messageText'], $validate['messageCode']);
                    }
                } else {
                    throw new NotAcceptableException($validate['messageText'], $validate['messageCode']);
                }
            } else {
                $this->Crud->on('afterSave', function (Event $event, $imageUrl = null) {

                    $data = $this->request->getData();

                    if ($event->subject->created) {
                        $this->set(
                            'data',
                            [
                                'user_id' => $event->subject->entity->id,
                                'result'  => "We have sent email verification link on your email-Id. Please verify your email before login.",
                            ]
                        );
                        $this->Crud->action()->config('serialize.data', 'data');

                        $length    = 20;
                        $otp       = $this->randomPassword($length);
                        $userTable = TableRegistry::get('Users');
                        $user      = $userTable->get($event->subject->entity->id);
                        $user->otp = $otp;

                        $imageUrl = null;
                        $error    = array();

                        if (isset($_FILES['imageUrl'])) {
                            if ($_FILES['imageUrl']['error'] > 0) {
                                $error['messageText'] = "Error during uploading, try again.";
                                $error['messageCode'] = 415;
                                $error['successCode'] = 0;
                                throw new NotAcceptableException($error['messageText'], $error['messageCode']);
                            } else {
                                $extsAllowed = array('jpg', 'jpeg', 'png', 'gif');
                                $extUpload   = strtolower(substr(strrchr($_FILES['imageUrl']['name'], '.'), 1));

                                if (in_array($extUpload, $extsAllowed)) {
                                    $name   = "img/{$_FILES['imageUrl']['name']}";
                                    $result = move_uploaded_file($_FILES['imageUrl']['tmp_name'], $name);
                                    if ($result) {
                                        $imageUrl = $name;
                                    }
                                } else {
                                    $imageUrl             = null;
                                    $error['messageText'] = "File is not valid. Please try again.";
                                    $error['messageCode'] = 415;
                                    $error['successCode'] = 0;
                                    throw new NotAcceptableException($error['messageText'], $error['messageCode']);
                                }
                            }
                        }

                        if ($imageUrl !== null) {
                            $user['user_image'] = $imageUrl;
                        }

                        $result = $userTable->save($user);

                        $url         = HTTP_ROOT . "users/verify/" . $otp;
                        $user_name   = $data['firstname'] . ' ' . $data['lastname'];
                        $userDataArr = array(
                            'url'       => $url,
                            'user_name' => $user_name,
                        );

                        $email = new Email();
                        $email->template('verification', 'verification')
                            ->emailFormat('html')
                            ->to($event->subject->entity->email)
                            ->subject('Verification mail')
                            ->from([Admin_email => 'Spotter'])
                            ->viewVars($userDataArr)
                            ->send();
                    }
                });
                return $this->Crud->execute();
            }
        }
    }

    // Login with user email & password and also with facebook (using fbToken)
    public function login()
    {

        if ($this->request->is('post')) {
            $data         = $this->request->getData();
            $device_token = $data['device_token'];
            $device_type  = $data['device_type'];
            $userTable    = TableRegistry::get('Users');

            if (@$data['id'] && trim(@$data['id']) != '') {
                $fbData = $this->request->getData();
                $user   = $this->Users->find('all')->where(['social_token' => $fbData['id']])->hydrate(false)->first();
                if ($user) {
                    //Update user social token if email already in db.
                    if ($user['email']) {
                        $query  = $userTable->query();
                        $result = $query->update()
                            ->set(['email' => @$fbData['email']])
                            ->where(['id' => $user['id']])
                            ->execute();
                    }
                    $token = JWT::encode(
                        [
                        'sub' => $user['id'],
                        'exp' => strtotime('+1 day', time()),
                        ],
                        Security::salt()
                    );
                } else {
                    $entity               = $userTable->newEntity();
                    $entity->social_token = @$fbData['id'];
                    $entity->social_type  = 'facebook';
                    $entity->active       = 1;
                    if (@$fbData['first_name'] != '' && $fbData['last_name'] != '' && $fbData['email'] != '') {
                        $entity->firstname = $fbData['first_name'];
                        $entity->lastname  = $fbData['last_name'];
                        $entity->email     = $fbData['email'];
                    }

                    $result     = $userTable->save($entity);
                    $user['id'] = $result->id;
                    if ($result) {
                        $token = JWT::encode(
                            [
                                'sub' => $result->id,
                                'exp' => strtotime('+1 day', time()),
                                //'exp' =>  time() + 12000
                            ],
                            Security::salt()
                        );
                    }
                }
                $user = $userTable->get($user['id']);
            } else {
                $user = $this->Auth->identify();

                if (!$user) {
                    throw new UnauthorizedException('Invalid username or password', 412);
                }
                $token = JWT::encode(
                    [
                    'sub' => $user['id'],
                    'exp' => strtotime('+1 day', time()),
                    ],
                    Security::salt()
                );
            }

            $is_complete = true;
            if (@$user['firstname'] == '' || @$user['lastname'] == '' || @$user['gender'] == '' || @$user['about'] == '' || @$user['age'] == '' || $user['user_image'] == '' || @$user['state'] == '' || @$user['country'] == '') {
                $is_complete = false;
            }

            $user_image = "";
            if (!empty($user['user_image']) || $user['user_image'] != null) {
                $user_image = HTTP_ROOT . $user['user_image'];
            }

            $setData = [
                'token'              => $token,
                'id'                 => $user['id'],
                'firstname'          => $user['firstname'],
                'lastname'           => $user['lastname'],
                'email'              => $user['email'],
                'gender'             => $user['gender'],
                'about'              => $user['about'],
                'address'            => $user['address'],
                'imageUrl'           => $user_image,
                'workout_place_id'   => $user['workout_place_id'],
                'workout_place_name' => $user['workout_place_name'],
                'result'             => "You have logged in successfully",
            ];

            foreach ($setData as $key => $value) {
                if (empty($value) || $value === null) {
                    $setData[$key] = "";
                }
            }

            $this->set([
                'success'    => true,
                'data'       => $setData,
                '_serialize' => ['success', 'data'],
            ]);

            $user = $userTable->get($user['id']);
            if ($device_token != $user['device_token']) {
                $user->device_token = $device_token;
                $user->device_type  = $device_type;
            }

            $user->is_logged_in = 1;
            $user->token        = $token;
            if (@$data['lat'] != '' && @$data['lng'] != '') {
                $user->lat = @$data['lat'];
                $user->lng = @$data['lng'];
            }
            $result = $userTable->save($user);
        }
    }

    // Logout functionality
    public function logout()
    {

        if ($this->request->is('post')) {
            $data               = $this->request->getData();
            $user               = $this->Users->get($data['user_id']);
            $user->is_logged_in = 0;
            $user->device_token = '';
            $result             = $this->Users->save($user);

            if ($result) {
                $this->set([
                    'success'    => true,
                    'data'       => [
                        'result' => "You have logged out successfully",
                    ],
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new CrudException(__('Some error occured. Please try again later.'));
            }
        }
    }

    // Forgot password [send new password in registered email]
    public function forgotpassword()
    {

        if ($this->request->is('post')) {
            $length    = 8;
            $data      = $this->request->data;
            $password1 = $this->randomPassword($length);
            $hasher    = new DefaultPasswordHasher();
            $password  = $hasher->hash($password1);
            $userTable = TableRegistry::get('Users');
            $token     = \Cake\Utility\Text::uuid();
            $modified  = date('Y-m-d h:i:s');
            $query     = $userTable->query();
            $result1   = $this->Users->find('all')->where(['email' => $data['email']])->hydrate(false)->toArray();
            if ($result1) {
                $result = $query->update()
                    ->set(['password' => $password, 'updated_at' => $modified])
                    ->where(['email' => $data['email']])
                    ->execute();
                if ($result) {
                    $user_name   = $result1[0]['firstname'] . ' ' . $result1[0]['lastname'];
                    $userDataArr = array(
                        'user_name' => $user_name,
                        'password'  => $password1,
                    );
                    $email = new Email();
                    $email->template('reset_password', 'reset_password')
                        ->emailFormat('html')
                        ->to($data['email'])
                        ->subject('Reset Password')
                        ->from([Admin_email => 'Spotter'])
                        ->viewVars($userDataArr)
                        ->send();
                }
                $session = $this->request->session();
                $session->write('userf.email', $data['email']);
                $this->set([
                    'success'    => true,
                    'data'       => [
                        'result' => "New password has been sent to your E-mail.",
                    ],
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new NotFoundException(__('The user not found. Please enter valid email.'));
            }
        }
    }

    // Get Profile of user
    public function getProfile($id = null)
    {

        $user = $this->Users->find()->where(['id' => $id])->hydrate(false)->first();

        $a = $user['user_image'];

        $user_image = "";
        if (!empty($user['user_image']) || $user['user_image'] != null) {
            $user_image = HTTP_ROOT . $user['user_image'];
        }

        $unread_msgs = 0;
        $unread_msgs = $this->Chats->find()
            ->where(['is_read' => 0, 'to_user_id' => $id])
            ->count();

        $setData = [
            'id'                 => $user['id'],
            'firstname'          => $user['firstname'],
            'lastname'           => $user['lastname'],
            'email'              => $user['email'],
            'gender'             => $user['gender'],
            'about'              => $user['about'],
            'address'            => $user['address'],
            'imageUrl'           => $user_image,
            'workout_place_id'   => $user['workout_place_id'],
            'workout_place_name' => $user['workout_place_name'],
            'unread_msgs'        => $unread_msgs,
        ];

        foreach ($setData as $key => $value) {
            if ($key !== 'unread_msgs') {
                if (empty($value) || $value === null) {
                    $setData[$key] = "";
                }
            }
        }

        if ($user) {
            $this->set([
                'success'    => true,
                'data'       => $setData,
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__('User not found.', 702));
        }
    }

    // Validate data of updated user
    private function validateupdateProfile($id, $data)
    {

        $error     = array();
        $userTable = TableRegistry::get('Users');
        $user      = $userTable->find()->where(['email' => $data['email'], 'id <>' => $id])->toArray();
        if (count($user) > 0) {
            $error['messageText'] = "Email already exists.";
            $error['messageCode'] = 1053;
            $error['successCode'] = 0;
        } else {
            if (!isset($data['email']) || empty($data['email'])) {
                $error['messageText'] = "Email can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } elseif (!isset($data['firstname']) || empty($data['firstname'])) {
                $error['messageText'] = "Firstname can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } elseif (!isset($data['lastname']) || empty($data['lastname'])) {
                $error['messageText'] = "Lastname can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } elseif (!isset($data['gender']) || empty($data['gender'])) {
                $error['messageText'] = "Gender can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } elseif (!isset($data['address']) || empty($data['address'])) {
                $error['messageText'] = "Address can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } elseif (!isset($data['workout_place_id']) || empty($data['workout_place_id'])) {
                $error['messageText'] = "Workout Address can not be empty.";
                $error['messageCode'] = 406;
                $error['successCode'] = 0;
            } else {
                $error['messageCode'] = 200;
            }
        }

        return $error;
    }

    // Edit user data
    public function edit()
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $id       = @$data['id'];
            $validate = array();
            if (@$data['email']) {
                $validate = $this->validateupdateProfile($id, $data);
                if (@$validate['messageCode'] != 200) {
                    throw new NotAcceptableException($validate['messageText']);
                }
            }

            // start uploading image
            $imageUrl = null;
            $error    = array();
            if (isset($_FILES['imageUrl'])) {
                if ($_FILES['imageUrl']['error'] > 0) {
                    $error['messageText'] = "Error during uploading, try again.";
                    $error['messageCode'] = 415;
                    $error['successCode'] = 0;
                    throw new NotAcceptableException($error['messageText'], $error['messageCode']);
                } else {
                    $extsAllowed = array('jpg', 'jpeg', 'png', 'gif');
                    $extUpload   = strtolower(substr(strrchr($_FILES['imageUrl']['name'], '.'), 1));

                    if (in_array($extUpload, $extsAllowed)) {
                        $name   = "img/{$_FILES['imageUrl']['name']}";
                        $result = move_uploaded_file($_FILES['imageUrl']['tmp_name'], $name);
                        if ($result) {
                            $imageUrl = $name;
                        }
                    } else {
                        $imageUrl             = null;
                        $error['messageText'] = "File is not valid. Please try again.";
                        $error['messageCode'] = 415;
                        $error['successCode'] = 0;
                        throw new NotAcceptableException($error['messageText'], $error['messageCode']);
                    }
                }
            }

            if ($imageUrl !== null) {
                $data['user_image'] = $imageUrl;
            }

            $userTable = TableRegistry::get('Users');
            $user      = $userTable->get(@$data['id']);
            $userTable->patchEntity($user, $data);
            $result = $userTable->save($user);
            if ($result) {
                if ($user['user_image']) {
                    $user['user_image'] = HTTP_ROOT . $user['user_image'];
                }

                $setData = [
                    'id'                 => $user['id'],
                    'firstname'          => $user['firstname'],
                    'lastname'           => $user['lastname'],
                    'email'              => $user['email'],
                    'gender'             => $user['gender'],
                    'about'              => $user['about'],
                    'address'            => $user['address'],
                    'imageUrl'           => $user['user_image'],
                    'workout_place_id'   => $user['workout_place_id'],
                    'workout_place_name' => $user['workout_place_name'],
                    'result'             => "Your profile updated successfully",
                ];

                foreach ($setData as $key => $value) {
                    if (empty($value) || $value === null) {
                        $setData[$key] = "";
                    }
                }

                $this->set([
                    'success'    => true,
                    'data'       => $setData,
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new CrudException("Your data couldn't be saved. Please try again.");
            }
        }
    }

    // Check user's logged in status
    public function loginStatus($id = null)
    {

        $user = $this->Users->get($id);

        //Check if user exists
        if ($user) {
            $message = '';
            $success = '';
            //Check if user logged in
            if ($user['is_logged_in'] === 1) {
                $success = true;
                $message = "You are logged in.";
            } else {
                $success = false;
                $message = "You are logged out.";
            }

            $this->set([
                'success'    => $success,
                'data'       => [
                    'message' => $message,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            //if user does not exist
            throw new NotFoundException(__('User not found.', 702));
        }
    }

    // List of users according to gym id(workout place id)
    public function listUsersByGymId($workout_place_id = null, $user_id = null)
    {

        $user_id = array($user_id);
        $users   = $this->Users->find()->where(['Users.workout_place_id' => $workout_place_id, 'Users.role_id' => 2, 'Users.id NOT IN' => $user_id])->hydrate(false)->toArray();
        foreach ($users as $key => $user) {
            foreach ($user as $inner_key => $value) {
                if (empty($value) || $value === null) {
                    $user[$inner_key] = "";
                }
            }

            if ($user['user_image']) {
                $user['user_image'] = HTTP_ROOT . $user['user_image'];
            }

            $users[$key] = $user;
        }

        if (count($users) > 0) {
            $this->set([
                'success'    => true,
                'data'       => [
                    'users' => $users,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__('No user found according to selected gym.'));
        }
    }

    // Schedule workout apis
    public function scheduleWorkout()
    {

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $workout_data  = $this->Workouts->newEntity($data);
            $workout_time  = date("h:ia", strtotime($data['time']));
            $workout_place = $data['workout_place_name'];

            if ($this->Workouts->save($workout_data)) {
                if ($data['friend_id']) {
                    $user_data = $this->Users->get($data['user_id']);
                    $username  = ucfirst($user_data['firstname']) . ' ' . ucfirst($user_data['lastname']);

                    $friend_data  = $this->Users->get($data['friend_id']);
                    $device_token = $friend_data['device_token'];
                    $device_type  = $friend_data['device_type'];

                    $msg            = $this->CoreConfigs->find()->where(['config_id' => 'add_workout_schedule'])->hydrate(false)->first();
                    $before_message = $msg['config_data'];
                    $afterMessage   = str_replace(array('{username}', '{gymname}', '{time}'), array($username, $workout_place, $workout_time), $before_message);

                    $unreadNotification = $this->saveNotification($afterMessage, 'schedule_workout', $data['user_id'], $data['friend_id']);

                    $push_notification = $this->pushNotifications($device_type, $device_token, $afterMessage, $unreadNotification, $data['user_id'], 'schedule_workout');
                }

                $date = explode(' ', $data['time']);
                $this->set([
                    'success'    => true,
                    'data'       => [
                        'message' => 'Congrats! you scheduled a workout at ' . $workout_time . ' at the ' . $workout_place . ' ' . $date['0'] . ' !',
                    ],
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new CrudException("Your data couldn't be saved. Please try again.");
            }
        }
    }

    // Recent Scheduled Workouts
    public function recentWorkouts($id = null)
    {

        $users = $this->Workouts->find()->where(['user_id' => $id])->Contain('Users')->hydrate(false)->order(['Workouts.id' => 'DESC'])->toArray();

        if (count($users) > 0) {
            $recent_users = array();
            $count        = 0;
            foreach ($users as $key => $user) {
                if ($user['friend_id']) {
                    $firstname = $user['user']['firstname'];
                    $lastname  = $user['user']['lastname'];
                    $imageUrl  = $user['user']['user_image'];

                    if (empty($firstname) || $firstname === null) {
                        $firstname = "";
                    }
                    if (empty($lastname) || $lastname === null) {
                        $lastname = "";
                    }
                    if (empty($imageUrl) || $imageUrl === null) {
                        $imageUrl = "";
                    }
                    if ($imageUrl) {
                        $imageUrl = HTTP_ROOT . $imageUrl;
                    }

                    $recent_users[$count]['id']        = $user['user']['id'];
                    $recent_users[$count]['firstname'] = $firstname;
                    $recent_users[$count]['lastname']  = $lastname;
                    $recent_users[$count]['imageUrl']  = $imageUrl;
                    $count++;
                }
            }
            $this->set([
                'success'    => true,
                'data'       => [
                    'users' => $recent_users,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__('You have not scheduled workout with any user.'));
        }
    }

    //Get list of Invite users
    public function getInviteUsers($id = null)
    {
        $user            = $this->Users->get($id);
        $gymID           = $user['workout_place_id'];
        $excludedUserIds = array($id);
        $friends         = $this->getFriendIds($id);
        $alreadyInvited  = $this->alreadyRequestSent($id);
        //Add friend ids in excluded users
        if (count($friends) > 0) {
            foreach ($friends as $key => $friend_id) {
                $excludedUserIds[] = $friend_id;
            }
        }
        //Add already requested usersin excluded users
        if (count($alreadyInvited) > 0) {
            foreach ($alreadyInvited as $key => $user_id) {
                $excludedUserIds[] = $user_id;
            }
        }
        $excludedUserIds = array_unique($excludedUserIds);
        $users           = $this->Users->find()
            ->where(['Users.id NOT IN' => $excludedUserIds,
                // 'Users.workout_place_id'   => $gymID,
                'Users.role_id'            => 2])
            ->hydrate(false)
            ->toArray();

        if (count($users) > 0) {
            $invite_users = array();
            $count        = 0;
            foreach ($users as $key => $user) {
                $firstname          = $user['firstname'];
                $lastname           = $user['lastname'];
                $imageUrl           = $user['user_image'];
                $workout_place_id   = $user['workout_place_id'];
                $workout_place_name = $user['workout_place_name'];

                if (empty($firstname) || $firstname === null) {
                    $firstname = "";
                }
                if (empty($lastname) || $lastname === null) {
                    $lastname = "";
                }
                if (empty($imageUrl) || $imageUrl === null) {
                    $imageUrl = "";
                }
                if ($imageUrl) {
                    $imageUrl = HTTP_ROOT . $imageUrl;
                }

                $fullName = "";
                if (!empty($firstname)) {
                    $fullName = $firstname;
                    if (!empty($lastname)) {
                        $fullName .= ' ' . $lastname;
                    }
                }

                if (!empty($fullName)) {
                    $invite_users[$count]['id']       = $user['id'];
                    $invite_users[$count]['fullName'] = $fullName;
                    $invite_users[$count]['imageUrl'] = $imageUrl;
                    $count++;
                }
            }

            $this->set([
                'success'    => true,
                'data'       => [
                    'users' => $invite_users,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__('There are no users matching with your profile.'));
        }
    }

    // Get friends user ids
    private function getFriendIds($id = null)
    {

        $friend_ids = array();
        $result     = $this->Friends->find()
            ->where(['status' => 1, 'OR' => ['user_one_id' => $id, 'user_two_id' => $id]])
            ->hydrate(false)->toArray();

        if (count($result) > 0) {
            foreach ($result as $key => $friend) {
                if ($friend['user_one_id'] != $id) {
                    $friend_ids[] = $friend['user_one_id'];
                }
                if ($friend['user_two_id'] != $id) {
                    $friend_ids[] = $friend['user_two_id'];
                }
            }
        }
        return $friend_ids;
    }

    // Check if friend request already sent
    private function alreadyRequestSent($id = null)
    {

        $requested_friend_ids = array();
        $result               = $this->Friends->find()
            ->where(['status' => 0, 'OR' => ['user_one_id' => $id, 'user_two_id' => $id]])
            ->hydrate(false)->toArray();

        if (count($result) > 0) {
            foreach ($result as $key => $requested_friend) {
                if ($requested_friend['user_one_id'] != $id) {
                    $requested_friend_ids[] = $requested_friend['user_one_id'];
                }
                if ($requested_friend['user_two_id'] != $id) {
                    $requested_friend_ids[] = $requested_friend['user_two_id'];
                }
            }
        }
        return $requested_friend_ids;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {

        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } elseif ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function msort($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                asort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }

    public function fsort($array, $key, $sort_flags = SORT_REGULAR)
    {
     
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }

                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                   
                natcasesort($mapping);
               array_reverse($mapping,true);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }

                return $sorted;
            }
        }
        return $array;
    }

    // Get list of friends with detail
    public function getFriends($id = null)
    {

        $friends            = array();
        $sugggested_friends = array();
        $suggestedFriendIds = array();
        $friend_ids         = $this->getFriendIds($id);
        $alreadyInvited     = $this->alreadyRequestSent($id);

        /*pr($friend_ids);
        pr($alreadyInvited);
        pr(array_merge($friend_ids,$alreadyInvited));die();*/
        $userTable      = TableRegistry::get('Users');
        $start_user     = $userTable->get(@$id);
        $start_user_lat = $start_user['lat'];
        $start_user_lng = $start_user['lng'];

        if (count($friend_ids) > 0) {
            $users = $this->Users->find()->where(['id IN' => $friend_ids])->toArray();
            foreach ($users as $key => $user) {
                $firstname          = $user['firstname'];
                $lastname           = $user['lastname'];
                $workout_place_id   = $user['workout_place_id'];
                $workout_place_name = $user['workout_place_name'];
                $imageUrl           = $user['user_image'];

                $f_lat = $user['lat'];
                $f_lng = $user['lng'];

                $distance = $this->distance($start_user['lat'], $start_user['lng'], $user['lat'], $user['lng'], 'K');

                if (empty($firstname) || $firstname === null) {
                    $firstname = "";
                }
                if (empty($lastname) || $lastname === null) {
                    $lastname = "";
                }
                if (empty($workout_place_id) || $workout_place_id === null) {
                    $workout_place_id = "";
                }
                if (empty($workout_place_name) || $workout_place_name === null) {
                    $workout_place_name = "";
                }
                if (empty($imageUrl) || $imageUrl === null) {
                    $imageUrl = "";
                }
                if ($imageUrl) {
                    $imageUrl = HTTP_ROOT . $imageUrl;
                }

                $friends[$key]['id']                 = $user['id'];
                $friends[$key]['firstname']          = $firstname;
                $friends[$key]['lastname']           = $lastname;
                $friends[$key]['workout_place_id']   = $workout_place_id;
                $friends[$key]['workout_place_name'] = $workout_place_name;
                $friends[$key]['imageUrl']           = $imageUrl;
                $friends[$key]['distance']           = $distance;
            }

            // Suggested Friends (get from friends friends who are not friend of logged in user)
            foreach ($friend_ids as $friendId) {
                // Friends friends list.
                $ff_list = $this->getFriendIds($friendId);
                foreach ($ff_list as $ffriendId) {
                    // If the friendsFriend(ff) is not us, and not our friend, he can be suggested
                    if ($ffriendId != $id && !in_array($ffriendId, $friend_ids)) {
                        $suggestedFriendIds[] = $ffriendId;
                    }
                }
            }

            $sugg_friends = array();
            if (count($suggestedFriendIds) > 0) {
                if (count($alreadyInvited) > 0) {
                    $sugg_friends = $this->Users->find()->where(['Users.id IN' => $suggestedFriendIds, 'Users.id NOT IN' => $alreadyInvited])->hydrate(false)->toArray();
                } else {
                    $sugg_friends = $this->Users->find()->where(['Users.id IN' => $suggestedFriendIds])->hydrate(false)->toArray();
                }
            }
            if (count($sugg_friends) == 0) {
                /*  If suggested friend not found from above method than
                create an array of user ids which needs to be excluded from users list.
                this array contains user's own id and his friends ids.
                Get those users who are not my friend, they can be suggested */
                array_push($friend_ids, $id);
                if (count($alreadyInvited) > 0) {
                    $friend_ids = array_merge($friend_ids, $alreadyInvited);
                }
                $sugg_friends = $this->Users->find()->where(['Users.id NOT IN' => $friend_ids, 'role_id' => 2])->hydrate(false)->toArray();
            }

            $count = 0;
            foreach ($sugg_friends as $key => $suggested_friend) {
                $firstname          = $suggested_friend['firstname'];
                $lastname           = $suggested_friend['lastname'];
                $workout_place_id   = $suggested_friend['workout_place_id'];
                $workout_place_name = $suggested_friend['workout_place_name'];
                $imageUrl           = $suggested_friend['user_image'];

                $distance_s = $this->distance($start_user['lat'], $start_user['lng'], $suggested_friend['lat'], $suggested_friend['lng'], 'K');

                if (empty($firstname) || $firstname === null) {
                    $firstname = "";
                }
                if (empty($lastname) || $lastname === null) {
                    $lastname = "";
                }
                if (empty($workout_place_id) || $workout_place_id === null) {
                    $workout_place_id = "";
                }
                if (empty($workout_place_name) || $workout_place_name === null) {
                    $workout_place_name = "";
                }
                if (empty($imageUrl) || $imageUrl === null) {
                    $imageUrl = "";
                }
                if ($imageUrl) {
                    $imageUrl = HTTP_ROOT . $imageUrl;
                }

                if (!empty($firstname)) {
                    $sugggested_friends[$count]['id']                 = $suggested_friend['id'];
                    $sugggested_friends[$count]['firstname']          = $firstname;
                    $sugggested_friends[$count]['lastname']           = $lastname;
                    $sugggested_friends[$count]['workout_place_id']   = $workout_place_id;
                    $sugggested_friends[$count]['workout_place_name'] = $workout_place_name;
                    $sugggested_friends[$count]['imageUrl']           = $imageUrl;
                    $sugggested_friends[$count]['distance']           = $distance_s;
                    $count++;
                }
            }
            
            $friends_n            = $this->fsort($friends, array('distance'));
            $sugggested_friends_n = $this->msort($sugggested_friends, array('distance'));
            $set_data             = [
                'suggestedFriends' => $sugggested_friends_n,
                'friendList'       => $friends_n,
            ];

            $this->set([
                'success'    => true,
                'data'       => $set_data,
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException("You haven't any friend yet. Please click on below button for sending invitations to other users.");
        }
    }

    // Send invitation to friends
    public function sendInvite()
    {

        if ($this->request->is('post')) {
            $data      = $this->request->getData();
            $sender_id = $data['user_one_id'];

            foreach ($data['user_two_id'] as $key => $reciever_id) {
                $friends_data                 = $this->Friends->newEntity();
                $friends_data->user_one_id    = $sender_id;
                $friends_data->user_two_id    = $reciever_id;
                $friends_data->action_user_id = $sender_id;
                $save                         = $this->Friends->save($friends_data);

                if ($save) {
                    $sender_data = $this->Users->get($sender_id);
                    $sendername  = ucfirst($sender_data['firstname']) . ' ' . ucfirst($sender_data['lastname']);

                    $reciever_data = $this->Users->get($reciever_id);
                    $device_token  = $reciever_data['device_token'];
                    $device_type   = $reciever_data['device_type'];

                    $msg                = $this->CoreConfigs->find()->where(['config_id' => 'send_invite'])->hydrate(false)->first();
                    $before_message     = $msg['config_data'];
                    $afterMessage       = str_replace(array('{username}'), array($sendername), $before_message);
                    $unreadNotification = $this->saveNotification($afterMessage, 'send_invite', $sender_id, $reciever_id);

                    $push_notification = $this->pushNotifications($device_type, $device_token, $afterMessage, $unreadNotification, $sender_data, $sender_id);
                }
            }

            if ($save) {
                $this->set([
                    'success'    => true,
                    'data'       => [
                        'message' => 'Invitation sent successfully.',
                    ],
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new CrudException("Error detected while sending invitations. Please try again later.");
            }
        }
    }

    // Accept friend request
    public function acceptRequest()
    {

        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $sender_id      = $data['user_one_id'];
            $reciever_id    = $data['user_two_id'];
            $friend_request = $this->Friends->find()->where(['user_one_id' => $sender_id, 'user_two_id' => $reciever_id, 'status' => 0])->first();

            if ($friend_request) {
                $updated_data                   = array();
                $updated_data['status']         = 1;
                $updated_data['action_user_id'] = $reciever_id;
                $accept_req                     = $this->Friends->patchEntity($friend_request, $updated_data);
                if ($this->Friends->save($accept_req)) {
                    $reciever_data = $this->Users->get($reciever_id);
                    $reciever_name = ucfirst($reciever_data['firstname']) . ' ' . ucfirst($reciever_data['lastname']);

                    $sender_data  = $this->Users->get($sender_id);
                    $device_token = $sender_data['device_token'];
                    $device_type  = $sender_data['device_type'];

                    $msg            = $this->CoreConfigs->find()->where(['config_id' => 'accept_friend_request'])->hydrate(false)->first();
                    $before_message = $msg['config_data'];
                    $afterMessage   = str_replace(array('{username}'), array($reciever_name), $before_message);

                    $this->deleteInviteNotification($sender_id, $reciever_id, 'send_invite');
                    $unreadNotification = $this->saveNotification($afterMessage, 'accept_request', $reciever_id, $sender_id);

                    $push_notification = $this->pushNotifications($device_type, $device_token, $afterMessage, $unreadNotification, $reciever_id);

                    $this->set([
                        'success'    => true,
                        'data'       => [
                            'message' => "Your request has accepted.",
                        ],
                        '_serialize' => ['success', 'data'],
                    ]);
                } else {
                    throw new CrudException("Some error occured. Please try again later.");
                }
            } else {
                throw new NotFoundException("No pending request.");
            }
        }
    }

    // Reject friend request
    public function rejectRequest()
    {

        if ($this->request->is('post')) {
            $data           = $this->request->getData();
            $user_one_id    = $data['user_one_id'];
            $user_two_id    = $data['user_two_id'];
            $friend_request = $this->Friends->find()->where(['user_one_id' => $user_one_id, 'user_two_id' => $user_two_id, 'status' => 0])->first();

            if ($friend_request) {
                $updated_data                   = array();
                $updated_data['status']         = 2;
                $updated_data['action_user_id'] = $user_two_id;
                $reject_req                     = $this->Friends->patchEntity($friend_request, $updated_data);
                if ($this->Friends->save($reject_req)) {
                    $this->deleteInviteNotification($user_one_id, $user_two_id, 'send_invite');
                    $this->set([
                        'success'    => true,
                        'data'       => [
                            'message' => "Your request has rejected.",
                        ],
                        '_serialize' => ['success', 'data'],
                    ]);
                } else {
                    throw new CrudException("Some error occured. Please try again later.");
                }
            } else {
                throw new NotFoundException("No pending request.");
            }
        }
    }

    //Get all notifications
    public function getNotifications($id = null)
    {

        //Mark all notifications as read
        $user                       = $this->Users->get($id);
        $user->unread_notifications = 0;
        $this->Users->save($user);
        $type = array('recieve_message');

        $notifications = $this->Notifications->find()->where(['reciever_id' => $id, 'type NOT IN' => $type])->hydrate(false)->order(['Notifications.id' => 'DESC'])->toArray();

        if (count($notifications)) {
            foreach ($notifications as $key => $value) {
                $notifications[$key]['key'] = $key;
            }

            $this->set([
                'success'    => true,
                'data'       => [
                    'notifications' => $notifications,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__("You don't have any feed."));
        }
    }

    //Delete Notification by id
    public function deleteNotification($id = null)
    {

        $notification = $this->Notifications->get($id);
        $result       = $this->Notifications->delete($notification);

        if ($result) {
            $this->set([
                'success'    => true,
                'data'       => [
                    'message' => "Notification deleted successfully",
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new CrudException(__('Some error occured. Please try again later.'));
        }
    }

    //Dismiss friend request notification on accept & reject request
    private function deleteInviteNotification($sender_id, $reciever_id, $type)
    {

        $notification = $this->Notifications->find()->where(['sender_id' => $sender_id, 'reciever_id' => $reciever_id, 'type' => $type])->first();

        $result = $this->Notifications->delete($notification);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

/************************************************************/

    // fetch all schedules
    public function getschedules($id = null)
    {
    }

    public function deleteUser()
    {
        $data = $this->request->data;
        $user = $this->Users->get(@$data['user_id']);
        if ($user) {
            $user->email = @$data['email'];
            $this->Users->save($user);
            $this->set([
                'success'    => true,
                'data'       => [
                    'message' => "Email updated successfully.",
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException("No user found.");
        }
    }

    public function test()
    {

        $response['messageCode'] = 200;
        $response['messageText'] = "OK";
        $this->set('data', [
            'messageCode' => 200,
            'messageText' => "OK",
        ]);
        $this->Crud->action()->config('serialize.data', 'data');
        return $this->Crud->execute();
        // echo json_encode($response); die;
    }

    private function upload_image($content = null, $imgname = null, $dest = null, $extn = null)
    {
        if ($content) {
            $dataarray = explode('base64,', $content);
            if (count($dataarray) == 1) {
                $extn = 'jpg';

                if ($extn) {
                    $img_name = $imgname . "-" . time();
                    $data     = $content;
                    $data     = str_replace(' ', '+', $data);
                    $data     = base64_decode($data);
                    file_put_contents($dest . $img_name . "." . $extn, $data);

                    chmod($dest . $img_name . "." . $extn, 0777);
                    return $img_name . "." . $extn;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function uploadImage($base64)
    {
        $img = explode(',', $base64);

        $data     = base64_decode($img[1]);
        $filename = uniqid() . '.png';
        $file_dir = "../webroot/img/" . $filename;

        $success = file_put_contents($file_dir, $data);
        if ($success) {
            return $filename;
        } else {
            return false;
        }
    }

    public function getCountries()
    {
        $countryTable = TableRegistry::get('Countries');
        $countries    = $countryTable->find('all')->hydrate(false)->toArray();
        if ($countries) {
            $this->set([
                'success'    => true,
                'data'       => [
                    'countries' => $countries,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException(__('No country found.'));
        }
    }

    public function getState($id = null)
    {
        $statesTable = TableRegistry::get('States');
        $states      = $statesTable->find('all')->where(['country_id' => $id])->hydrate(false)->toArray();
        if ($states) {
            $this->set([
                'success'    => true,
                'data'       => [
                    'states' => $states,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            //throw new NotAcceptableException('No state found.' ,422);
            $this->set([
                'success'    => false,
                'data'       => [
                    'message' => 'No state found.',
                    'url'     => '',
                    'code'    => 701,
                ],
                '_serialize' => ['success', 'data'],
            ]);
        }
    }

    public function resetPassword()
    {
        $data        = $this->request->data;
        $oldPassword = $this->Users->get($data['user_id'])->password;
        $password    = $data['currentpassword'];
        $hasher      = new DefaultPasswordHasher();
        $result      = $hasher->check($password, $oldPassword);

        if ($result) {
            if ($data['password'] == $data['confirmPassword']) {
                unset($data['confirmPassword']);
                unset($data['currentpassword']);
                $user = $this->Users->get($data['user_id']);
                unset($data['user_id']);
                $user = $this->Users->patchEntity($user, $data);
                if ($this->Users->save($user)) {
                    $this->set([
                        'success'    => true,
                        'data'       => [
                            'message' => "Password has been updated.",
                        ],
                        '_serialize' => ['success', 'data'],
                    ]);
                } else {
                    throw new CrudException("Some error occured. Please try again later.");
                }
            } else {
                throw new NotAcceptableException("Passwords do not match.");
            }
        } else {
            throw new UnauthorizedException("Invalid password.");
        }
    }

    public function sendOtp()
    {
        $data = $this->request->data;
        $user = $this->Users->get(@$data['user_id']);
        if ($user) {
            $alphabet    = "0123456789";
            $pass        = array(); //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 4; $i++) {
                $n      = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            $otp       = implode($pass);
            $user->otp = $otp;
            $this->Users->save($user);
            $email = new Email('default');
            $email->from([Admin_email => 'Spotter'])
                ->to($user->email)
                ->subject('Change Email')
                ->send($otp);
            $this->set([
                'success'    => true,
                'data'       => [
                    'message' => "Otp has been sent.",
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException("No user found.");
        }
    }

    public function verifyOtp()
    {
        $data = $this->request->data;
        $user = $this->Users->get(@$data['user_id']);
        if ($user) {
            if ($user->otp == $data['otp']) {
                $user->otp = "";
                $this->Users->save($user);
                $this->set([
                    'success'    => true,
                    'data'       => [
                        'message' => "Otp verified.",
                    ],
                    '_serialize' => ['success', 'data'],
                ]);
            } else {
                throw new UnauthorizedException("Otp does not match.");
            }
        } else {
            throw new NotFoundException("No user found.");
        }
    }

    public function updateEmail()
    {
        $data = $this->request->data;
        $user = $this->Users->get(@$data['user_id']);
        if ($user) {
            $user->email = @$data['email'];
            $this->Users->save($user);
            $this->set([
                'success'    => true,
                'data'       => [
                    'message' => "Email updated successfully.",
                ],
                '_serialize' => ['success', 'data'],
            ]);
        } else {
            throw new NotFoundException("No user found.");
        }
    }

    private function notThisUser($user_id, $token)
    {

        $driverTable = TableRegistry::get('Users');
        $driverInfo  = $driverTable
            ->find()
            ->select(['id', 'firstname', 'lastname', 'status'])
            ->where(['id' => $user_id])
            ->andwhere(['token' => $token])
            ->andwhere(['type_user' => 'customer'])
            ->andwhere(['is_deleted' => 0])
            ->toArray();

        if (count($driverInfo) > 0) {
            return true;
        }
        return false;
    }

    public function sendMsg()
    {
        $push_notification = $this->pushNotifications(1, "fXo-mpIyOho:APA91bFLAQ8uChqsy3xlVq-IlFZNK7hu9oF1nXfh3vL1I2OAoIJ3O2KjsTNpYSkBOpCP4kteaJpjnXQccQaxhyXd6iLQ0alJTH-XyuL_G2ydwmTftQtM_ref_D1jq3G77l4MSQ5hB-sm", "Mss Test has messaged you", 3, 88, "recieve_message");


        // $push_notification = $this->pushNotifications(1, 'czXbLrG25XU:APA91bGqa_hx-eEQgpnSk0Bt9WhwJ3Uytz5u10P2QqLl-7pPxifVi30kZgKlwHVuSf1jx1ZwlYuZwPfBzq-lYUJztPX33VqniJ7BOajFz5qoPnY6WhHcEe2wKvQmA4EX-lUeSZyvphh2', "Working fine.", 7, 88, 'recieve_message');
        pr($push_notification);
        die;
    }

/************************************************************/
}
