<?php

namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;  
use Cake\Network\Exception\NotFoundException;
use Crud\Exception\CrudException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class FriendsController extends AppController {

    public $paginate = [
        'page' => 1,
        'limit' => 5,
        //'maxLimit' => 15,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id=null) {
        $this->loadModel('Users');
        $this->set('title', 'Friends'); 
        $page      = 0;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = $_GET['page'];
        }
        if ((isset($_GET['query']))){ 
            if (isset($_GET['page']) && !empty($_GET['page'])) {
                $page = $_GET['page'];
            }
            $condition[] = [                        
                        
                        'Friends.status' => 1,
                        'OR' => [
                            'Fusers.firstname LIKE' => trim($_GET['query']) . '%',
                            'FFriends.firstname LIKE' => trim($_GET['query']) . '%',
                        ],
                        'AND' =>[
                            'OR' => [
                                'ffriend_id'=> $id,
                                'user_id'=> $id,
                            ],
                        ]
                    ];
        }else{
            $condition[] = [
                        'Friends.status' => 1,
                        'OR' => [
                            'ffriend_id'=> $id,
                            'user_id'=> $id,
                        ]
                    ]; 
        } 
                  

        $this->paginate = [
            'contain'   => ['Fusers','FFriends'],
            'conditions'=> $condition
        ];
        $friends  = $this->paginate($this->Friends);
        if($friends){
            $this->set('data', [
                    'friends'=> $friends
                ]);
            $this->Crud->action()->config('serialize.data', 'data');
            return $this->Crud->execute();
        }
        return $this->Crud->execute();
    }
    public function add($data=null)
    {
        if(!$data){
            $data       = $this->request->getData();
        }   
        $message        = '';           
        $friendsTable   =  TableRegistry::get('Friends');
        //$friend         = $this->Friends->find('all')->where(['user_id' => @$data['user_id'], 'book_id' => @$data['book_id']])->hydrate(false)->first();

        $friend = $this->Friends->find()->where(['user_id' => @$data['user_id'], 'ffriend_id'=> @$data['ffriend_id']])->orWhere(['user_id' => @$data['ffriend_id'], 'ffriend_id'=> @$data['user_id']])->hydrate(false)->first();

        if($friend){
            if(@$data['status'] == 2){
                $entity         = $friendsTable->get($friend['id']);
                $entity->status = $data['status'];
                $friendsTable->save($entity);
                $message        = "You have blocked this person successfully.";
            }else{
                $message    = "Friend request is already in process.";
            }
        }else{
            $data['status'] = 0;
            $entity     = $friendsTable->newEntity($data);
            $result     = $friendsTable->save($entity);
            $message    = "Friend request has been sent successfully.";
            $friend['id']= $result->id;
        }       
        
        if(@$result || $friend){
            $this->set([
                'success' => true,
                'data' => [
                    'id'=> $friend['id'],
                    'user_id' => @$data['user_id'],
                    'result'=> $message
                ],
                '_serialize' => ['success', 'data']
            ]);
        }else{
            throw new CrudException("Your data couldn't be saved. Please try again.");
        }
    }
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */ 
    public function block()
    {
        $data           = $this->request->getData();
        $data['status'] = 3;
        $this->add($data);
    }
}