<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;  
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

class GymsController extends AppController
{
	public $paginate = [
        'page' => 1,
        'limit' => 10,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];

    public function index()
    {
        $data   = $this->request->getData();
        $longitude = $data['longitude'];
        $latitude = $data['latitude'];

        // if(empty($longitude) && empty($latitude)) {

        // }
        $gyms = $this->paginate($this->Gyms);
        $this->set('data', [
                'gyms'=> $gyms
            ]);
        $this->Crud->action()->config('serialize.data', 'data');
        return $this->Crud->execute();
    }
}
