<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;  
use Cake\Network\Exception\NotFoundException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;

class ReviewLikesController extends AppController
{
	/*public $paginate = [
        'page' => 1,
        'limit' => 5,
        'sortWhitelist' => [
            'id', 'name'
        ]
    ];*/
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);       
    }


    public function likeReview()
    {
        $data               = $this->request->getData();
        $reviewLikesTable   =  TableRegistry::get('ReviewLikes');
       // $bookTable->patchEntity($book, $data1);
        $reviewLike          = $this->ReviewLikes->find('all')->where(['user_id' => @$data['user_id'], 'review_id' => @$data['review_id']])->hydrate(false)->first();
        if($reviewLike){
            $reviewLike1    = $reviewLikesTable->get($reviewLike['id']);
            $reviewLikesTable->patchEntity($reviewLike1, $data);
            $result         = $reviewLikesTable-> save($reviewLike1);
        }else{
            $entity = $reviewLikesTable->newEntity($data);
            $result = $reviewLikesTable->save($entity);
        }
        
        if($result){
            $this->set([
                'success' => true,
                'data' => [
                    'user_id' => @$data['user_id']
                ],
                '_serialize' => ['success', 'data']
            ]);
        }else{
            throw new CrudException("Your data couldn't be saved. Please try again.");
        }
    }
}