<?php
namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;  
use Cake\Network\Exception\NotFoundException;
use Crud\Error\Exception\CrudException;
use Cake\Network\Exception\BookloverException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class ChatsController extends AppController implements MessageComponentInterface {

    protected $clients;
    public function initialize() {
        parent::initialize();
        $this->clients = new \SplObjectStorage;
        $this->Auth->allow(['onOpen','onMessage','onClose','onError']);   
        $this->db_host = '192.168.0.13';
        $this->db_username = 'web';
        $this->db_password = 'ix9u3~nP$Xk~2L7yie';
        $this->db_name = 'staging_fitness';
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);        
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        $conn1  = mysqli_connect($this->db_host, $this->db_username, $this->db_password, $this->db_name);
        mysqli_set_charset($conn1,"utf8mb4");            
        $msg1       = json_decode($msg, true);
        $msgd       = @$msg1['message'];
        $user_id    = @$msg1['from_user_id'];
        $to_user_id = @$msg1['to_user_id'];

       
         
            

        if(@$msg1['type'] == 'Message'){
            $msgd = mysqli_real_escape_string($conn1, $msgd);
            $sql    = "INSERT INTO `chats` (`from_user_id`, `to_user_id`, `message`) VALUES ('".$user_id."', '".$to_user_id."' , '".$msgd."')";
            if ($conn1->query($sql) === TRUE) {                
                echo sprintf('Message saved successfully.'."\n");
                
                $sendername = ""; 
                $device_token = "";  
                $device_type = ""; 
                $afterMessage = "";
                $unread_notifications=0;
                $new_unread_count = 0 ;
                
                $sender_query = "SELECT * FROM `users` WHERE `id`= '".$user_id."'";
                $result_sender = $conn1->query($sender_query);
                $sender_data = array();

                if ($result_sender->num_rows > 0) {
                    $sender_data = $result_sender->fetch_assoc();
                    if(!empty($sender_data['firstname'])){
                        $sendername = $sender_data['firstname'];
                    }
                    if(!empty($sender_data['lastname'])){
                        $sendername .= ' '.$sender_data['lastname'];
                    }
                } 
                echo sprintf("Hey User =>>>>> ". $sendername."\n");

                $reciver_query = "SELECT * FROM `users` WHERE `id`= '".$to_user_id."'";
                $result_reciever = $conn1->query($reciver_query);

                if ($result_reciever->num_rows > 0) {
                    $reciever_data = $result_reciever->fetch_assoc();
                    $device_token = $reciever_data['device_token'];
                    $device_type = $reciever_data['device_type']; 
                    $unread_notifications = $reciever_data['unread_notifications'];


                }   

                echo sprintf("Device Token =>>>>> ". $device_token."\n");
                echo sprintf("Device type =>>>>> ". $device_type."\n"); 
                
                $msg_query = "SELECT * FROM `core_configs` WHERE `config_id`= 'recieve_message'";
                $result_msg = $conn1->query($msg_query);         

                if ($result_msg->num_rows > 0) {
                    $msg_data = $result_msg->fetch_assoc();
                    $before_message = $msg_data['config_data'];
                    $afterMessage =  str_replace(array('{username}'), array($sendername), $before_message);
                }

                echo sprintf("Message =>>>>> ". $afterMessage."\n");                   

                $add_notification = "INSERT INTO `notifications` (`notification`, `type`, `sender_id`, `reciever_id`) VALUES ('".$afterMessage."', 'recieve_message', '".$user_id."', '".$to_user_id."')";

                if ($conn1->query($add_notification) === TRUE) { 
                    $new_unread_count = $unread_notifications + 1;
                    $update_user = "UPDATE `users` SET `unread_notifications`='".$new_unread_count."' WHERE id='".$to_user_id."'";
                    if ($conn1->query($update_user) === TRUE) {
                        echo "Count {$new_unread_count} updated\n";
                    } 
                }
                // $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
                
                //  $txt = "$user_id-".$user_id."\n".",to_user_id-".$to_user_id."\n".",device_type-".$device_type."\n".", device_token-".$device_token."\n"." , afterMessage-".$afterMessage."\n".", new_unread_count-".$new_unread_count."\n"." ,user_id-".$user_id."\n".", to_user_id- ".$to_user_id.'-vivek';
             
                // fwrite($myfile, $txt);

                // fclose($myfile);

                
                $push_notification = $this->pushNotifications($device_type,$device_token,$afterMessage,$new_unread_count, $user_id,'recieve_message');

            }
        }

        if (@$msg1['type'] == 'read_msg') {
            $sql    = "UPDATE chats SET is_read= 1 WHERE to_user_id='".$user_id."' AND from_user_id='".$to_user_id."' AND is_read= 0";
            if ($conn1->query($sql) === TRUE) {
                echo sprintf('Message marked as read successfully.'."\n");
            }
        }

        if (@$msg1['type'] == 'connect') {
            $sql = "UPDATE users SET conn_id='".$from->resourceId."' WHERE id='".$user_id."'";


             

            if ($conn1->query($sql) === TRUE) {
                echo sprintf('New user connected successfully.'. "\n");
            }
        }

        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $sql1 = "SELECT `conn_id` FROM `users` WHERE `id`= '".$to_user_id."'";
                $result = $conn1->query($sql1);
                              
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    // Send message to specific User        
                    if($row['conn_id'] && $client->resourceId== $row['conn_id']){
                        if(@$msg1['type'] == 'Message'){
                            $client->send($msg);                         
                        }
                    }                    
                }                
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        $conn1  = mysqli_connect($this->db_host, $this->db_username, $this->db_password, $this->db_name); 
        $date   = date("Y-m-d H:i:s");
        $sql    = "UPDATE users SET conn_id='', last_seen = '".$date."' WHERE conn_id='".$conn->resourceId."'";
        if ($conn1->query($sql) === TRUE) {
            echo  sprintf("Information updated successfully\n");
        }
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
    
    //Get chat of two persons
    public function index() {

        $this->loadModel('Users'); 
        $data       = $this->request->getData(); 

        $page       = 0;
        if (@$data['page'] && !empty($data['page'])) {
            $page = $data['page'];
        }

        $condition[] = [                     
                        'OR' => [
                            'from_user_id' => $data['from_user_id'],
                            'to_user_id' => $data['from_user_id'],
                        ],
                        'AND' =>[
                            'OR' => [
                                'from_user_id'=> $data['to_user_id'],
                                'to_user_id'=> $data['to_user_id'],
                            ],
                        ]
                    ];                  
        
        $this->Chats->updateAll(
                    ['is_read'=>1],
                    ['from_user_id'=> $data['to_user_id'],'to_user_id'=> $data['from_user_id']]);

        $this->paginate = [
            'conditions'=> $condition,            
            'page'=> @$page,
            'order'=>  [
                        'Chats.id' => 'desc'
                    ],
            'limit'=> 20        
        ];

        $chats  = $this->paginate($this->Chats);

        $latest_msg = count($chats)-1;
        
        $messages = array();
        foreach ($chats as $key => $chat) {
            $messages[$latest_msg] = $chat;            
            $created_time = date('Y-m-d\TH:i:s', strtotime($chat['created']));            
            $messages[$latest_msg]['created_time'] = $created_time;                        
            $latest_msg--;
        }
        
        $messages = array_reverse($messages);
        
        if(count($chats)<0){
            $messages = array_values($messages);
        }

        $from_user_data = $this->Users->find()->where(['id'=>$data['from_user_id']])->select(['id','firstname','lastname','user_image'])->hydrate(false)->first();

        if(empty($from_user_data['user_image']) || $from_user_data['user_image'] === null){
            $from_imgUrl = "";
        } else{
            $from_imgUrl = $from_user_data['user_image'];
        }  

        if(empty($from_user_data['firstname'])) {
            $from_firstname = "";
        } else{
            $from_firstname = $from_user_data['firstname'];
        }

        if(empty($from_user_data['lastname'])){
            $from_lastname = "";
        } else{
            $from_lastname = $from_user_data['lastname'];
        }

        $from_fullName = $from_firstname.' '.$from_lastname;

        $from_user_data['uid'] = $from_user_data['id'];        
        $from_user_data['fullName'] = $from_fullName;
        $from_user_data['imgUrl'] = $from_imgUrl;
        $from_user_data['message'] = "";

        $to_user_data = $this->Users->find()->where(['id'=>$data['to_user_id']])->select(['id','firstname','lastname','user_image'])->hydrate(false)->first();

        if(empty($to_user_data['user_image']) || $to_user_data['user_image'] === null){
            $imgUrl = "";
        } else{
            $imgUrl = $to_user_data['user_image'];
        }

        if(empty($to_user_data['firstname'])) {
            $to_firstname = "";
        } else{
            $to_firstname = $to_user_data['firstname'];
        }

        if(empty($to_user_data['lastname'])){
            $to_lastname = "";
        } else{
            $to_lastname = $to_user_data['lastname'];
        }

        $to_fullName = $to_firstname.' '.$to_lastname;

        $to_user_data['to_user_id'] = $to_user_data['id'];        
        $to_user_data['fullName'] = $to_fullName;
        $to_user_data['imgUrl'] = $imgUrl;

        if(count($messages)>0){        	
            $data = [
                    'fromUser'=> $from_user_data,
                    'toUser'=> $to_user_data,
                    'chats'=> $messages
                ];            
            $this->set([
                'success' => true,
                'data' => $data,
                '_serialize' => ['success', 'data']
            ]);       
        } else {
            $data = [
                    'fromUser'=> $from_user_data,
                    'toUser'=> $to_user_data,
                    'message' => 'No conversation found.',
                    'chats'=> []
                ];            
            $this->set([
                'success' => false,
                'data' => $data,
                '_serialize' => ['success', 'data']
            ]); 
        }

    }

    //get unique results of chatting
    public function uniqueResults($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
       
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    //Latest one sent and recieve message of all users
    public function inbox($id=null){

        $this->loadModel('Users');
        $user_id = $id;
        
        $chatsTable = TableRegistry::get('Chats');
       
        $chats = $chatsTable->find()->where(['Chats.from_user_id' => $user_id])->orWhere(['Chats.to_user_id'=> $user_id])->order(['Chats.id'=>'DESC'])->select(['id', 'from_user_id','to_user_id','message','created'])->hydrate(false)->toArray();        

        $chats1 = array();
        $chats11 = array();
        if($chats){
            $chats1     = $chats;
            foreach ($chats as $key => $value) {
                
                if($value['from_user_id'] == $user_id){
                    $new= $value['from_user_id'].'_'.$value['to_user_id'];
                }

                if($value['to_user_id'] == $user_id){
                    $new= $value['to_user_id'].'_'.$value['from_user_id'];
                }
                $chats1[$key]['new']        =  $new;
            }
            $chats1 = $this->uniqueResults($chats1,'new');
            
            foreach ($chats1 as $key1 => $value1) {
                unset($value1['new']);
                $chats11[] =  $value1;                
            }
        }               

        foreach ($chats11 as $key2 => $value2) {
                        
            $unread_msgs = 0;
            if($id == $value2['from_user_id']){
                $unread_msgs = $chatsTable->find()
                            ->where(['is_read' => 0,'from_user_id' => $value2['to_user_id'],'to_user_id'=>$value2['from_user_id']])
                            ->count();
            } else{
                $unread_msgs = $chatsTable->find()
                            ->where(['is_read' => 0,'from_user_id' => $value2['from_user_id'],'to_user_id'=>$value2['to_user_id']])
                            ->count();
            }
            

            if($value2['from_user_id'] == $user_id){
               $user = $this->Users->get($value2['to_user_id']);
            }else{
              $user = $this->Users->get($value2['from_user_id']);
            }

            if(!empty($user['user_image'])){
                $imgUrl = $user['user_image'];
            } else{
                $imgUrl = '';
            }

            if(!empty($user['firstname'])) {
                $fullName = $user['firstname'];
                if(!empty($user['lastname'])){
                    $fullName = $fullName.' '.$user['lastname'];
                }
            } else{
                $fullName = '';
            }

            $chats11[$key2]['uid'] = $user['id'];
            $chats11[$key2]['fullName'] = $fullName;
            $chats11[$key2]['imgUrl'] = $imgUrl;            
            $chats11[$key2]['unread'] = $unread_msgs;                        
            $created_time = date('Y-m-d\TH:i:s', strtotime($value2['created']));            
            $chats11[$key2]['created_time'] = $created_time;                        
        }
    
        if(count($chats11)>0){
            $this->set([
                'success' => true,
                'data' => $chats11,
                '_serialize' => ['success', 'data']
            ]);
        } else {
            throw new NotFoundException(__('No conversation found.'));
        }
        
    }      

}