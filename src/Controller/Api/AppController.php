<?php
namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Network\Exception\ConflictException;
use Cake\Network\Exception\NotAcceptableException;
use Cake\Network\Exception\NotFoundException;
use Crud\Exception\CrudException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;

class AppController extends Controller
{
    use \Crud\Controller\ControllerTrait;

    public function initialize()
    {
        parent::initialize();

        $this->cert_file = '/var/www/html/spotter/webroot/certificate/fitness.pem';
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Add',
                'Crud.Edit',
                'Crud.Delete'
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog'
            ]
        ]);

        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                'fields' => ['username' => 'email'],
                    'scope' => ['Users.active' => 1]
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Users',
                    'scope' => ['Users.active' => 1],
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
        ]);
        $this->loadComponent('Flash');
        
        if (!@$this->request->data['user_id']) {
            $this->Auth->allow(['index','getDashboard','searchBooks','delete']);
        }
        $this->Auth->allow(['add', 'login','forgotpassword','getState','getCountries','view','loginStatus']);
    }
    
    private function validateUser($userId, $token)
    {
        $config['allowedAlgs'] =  ['HS256'];

        $userTable  = TableRegistry::get('Users');
        $user       = $userTable->find()
                        ->where(['AND'=>['is_logged_in'=>1,'id'=>$userId]])->count();
        if ($user) {
            /*$payload = JWT::decode(
                        $token,
                        Security::getSalt(),
                        $config['allowedAlgs']
                    );
            $user_id = $payload->sub;
            if($user_id == $userId){*/
                return true;
            /*}else{
                return false;
            }*/
        } else {
            return false;
        }
        
       // return $payload;
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function pushNotifications($device_type, $device_token, $message, $unread_count, $sender_id = null, $type = null)
    {
        //For IOS
        if ($device_type == 2) {
            $passphrase = '123456789';
            $pem_file = getcwd().'/certificate/fitness.pem';
            
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->cert_file);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            
            $ios_mode = 'ssl://gateway.sandbox.push.apple.com:2195';
            $error = array();

            // Open a connection to the APNS server
            try {
                $fp = stream_socket_client(
                    $ios_mode,
                    $err,
                    $errstr,
                    60,
                    STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT,
                    $ctx
                );
                if (!$fp) {
                    $error['code']  = 401;
                    $error['error'] = "The detail entered for IOS is not correct: $err $errstr";
                    return $error;
                }

                // Create the payload body
                $body['aps'] = array(
                    'alert' => $message,
                    'sound' => 'default',
                    'badge' => $unread_count
                );

                if ($type == 'recieve_message') {
                    $body['sender_id'] = $sender_id;
                }
                
                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));
            } catch (\Exception $e) {
                $error['code']  = 401;
                $error['error'] = $e->getMessage();
            }

            // Close the connection to the server
            fclose($fp);

            if (!$result) {
                return $error;
            }
            return true;
        } else {
            $url = "https://fcm.googleapis.com/fcm/send";

            $fcmMsg = array(
                'message' => $message,
                'title' => 'Fitness',
                'sound' => "default",
                'color' => "#203E78",
                'sender_id' => $sender_id  //icon color
            );
     
            $fcmFields = array(
                'to' => $device_token,
                'priority' => 'high',
                'data' => $fcmMsg
            );

            $headers = array(
                //'Authorization: key=' . API_ACCESS_KEY,
                 'Authorization: key=AIzaSyCNK4QRWNx82OR3ULKMwWPiCpW1XmWOVOc',
                'Content-Type: application/json'
            );
             
            try {
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));

                // Execute post
                $result   = curl_exec($ch);
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if ($httpcode == '401') {
                    curl_close($ch);
                    return $httpcode;
                }
                if ($result === false) {
                    return false;
                }
            } catch (\Exception $e) {
                $error['code']  = 401;
                $error['error'] = $e->getMessage();
                return $error;
            }

            // Close connection
            curl_close($ch);
            return;
        }
    }

    public function saveNotification($message, $type, $sender_id, $reciever_id)
    {
    
        $this->loadModel('Notifications');
        $this->loadModel('Users');

        $notify_data = $this->Notifications->newEntity();
        $notify_data->notification = $message;
        $notify_data->type = $type;
        $notify_data->sender_id = $sender_id;
        $notify_data->reciever_id = $reciever_id;
        $notify_save = $this->Notifications->save($notify_data);

        if ($notify_save) {
            $user = $this->Users->get($reciever_id);
            $unread_notifications = $user['unread_notifications'];
            $new_count = $unread_notifications + 1;
            $user->unread_notifications = $new_count;
            $result = $this->Users->save($user);
            $recent_count = $result['unread_notifications'];
            return $recent_count;
        } else {
            return false;
        }
    }
}
