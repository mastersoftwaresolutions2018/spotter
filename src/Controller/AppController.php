<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;




/**
    @SWG\Swagger(
        @SWG\Info(
            title="master-cakephp-swagger",
            description="Quickstart annotation example",
            termsOfService="http://swagger.io/terms/",
            version="1.0.0"
        )
    )        
      
   @SWG\Post(
        path="/booklover/api/users/add.json",
        summary="Register",
        tags={"Register"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="name",
            description="User's name",
            in="formData",
            type="string"
        ),
        @SWG\Parameter(
           name="email",
           in="formData",
           description="User's email",
           required=true,
           type="string",
           format="email"
          ),
        @SWG\Parameter(
           name="password",
           in="formData",
           description="User's password",
           required=true,
           type="string",
           format="password"
          ),     
        
          
    
        @SWG\Response(
            response="200",
            description="You have registered successfully",
           @SWG\Schema(
                
                ref="#/definitions/create"
            )
           
        ),
        @SWG\Response(
            response=406,
            description="Email can not be empty."
        ),
        @SWG\Response(
            response=409,
            description="Email already exists."
        ),
        @SWG\Response(
            response=422,
            description="A validation error occurred."
        )
    )
    @SWG\Definition(
               definition="create",
               required=true,
               @SWG\Property(property="id", type="integer"),
               @SWG\Property(property="name", type="string"),
               @SWG\Property(property="email", type="string"),
               @SWG\Property(property="password", type="string"),
               
           )

  @SWG\Post(
        path="/booklover/api/users/login.json",
        summary="Login",
        tags={"Login"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
           name="email",
           in="formData",
           description="User's email",
           required=true,
           type="string",
           format="email"
          ),
        @SWG\Parameter(
           name="password",
           in="formData",
           description="User's password",
           required=true,
           type="string",
           format="password"
          ),     
        
          
    
        @SWG\Response(
            response="200",
            description="You have logged in successfully",
           @SWG\Schema(
                
                ref="#/definitions/create"
            )
           
        ),
        @SWG\Response(
            response=412,
            description="Invalid username or password"
        )
    )
    @SWG\Definition(
               definition="Login",
               required=true,
               @SWG\Property(property="email", type="string"),
               @SWG\Property(property="password", type="string"),
               
           )


    @SWG\Post(
        path="/booklover/api/users/edit.json",
        summary="completeProfile",
        tags={"completeProfile"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="id",
            description="User id",
            in="formData",
            required=true,
            type="integer"
        ),
        @SWG\Parameter(
            name="name",
            description="User's name",
            in="formData",
            type="string"
        ),
        @SWG\Parameter(
           name="gender",
           in="formData",
           description="User's gender",
           type="string"
          ),
        @SWG\Parameter(
           name="age",
           in="formData",
           description="User's age",
           type="integer"
          ),
         @SWG\Parameter(
           name="state",
           in="formData",
           description="User's state",
           type="string"
          ),
        @SWG\Parameter(
           name="country",
           in="formData",
           description="User's country",
           type="string"
          ),
        @SWG\Parameter(
           name="about",
           in="formData",
           description="User's Information",
           type="string"
          ),
        @SWG\Parameter(
           name="user_image",
           in="formData",
           description="User's Image",
           type="string"
          ),
        @SWG\Parameter(
           name="email",
           in="formData",
           description="User's email",
           type="string",
           format="email"
          ),
    @SWG\Response(
            response="200",
            description="Your profile updated successfully",
           @SWG\Schema(
                ref="#/definitions/update"
            ) 
        ),
        @SWG\Response(
            response=500,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="update",
               required=true,
               @SWG\Property(property="id", type="integer"),
               @SWG\Property(property="name", type="string"),
               @SWG\Property(property="state", type="string"),
               @SWG\Property(property="country", type="string"),
               @SWG\Property(property="gender", type="string"),
               @SWG\Property(property="user_image", type="string"),
               @SWG\Property(property="about", type="string"),
               @SWG\Property(property="email", type="string"),
               @SWG\Property(property="password", type="string"),
               
           )

    @SWG\Post(
        path="/booklover/api/users/forgotpassword.json",
        summary="Forgot password",
        tags={"Forgot password"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="email",
            description="User email",
            in="formData",
            required=true,
            type="string"
        ),
    @SWG\Response(
            response="200",
            description="New password has been sent to your E-mail.",
           @SWG\Schema(
                ref="#/definitions/Forgot password"
            ) 
        ),
        @SWG\Response(
            response=404,
            description="The user not found. Please enter valid email."
        )
    )
    @SWG\Definition(
               definition="Forgot password",
               required=true,
               @SWG\Property(property="email", type="string"),   
           )



  @SWG\Get(
        path="/booklover/api/users/getCountries.json",
        summary="Get Countries list",
        tags={"Get Countries list"},
        consumes={"application/json"},
        produces={"application/xml", "application/json"},
          
        @SWG\Response(
            response="200",
            description="countries",
            @SWG\Schema(
                
                ref="#/definitions/getCountries"
            )
        ),
        @SWG\Response(
            response=429,
            description="Error"
        )
    )
        @SWG\Definition(
               definition="getCountries",
           )
  




  @SWG\Get(
        path="/booklover/api/users/getState/{id}.json",
        summary="Get states by country id",
        tags={"Get states by country id"},
        consumes={"application/json"},
        produces={"application/xml", "application/json"},
        
        @SWG\Parameter(
            name="id",
            description="Numeric id of the country to get states",
            in="path",
            required=true,
            type="integer",
            
        ),
  
        @SWG\Response(
            response="200",
            description="states",
            @SWG\Schema(
                
                ref="#/definitions/getState"
            )
        ),
        @SWG\Response(
            response=404,
            description="No state found."
        )
    )
        @SWG\Definition(
               definition="getState",
               required=true,
               @SWG\Property(property="id", type="integer"), 
           )



@SWG\Get(
        path="/booklover/api/users/getProfile/{id}.json",
        summary="Get User's Profile id",
        tags={"Get User Info"},
        consumes={"application/json"},
        produces={"application/xml", "application/json"},
        
        @SWG\Parameter(
            name="id",
            description="Numeric id of the User",
            in="path",
            required=true,
            type="integer",
            
        ),
  
        @SWG\Response(
            response="200",
            description="UserInfo",
            @SWG\Schema(
                
                ref="#/definitions/getUserInfo"
            )
        ),
        @SWG\Response(
            response=404,
            description="User not found."
        )
    )
        @SWG\Definition(
               definition="getUserInfo",
               required=true,
               @SWG\Property(property="id", type="integer"), 
           )



@SWG\Post(
        path="/booklover/api/books/index.json",
        summary="getBookCategoryListing",
        tags={"getBookCategoryListing"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="user_id",
            description="User id",
            in="formData",
            type="integer"
        ),
        @SWG\Parameter(
            name="category_id",
            description="Book's Category",
            in="formData",
            type="integer"
        ),
        @SWG\Parameter(
           name="query",
           in="formData",
           description="In case of searching",
           type="string"
          ),
         
    @SWG\Response(
            response="200",
            description="Books",
           @SWG\Schema(
                ref="#/definitions/getBookCategoryListing"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="getBookCategoryListing",
               required=true,
               @SWG\Property(property="user_id", type="integer"),
               @SWG\Property(property="category_id", type="integer"),
               @SWG\Property(property="query", type="string"),
           )


@SWG\Post(
        path="/booklover/api/books/setLike.json",
        summary="SetLike Book",
        tags={"SetLike"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="id",
            description="Book id",
            in="formData",
            type="integer"
        ),
        @SWG\Parameter(
            name="user_id",
            description="User's Id",
            in="formData",
            type="integer"
        ),
         
    @SWG\Response(
            response="200",
            description="Book has added to favourite successfully.",
           @SWG\Schema(
                ref="#/definitions/setLike"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="setLike",
               required=true,
               @SWG\Property(property="id", type="integer"),
               @SWG\Property(property="user_id", type="integer"),
           )



@SWG\Post(
        path="/booklover/api/users/getDashboard.json",
        summary="getDashboard",
        tags={"getDashboard"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="user_id",
            description="User's Id if user logged in.",
            in="formData",
            type="integer"
        ),
         
    @SWG\Response(
            response="200",
            description="categories,Recent Books, Popular Books",
           @SWG\Schema(
                ref="#/definitions/getDashboard"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="getDashboard",
               required=true,
               @SWG\Property(property="user_id", type="integer"),
           )



@SWG\Post(
        path="/booklover/api/shelves/updateStatus.json",
        summary="setShelfStatus",
        tags={"setShelfStatus"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="user_id",
            description="User's Id.",
            in="formData",
            required=true,
            type="integer"
        ),
        @SWG\Parameter(
            name="book_id",
            description="Book's Id.",
            in="formData",
            required=true,
            type="integer"
        ),
        @SWG\Parameter(
            name="shelf_status",
            description="Shelf Status. [0- want to read, 1- reading, 2- read]",
            in="formData",
            type="integer"
        ),
         
    @SWG\Response(
            response="200",
            description="Book has added to shelf successfully.",
           @SWG\Schema(
                ref="#/definitions/setShelfStatus"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="setShelfStatus",
               required=true,
               @SWG\Property(property="user_id", type="integer"),
               @SWG\Property(property="book_id", type="integer"),
               @SWG\Property(property="shelf_status", type="integer"),
           )



@SWG\Get(
        path="/booklover/api/books/view/{book_id}/{user_id}.json",
        summary="getBookDetail",
        tags={"getBookDetail"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="book_id",
            description="Book's Id.",
            in="path",
            required=true,
            type="integer"
        ),
        @SWG\Parameter(
            name="user_id",
            description="User's Id.",
            in="path",
            required=true,
            type="integer"
        ),

    @SWG\Response(
            response="200",
            description="Book data.",
           @SWG\Schema(
                ref="#/definitions/getBookDetail"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="getBookDetail",
               required=true,
           )


@SWG\Get(
        path="/booklover/api/reviews/index.json",
        summary="getAllReviews",
        tags={"getAllReviews"},
        consumes={"application/json"},
        produces={"application/xml","application/json"},
        @SWG\Parameter(
            name="book_id",
            description="Book's Id.",
            in="formData",
            required=true,
            type="integer"
        ),

    @SWG\Response(
            response="200",
            description="Book with Reviews.",
           @SWG\Schema(
                ref="#/definitions/getAllReviews"
            ) 
        ),
        @SWG\Response(
            response=401,
            description="Expired token"
        )
    )
   @SWG\Definition(
               definition="getAllReviews",
               required=true,
           )                      




 */
 

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = [
        'Acl' => [
            'className' => 'Acl.Acl'
        ]
    ];

    public static $AclActionsExclude = [
        'isAuthorized'
    ];
    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        //


        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        //$this->loadComponent('Auth');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'scope' => ['Users.active' => 1]
                ]
            ]
        ]);
        /*$this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'scope' => ['Users.active' => 1]
                ]
            ],
            'authorize' => [
                'Acl.Actions' => ['actionPath' => 'controllers/']
            ],
            'loginAction' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'prefix' => false
            ],
            'authError' => 'You are not authorized to access that location.',
            'flash' => [
                'element' => 'error'
            ]
        ]);*/


 
  
  
        // if ($this->request->params['controller'] !=='Acl') {
                    

            # code...
        // }
         // Only for ACL setup
        // $this->Auth->allow();
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The beforeFilter event.
     * @return void
     */
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow('display');


         if (isset($this->request->params['_matchedRoute']) && $this->request->params['_matchedRoute'] == '/admin' || $this->request->params['controller'] =='Acl' ) {
          //  die('adminbackend');
                        $this->viewBuilder()->layout('adminbackend');

        } else{
         // die('frontend');
                 $this->viewBuilder()->layout('adminbackend');      //$this->viewBuilder()->layout('frontend');

        }

    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        parent::beforeRender($event);

        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * isAuthorized.
     *
     * @param array $user user logged.
     * @return void
     */
    public function isAuthorized($user) {
        // return true;
        
        // Admin can access every action
        if (isset($user['role_id']) && $user['role_id'] === 1) {
            return true;
        }

        // Default deny
        return false;
    }
    public function referer($default = null, $local = false)
     {
         if (!$this->request) {
            return Router::url($default, !$local);
         }

         $referer = $this->request->referer($local);
         if ($referer === '/' && $default && $default !== $referer) {
            $url = Router::url($default, !$local);
            $base = $this->request->getAttribute('base');
         if ($local && $base && strpos($url, $base) === 0) {
            $url = substr($url, strlen($base));
         if ($url[0] !== '/') {
            $url = '/' . $url;
         }

         return $url;
         }

         return $url;
         }

         return $referer;
     }

}
