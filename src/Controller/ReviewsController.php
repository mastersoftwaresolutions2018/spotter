<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ReviewsController extends AppController {

	/**
     * Index method
     *
     * @return \Cake\Network\Response|null
    */
    public function index() {
        $this->set('title', 'Reviews & Ratings');
        $page      = 0;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = $_GET['page'];
        } 
        $condition[] = [
                    'AND' => [
                        'Reviews.is_deleted'=> 0,
                        'Reviews.status'=> 1
                    ],
                ];
        $this->paginate = [
        		'contain' => ['Books','Users'],
                'page'          => $page,
                'conditions'    => $condition,
            ];
        $reviews = $this->paginate($this->Reviews);

        $this->set(compact('reviews'));
        $this->set('_serialize', ['reviews']);
    }
    /*
     * Trashed Reviews
    */
    public function trash() {
        $this->set('title', 'Trashed Reviews');
        $page      = 0;
        if (isset($_GET['page']) && !empty($_GET['page'])) {
            $page = $_GET['page'];
        } 
        $condition[] = [
                    'AND' => [
                        'Reviews.is_deleted'=> 1
                    ],
                ];
        $this->paginate = [
        		'contain' => ['Books','Users'],
                'page'          => $page,
                'conditions'    => $condition,
            ];
        $reviews = $this->paginate($this->Reviews);

        $this->set(compact('reviews'));
        $this->set('_serialize', ['reviews']);
    } 

    private function validateAddreview($data){
        
        $error = array();
        if ( ! isset( $data['user_id'] ) || empty( $data['user_id'] ) ) {
            $error['name'] = 'User name is required.';
        }elseif( ! isset( $data['book_id'] ) || empty( $data['book_id'] ) ){
            $error['name'] = 'Book name is required.';
        }elseif ( ! isset( $data['description'] ) || empty( $data['description'] ) ) {
            $error['name'] = 'Description Name is required.';
        }elseif ( ! isset( $data['rating'] ) || empty( $data['rating'] ) ) {
            $error['name'] = 'Rating is required.';
        }
        if( count( $error ) > 0 ){
             $error['statuscode'] = 201;   
        }else{

             $error['statuscode'] = 200;   
        }
       return $error;
    } 
    /*
     * Add reviews 
    */
    public function add()
    {
        $this->set('title', 'Add Reviews');
        $userTable  =   TableRegistry::get('Users');   
        $Users     	=   $userTable->find('list', [
                                'keyField' => 'id',
                                'valueField' => 'firstname'
                            ])->where(['is_deleted'=>0,'role_id'=>2])->hydrate(false)->toArray();
        $bookTable  =   TableRegistry::get('Books');   
        $Books     	=   $bookTable->find('list', [
                                'keyField' => 'id',
                                'valueField' => 'name'
                            ])->where(['is_deleted'=>0])->hydrate(false)->toArray();
        if ($this->request->is('post')) {
        	$error = $this->validateAddreview($this->request->getData());     
            if($error['statuscode'] == 200)
            {
                $data 			= $this->request->getData();
                $reviewTable 	= TableRegistry::get('Reviews');
                $review 		= $bookTable->newEntity();
                $review 		= $this->Reviews->patchEntity($review, $this->request->data);
               // pr($data); die;
                $result = $reviewTable->save($review);
                
                if( $result->id ) {                                        
                    /*$error['statuscode'] = 200;
                    $error['name'] = "Book saved successfully";
                    echo json_encode($error);die;*/                    
	                $this->Flash->success(__("Review saved successfully"));
	                return $this->redirect(['action' => 'index']);
                }else{                	                    
	                $this->Flash->error(__("Something went wrong. Please try again later."));
                	$this->set(compact('Categories', 'Books','data'));
                } 
            }else{
            	$data = $this->request->data;
                $this->Flash->error(__($error['name']));
                $this->set(compact('Users', 'Books','data'));
            }    
            
        }else{
            $this->set(compact('Users', 'Books')); 
        }
    }

    public function edit($id = null)
    {
        $this->set('title', 'Edit Reviews');
        if ($this->request->is(['patch', 'post', 'put'])) { 
            $review = $this->Reviews->get($id);
            $review = $this->Reviews->patchEntity($review, $this->request->data);
            if($this->Reviews->save($review)){
                $this->Flash->success(__('Review has been updated successfully.'));
                return $this->redirect($this->referer());
            }else{
                $error['name'] = "Something went wrong";
                $this->Flash->success(__($error['name']));
            }
        }
        $review   = $this->Reviews->get($id, [
                'contain' => ['Books','Users']
            ]);
        $this->set(compact('review'));
    }
    public function delete($id = null, $delete=null)
    {
    	if($delete){
    		$review = $this->Reviews->get($id);
    		if($this->Reviews->delete($review)){
    			$this->Flash->success(__('The review has been deleted permanently.'));
    		}else{    			
            $this->Flash->error(__('The review could not be deleted. Please, try again.'));
    		}
    	}else{
    		$review = $this->Reviews->get($id);
	        $review->is_deleted = 1;
	        if ($this->Reviews->save($review)) {
	            $this->Flash->success(__('The review has been moved to trashed.'));
	        } else {
	            $this->Flash->error(__('The review could not be deleted. Please, try again.'));
	        }
    	}        
        return $this->redirect($this->referer());
    }
    public function undelete($id = null)
    {
        $review = $this->Reviews->get($id);
        $review->is_deleted = 0;
        if ($this->Reviews->save($review)) {
            $this->Flash->success(__('The review has been undeleted.'));
        } else {
            $this->Flash->error(__('The review could not be undelet. Please, try again.'));
        }
        return $this->redirect(['action' => 'trash']);
    }
}