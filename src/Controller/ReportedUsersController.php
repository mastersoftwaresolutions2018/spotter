<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ReportedUsersController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->loadModel('Users');
        $this->set('title', 'Reported Users');  
        $condition[] = [
                        'AND' => [
                            'is_block'=> 0,
                        ],
                    ]; 

        $this->paginate = [
            'contain'   => ['Reporters','Reporteds'],
            'conditions'=> $condition
        ];
        $users = $this->paginate($this->ReportedUsers);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */ 
    public function block($id = null)
    {
        $user               = $this->ReportedUsers->get($id);
        $user->is_block     = 1;
        $user->updated_at   = date('Y-m-d H:i:s');
        if ($this->ReportedUsers->save($user)) {
            $this->Flash->success(__('User has been blocked successfully.'));
        } else {
            $this->Flash->error(__('User could not be blocked. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}