<?php
namespace App\Controller;

use App\Controller\AppController;
use ReflectionClass;
use ReflectionMethod;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;

/**
 * Permissions Controller
 *
 *
 * @method \App\Model\Entity\Permission[] paginate($object = null, array $settings = [])
 */
class PermissionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

 



public function getControllers() {
    $files = scandir('../src/Controller/');
    $results = [];
    $ignoreList = [
        '.', 
        '..', 
        'Component', 
		'AppController.php',
		'PagesController.php',
		'ErrorController.php',
		'GroupsController.php',
    ];
    foreach($files as $file){
        if(!in_array($file, $ignoreList)) {
            $controller = explode('.', $file)[0];
            array_push($results, str_replace('Controller', '', $controller));
        }            
    }
    return $results;
}



public function getActions($controllerName) {
    $className = 'App\\Controller\\'.$controllerName.'Controller';
    $class = new ReflectionClass($className);
    $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
    $results = [$controllerName => []];
    $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
    foreach($actions as $action){
        if($action->class == $className && !in_array($action->name, $ignoreList)){
            array_push($results[$controllerName], $action->name);
        }   
    }
    return $results;
}




    public function index()
    {



  $acos = $this->Acl->Aco->find('all', array('order' => 'Acos.lft ASC'))
           ->contain(['Aros'  	]) 
           ->hydrate(false)
           ->toArray();

           





			$controllers = $this->getControllers();
			$resources = [];
		foreach($controllers as   $key => $value  ){
		$actions = $this->getActions($value);
		array_push($resources, $actions);
		}


		$query = TableRegistry::get('Groups')->find();
		$query->hydrate(false);
		$groupresult = $query->toList();


 
        $this->set(compact('resources'));
        $this->set(compact('resources'));
        $this->set(compact('acos'));
        $this->set(compact('groupresult'));
        $this->set('_serialize', ['resources','groupresult','acos']);
    }

    /**
     * View method
     *
     * @param string|null $id Permission id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $permission = $this->Permissions->get($id, [
            'contain' => []
        ]);

        $this->set('permission', $permission);
        $this->set('_serialize', ['permission']);
    }


 

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $permission = $this->Permissions->newEntity();
        if ($this->request->is('post')) {
            $permission = $this->Permissions->patchEntity($permission, $this->request->getData());
            if ($this->Permissions->save($permission)) {
                $this->Flash->success(__('The permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The permission could not be saved. Please, try again.'));
        }
        $this->set(compact('permission'));
        $this->set('_serialize', ['permission']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Permission id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $permission = $this->Permissions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $permission = $this->Permissions->patchEntity($permission, $this->request->getData());
            if ($this->Permissions->save($permission)) {
                $this->Flash->success(__('The permission has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The permission could not be saved. Please, try again.'));
        }
        $this->set(compact('permission'));
        $this->set('_serialize', ['permission']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Permission id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function assignpermission($group_id = null,$action = null,$permission=null)
    {
    	$this->autoRender = false;
    	$group_id=base64_decode($group_id);
    	$permission=base64_decode($permission);
    	$this->Acl->$action($group_id, $permission);
		return $this->redirect(['action' => 'index']);
    }
}
