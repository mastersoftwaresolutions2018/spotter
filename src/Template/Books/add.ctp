
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addBook']").validate({
    // Specify validation rules
    rules: {
      name: "required",
      isbn_no: "required",
      category_id: "required",
      author_name: "required",
      pages: "required",
      publisher_name: "required",
      description: "required",
      country: "required",
    },
    // Specify validation error messages
    messages: {
      name: "Book name is required.",
      isbn_no: "ISBN no. is required",
      category_id: "Category is required",
      author_name: "Author name is required",
      pages: {
        required:"Pages is required",
        pattern : "Only number is allowed"
      },
      publisher_name: "Publisher Name is required",
      description: "Book description is required.",
      country: "Country is required.",
    },
    submitHandler: function(form) {
        var name    = $("#name").val();
        var isbn_no = $("#isbn_no").val();
        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/books/getifnameExist1' ?>",
           data: { name: name }, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#name-error").length == 0){ 
                        $("#name").after('<div id="name-error" class="form-control-feedback" style="color:#f4516c">Book Name alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#name-error").html("Book Name alreay exists. Please choose another.");
                    }
                }else{
                    $.ajax({
                      type: "POST",
                      url: "<?php echo HTTP_ROOT.'admin/books/validateIsbn1' ?>",
                      data: { isbn_no: isbn_no },
                      success: function(response1)
                      {
                        if(response1 > 0){
                          if($("#isbn_no-error").length == 0){ 
                              $("#isbn_no").after('<div id="isbn_no-error" class="form-control-feedback" style="color:#f4516c">ISBN no. alreay exists. Please choose another.</div>');
                          }else{            
                              $("#isbn_no-error").html("ISBN no. alreay exists. Please choose another.");
                          }
                        }else{
                          form.submit();
                        }
                      }
                    });
                    
                }
            }
        });
    }
  });
}); 

</script>
 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add Book') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <!-- <a href="<?php echo HTTP_ROOT.'admin/books'; ?>">
                    <?php echo __('Back') ?>
                   </a> -->
                   <a href="<?php echo HTTP_ROOT.'admin/books'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Back
                    </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT ?>books/add" enctype="multipart/form-data" name="addBook" class="m-form m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Name</label> 
            <input type="text" name="name" class="form-control m-input m-input--square" id="name" value="<?php echo @$data['name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Isbn No.</label> 
            <input type="text" name="isbn_no" class="form-control m-input m-input--square" id="isbn_no" value="<?php echo @$data['isbn_no']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input select required">
            <label for="category_id">Select Category</label> 
            <select class="form-control m-input m-input--square" name="category_id">
              <option>Select Category</option>
              <?php foreach ($Categories as $key => $value) { ?>
                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="author_name">Author Name</label> 
            <input type="text" name="author_name" class="form-control m-input m-input--square" id="author_name" value="<?php echo @$data['author_name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="pages">No. of pages</label> 
            <input type="text" name="pages" class="form-control m-input m-input--square" id="pages" value="<?php echo @$data['pages']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="publisher_name">Publisher's Name</label> 
            <input type="text" name="publisher_name" class="form-control m-input m-input--square" id="publisher_name" value="<?php echo @$data['publisher_name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Description</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="description"><?php echo @$data['description']; ?></textarea>
          </div>        
        </div>
        <div class="form-group m-form__group">
          <label for="name">Cover Image</label>
            <input type="file" class ='form-control m-input m-input--square' name="cover_photo">
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="country">Country</label> 
            <input type="text" name="country" class="form-control m-input m-input--square" id="country" value="<?php echo @$data['country']; ?>">
          </div>   
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
