 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='editBook']").validate({
    // Specify validation rules
    rules: {
      name: "required",
      isbn_no: "required",
      category_id: "required",
      author_name: "required",
      pages: "required",
      publisher_name: "required",
      description: "required",
    },
    // Specify validation error messages
    messages: {
      name: "Book name is required.",
      isbn_no: "ISBN no. is required",
      category_id: "Category is required",
      author_name: "Author name is required",
      pages: {
        required:"Pages is required",
        pattern : "Only number is allowed"
      },
      publisher_name: "Publisher Name is required",
      description: "Book description is required.",
    },
    submitHandler: function(form) {
        var id      = "<?php echo @$book['id']; ?>";
        var name    = $("#name").val();

        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/books/getifnameExistTwice1' ?>",
           data: { name: name, id : id}, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#name-error").length == 0){ 
                        $("#name").after('<div id="name-error" class="form-control-feedback" style="color:#f4516c">Book Name alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#name-error").html("Book Name alreay exists. Please choose another.");
                    }
                }else{
                    form.submit();
                }
            }
        });
    }
  });
}); 

</script>
<div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Edit Book') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <a href="<?php echo HTTP_ROOT.'admin/books'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    <?php echo __('Back') ?>
                   </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT.'books/edit/'.@$book['id'] ?>" enctype="multipart/form-data" name="editBook" class="m-form m-form--fit m-form--label-align-right">
    <input type="hidden" name="id" value="<?php echo @$book['id']; ?>">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Name</label> 
            <input type="text" name="name" class="form-control m-input m-input--square" id="name" value="<?php echo @$book['name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Isbn No.</label> 
            <input type="text" name="isbn_no" class="form-control m-input m-input--square" id="name" value="<?php echo @$book['isbn_no']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input select required">
            <label for="category_id">Select Category</label> 
            <select class="form-control m-input m-input--square" id="category_id" name="category_id">
              <option value="">Select Category</option>
              <?php foreach ($Categories as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?php if(@$book['category_id']== $key) { echo "selected"; }  ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="author_name">Author Name</label> 
            <input type="text" name="author_name" class="form-control m-input m-input--square" id="author_name" value="<?php echo @$book['author_name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="pages">No. of pages</label> 
            <input type="text" name="pages" class="form-control m-input m-input--square" id="pages" value="<?php echo @$book['pages']; ?>" pattern="\d*" minlength="1" maxlength="6">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="publisher_name">Publisher's Name</label> 
            <input type="text" name="publisher_name" class="form-control m-input m-input--square" id="publisher_name" value="<?php echo @$book['publisher_name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Description</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="description"><?php echo @$book['description']; ?></textarea>
          </div>        
        </div>

        <div class="form-group m-form__group">
            <img src="<?php echo HTTP_ROOT.''.@$book['cover_photo']; ?>" style="max-width: 150px; max-height: 150px;">
        </div>

        <div class="form-group m-form__group">
          <label for="name">Cover Image</label>
            <input type="file" class ='form-control m-input m-input--square' name="cover_photo">
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="country">Country</label> 
            <input type="text" name="country" class="form-control m-input m-input--square" id="country" value="<?php echo @$book['country']; ?>">
          </div>   
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
