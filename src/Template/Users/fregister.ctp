
<section class="serach-result-main signuppage">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="profile-section">
				<div class="col-sm-12 noppading">
						<div class="profile-right-content clearfix">
				        	<div class="login-popup">
				        		
				        		<div class="col-sm-6 noppading">
                        <form id="form1"  role="form">
					        			<div class="content-right">
					        			<!-- <div class="logo-mod"><img src="<?php echo $this->request->webroot; ?>images/logo.png" class="img-responsive"></div> -->
					        			<div class="bigform-layout">
					        				<h1>Not yet a member? Sign up for free! </h1>
					        				<!-- <a href="<?php //echo $this->request->webroot; ?>users/login?provider=Facebook">Login with Facebook</a> -->
<!-- 					        				<button class="signbtn-blue">Connect With Facebook</button>
 -->
					        				<a  class="signbtn-blue" href="<?php echo $this->request->webroot; ?>users/login?provider=Facebook">Connect With Facebook</a>



					        				<p>For a more trusted profile, enable access to friends list.<br> This ONLY displays number of friends. </p>

					        				<div class="response-msg"></div>
					        			    <div class="orimg"><img src="<?php echo $this->request->webroot; ?>images/orimg.png" class="img-responsive"></div>

					        			    <div class="col-sm-6 right-t">
						        			    <div class="group">      
											      <input name="name" type="text" >
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Name</label>
											    </div>
										    </div>
										     <div class="col-sm-6 right-t " >
						        			    <div class="group">      
											      <input name="phone" type="text" >
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Phone Number</label>
											    </div>
										    </div>
										     <div class="col-sm-6 left-t date" id="datetimepicker1">
						        			    <div class="group">      
 											       <input type='text' name="dob"    />
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Birth Year</label>
											    </div>
										    </div>
										     <div class="col-sm-12">
						        			    <div class="group">      
											      <input name="email"  type="email" >
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Email Address</label>
											    </div>
										    </div>
										    <div class="col-sm-6 right-t">
						        			    <div class="group">      
											      <input name="password" id="password"  type="password" >
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Password</label>
											    </div>
										    </div>
										    <div class="col-sm-6 left-t">
						        			    <div class="group">      
											      <input name="confirm_password" type="password" >
											      <span class="highlight"></span>
											      <span class="bar"></span>
											      <label>Confirm Password</label>
											    </div>
										    </div>
										    <div class="col-sm-12">
										    	<p><input type="checkbox">  Receive updates on new features, or useful information for you</p>
										    </div>
										    <button class="signbtn">SIGN UP NOW</button>
										    <p>By clicking on “Connect with Facebook” or "Get started now" you certify that you have read<br> and accept our T&Cs and are over 18 years old</p>
										    </div>
										</div>
				        			</form>
				        		</div>
				        		<div class="col-sm-6 noppading">
				        			<div class="bg-img">
				        				<div class="right-slidercoursl">
				        				<div class="sign-clock">
				        					<p><a href="#">Already have an account?</a></p>
				        					<button class="signbtn-signup">sign up</button>
				        				</div>
				        					  <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
												    <!-- Indicators -->
												    <ol class="carousel-indicators">
												      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
												      <li data-target="#myCarousel" data-slide-to="1"></li>
												      <li data-target="#myCarousel" data-slide-to="2"></li>
												      <li data-target="#myCarousel" data-slide-to="3"></li>
												    </ol>

												    <!-- Wrapper for slides -->
												    <div class="carousel-inner" role="listbox">
												      <div class="item active">
												        <h1>Thousands of boats available every day. <br><span class="text-orange">Where do you want to go?</span></h1>
												      </div>
												      <div class="item">
												        <h1>Thousands of boats available every day. <br><span class="text-orange">Where do you want to go?</span></h1>
												      </div>
												      <div class="item">
												        <h1>Thousands of boats available every day. <br><span class="text-orange">Where do you want to go?</span></h1>
												      </div>
												      <div class="item">
												        <h1>Thousands of boats available every day. <br><span class="text-orange">Where do you want to go?</span></h1>
												      </div>
												    </div>

												  </div>
				        				</div>
				        			</div>
				        		</div>
				        	</div>
							
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

 

 <script type="text/javascript">
     $(function () {
   var bindDatePicker = function() {
        $(".date").datetimepicker({
        format:'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        }).find('input:first').on("blur",function () {
            // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
            // update the format if it's yyyy-mm-dd
            var date = parseDate($(this).val());

            if (! isValidDate(date)) {
                //create date based on momentjs (we have that)
                date = moment().format('YYYY-MM-DD');
            }

            $(this).val(date);
        });
    }
   
   var isValidDate = function(value, format) {
        format = format || false;
        // lets parse the date to the best of our knowledge
        if (format) {
            value = parseDate(value);
        }

        var timestamp = Date.parse(value);

        return isNaN(timestamp) == false;
   }
   
   var parseDate = function(value) {
        var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
        if (m)
            value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

        return value;
   }
   
   bindDatePicker();
 });
</script>

  
  


