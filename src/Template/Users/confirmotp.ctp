 <div class="users form large-9 medium-8 columns content">
<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?php echo __('Forgot password') ?>
            </h3>
        </div>
    </div>
</div>
<!--begin::Form-->

<form method="post" accept-charset="utf-8" class="m-form m-form--fit m-form--label-align-right" action="<?php echo HTTP_ROOT.'users/confirmotp' ?>">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">            
            <div class="input text">
                <label for="email">Enter OTP</label>
                <input name="token" class="form-control m-input m-input--square" id="token" type="text" required="">
            </div>             
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->button('Login', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
</form>
<!--end::Form-->
</div>


    
</div>
