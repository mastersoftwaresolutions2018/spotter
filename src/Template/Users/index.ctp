<!-- <script type="text/javascript">
    $(document).ready(function(){
        $('.limit').on('change',function(){
            $('#limit').submit();
        });
    });
</script> -->
<div class="users index large-9 medium-8 columns content">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Users
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">  
            <i class="fa fa-users" style="font-size: 24px">  </i> &nbsp;&nbsp; 
            <i style="font-size: 18px"><?= $this->Paginator->counter('{{count}}'); ?></i>  Users      
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <?php /*
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-9">
                            <!-- <div class="m-input-icon m-input-icon--left"> -->
                            <form method="get" action="<?php echo HTTP_ROOT.'users/index'; ?>" style="float: left" class="m-input-icon m-input-icon--left">
                            <div class="m-input-icon m-input-icon--left col-md-6">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search Users" id="generalSearch" name="query" value="<?php echo @$_GET['query'] ?>" required>
                                    <!-- <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <button type="submit" class="serch"><i class="la la-search"></i></button>
                                         <button type="submit" class="btn btn-success">
                                        Filter
                                    </button>
                                    </span> -->

                                </div>    
                                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                            <div class="m-input-icon m-input-icon--left">
                                                <button type="submit" class="btn btn-success">
                                                    Filter
                                                </button>
                                                <a href="<?php echo HTTP_ROOT.'admin/books' ?>" style="position: absolute; padding-left: 5%">
                                                <button type="button" class="btn btn-primary">
                                                        Reset
                                                    </button> </a>
                                            </div>
                                    </div>

                                    <!-- </div> -->
                                </form>

                                <a href="<?php echo HTTP_ROOT.'admin/users' ?>" style="position: absolute; padding-left: 5%">
                                <button type="button" class="btn btn-primary">
                                        Reset
                                    </button>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="<?php echo HTTP_ROOT.'admin/users/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="m-tooltip" title="" data-original-title="Add User">
                    Add User
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
    */?>

            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <form method="get" action="<?php echo HTTP_ROOT.'users/index' ?>">
                    <div class="form-group m-form__group row align-items-center">
                        
                        <div class="col-md-4">
                            <div class="m-form__group m-form__group--inline">
                                <!-- <div class="m-form__label">
                                    <label class="m-label m-label--single">
                                    Search Users:
                                    </label>
                                </div> -->
                                <div class="m-form__control">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search Users" id="generalSearch" name="query" value="<?php echo @$_GET['query'] ?>" required>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <button type="submit" class="btn btn-success">
                                    Filter
                                </button>
                                <a href="<?php echo HTTP_ROOT.'admin/users' ?>" style="position: absolute; padding-left: 5%">
                                <button type="button" class="btn btn-primary">
                                        Reset
                                    </button> </a>
                                
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>                     
                        
                        </div>
                        <div class="col-md-4 order-1 order-xl-2 m--align-right">
                            <a href="<?php echo HTTP_ROOT.'admin/users/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            Add User
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                    <!-- <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-6">
                            <div class="m-input-icon m-input-icon--left">
                                <button type="submit" class="btn btn-success">
                                    Filter
                                </button>
                                <a href="<?php echo HTTP_ROOT.'admin/users' ?>" style="position: absolute; padding-left: 5%">
                                <button type="button" class="btn btn-primary">
                                        Reset
                                    </button> </a>
                            </div>
                        </div>
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-4 order-1 order-xl-2 m--align-right">
                            <a href="<?php echo HTTP_ROOT.'admin/users/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            Add User
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>  -->                 
                    </form>

                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('firstname', 'Name'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('address', 'Address'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('email', 'Email'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('gender', 'Gender'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('status', 'Status'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>                        
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
            <?php
            if(count($users)){
                $i = 1;
                foreach ($users as $key => $user){ ?>

                    <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo $key; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo h($user->firstname." ".$user->lastname); ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php echo $user->address; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php echo $user->email; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php echo $user->gender; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php if($user->status == 0){ echo "Disabled"; }else{ echo "Enable"; } ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell">
                        <span style="width: 110px;" >
                            <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $user->id), array('escape' => false, 'data-toggle'=> 'm-tooltip','data-original-title'=> 'Edit'.$user->name )) ?>
                            <a href="<?php echo HTTP_ROOT.'admin/users/delete/'.$user->id; ?>" data-offset="-20px -20px" data-toggle="m-tooltip" title="" data-original-title="Delete <?php echo $user->name; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a>

                            
                            </span>
                        </td>                      
                    </tr>
    <?php       $i++;
                }
            }else{ ?>
                <tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
                        <td data-field="
                            Order ID
                            " class="m-datatable__cell"><span style="width: 100%;">
                            No user found.
                            </span>
                        </td>
                
                </tr>
    <?php   } ?>      
                </tbody>
            </table>
            <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                <ul class="m-datatable__pager-nav">
                    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
                    <?= $this->Paginator->numbers(); ?>
                    <?= $this->Paginator->next(__('Next') . ' >'); ?>
                </ul>
                <div class="m-datatable__pager-info">
                    <div class="btn-group bootstrap-select m-datatable__pager-size" style="width: 70px;">                    
                        <?php echo $this->Paginator->limitControl(); ?>
                    </div>
                    <span class="m-datatable__pager-detail"><?= $this->Paginator->counter('Displaying {{start}} - {{end}} of {{count}} records'); ?></span>
                </div>
            </div>
        </div>
        <!--end: Datatable -->
    </div>
</div>

<?php /* 
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next(__('Next') . ' >'); ?>

  </ul>

  
    <p><?= $this->Paginator->counter(); ?></p>
</nav>
 */ ?>


</div>
