<?php
if(@$data['dob']){
    $data['dob'] = $newDate = date("d/m/Y", strtotime($data['dob']));;
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNRgyO6gMXL4jBwtm_bG8cJIXqj9o_X1E&sensor=false&libraries=places"></script> 

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addUser']").validate({
    // Specify validation rules
    rules: {
      email: "required",
      firstname: "required",
      lastname: "required",
      about: "required",
      gender: "required",
      address: "required",
      city: "required",
      state: "required",
      country: "required",
      zipcode: "required",
      dob: "required",
      password: "required",
    },
    // Specify validation error messages
    messages: {
      email: "Email is required.",
      firstname: "First name is required",
      lastname: "Last name is required",
      about: "About is required",
      zipcode: {
        required:"Zipcode is required",
        pattern : "Only number is allowed"
      },
      gender: "Gender is required",
      address: "Address is required.",
      city: "City is required.",
      state: "State is required.",
      country: "Country is required.",
      password: "password is required.",
      dob: "dob is required.",
    },
    submitHandler: function(form) {
        var email    = $("#email").val();

        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/users/getifusernameexist1' ?>",
           data: { email: email}, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#email-error").length == 0){ 
                        $("#email").after('<div id="email-error" class="form-control-feedback" style="color:#f4516c">Email alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#email-error").html("Email alreay exists. Please choose another.");
                    }
                }else{
                    form.submit();
                }
            }
        });
    }
  });
}); 

</script>
<div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add User') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
            <h3 class="m-portlet__head-text">
               <a href="<?php echo HTTP_ROOT.'admin/users'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <?php echo __('Back') ?>
               </a>
            </h3>
        </div>
    </div>
</div>
<!--begin::Form-->

<?= $this->Form->create($user,['name'=> 'addUser', 'class'=>'m-form m-form--fit m-form--label-align-right']) ?>
    <div class="m-portlet__body">
    <input type="hidden" name="group_id" value="7">
    <input type="hidden" name="role_id" value="2">
    <input type="hidden" name="active" value="1">
    <?php /*
        <div class="form-group m-form__group">
            
            <?php 
            echo $this->Form->input('group_id', ['options' => $groups,'class'=>'form-control m-input m-input--square']);
            ?>
            
        </div>

        <div class="form-group m-form__group">            
            <?php 
            echo $this->Form->input('role_id', ['options' => $roles,'class'=>'form-control m-input m-input--square']);
            ?>            
        </div>
        */ ?>
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('firstname',['class' => 'form-control m-input m-input--square']);?>             
        </div>

        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('lastname',['class' => 'form-control m-input m-input--square']);?>             
        </div>
        
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('email',['label'=> 'Email','class' => 'form-control m-input m-input--square','type'=> 'email']);?>             
        </div>
        <div class="form-group m-form__group">            
            <div class="input select">
                <label for="gender">Gender</label>
                <select name="gender" class="form-control m-input m-input--square" id="gender">
                <option value="male" <?php if(@$data['gender']=='male'){ echo "selected";  } ?>>Male</option>
                <option value="female" <?php if(@$data['gender']=='female'){ echo "selected";  } ?>>Female</option>
                </select>
            </div>             
        </div>
        
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('about',['class' => 'form-control m-input m-input--square', 'rows'=>5]);?>             
        </div>

        <div class="form-group m-form__group">  
          <input id="searchInput" class="input-controls" type="text" placeholder="Enter a location">

          <div class="map" id="map" style="width: 100%; height: 300px;"></div>
        </div>

         <div class=" form-group m-form__group form_area">
            <input type="text" name="lat" id="lat" value="<?php echo @$data['lat']?>" placeholder="Enter Lattitude">
            <input type="text" name="lng" id="lng" value="<?php echo @$data['lng']; ?>" placeholder="Enter Longitude">
         </div>

        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('address',['class' => 'form-control m-input m-input--square']);?>             
        </div>
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('city',['class' => 'form-control m-input m-input--square']);?>             
        </div>
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('state',['class' => 'form-control m-input m-input--square']);?>             
        </div>
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('country',['class' => 'form-control m-input m-input--square']);?>             
        </div>
        
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('zipcode',['type'=> 'text', 'class' => 'form-control m-input m-input--square','pattern'=>'\d*', 'minlength'=> 1, 'maxlength'=> 6]);?>   
        </div>
        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('dob',['readonly','type'=> 'text', 'id'=> 'dob', 'class' => 'form-control m-input m-input--square']);?>             
        </div>
        <div class="form-group m-form__group">
            <?php echo $this->Form->input('password',['class' => 'form-control m-input m-input--square']);
            ?>
            
        </div>
        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
 <?= $this->Form->end() ?>
<!--end::Form-->
</div>


    
</div>

<script>
var lat = 28.5355161;
var lng = 77.39102649999995;
if ($("#lat").val()){
  lat = $("#lat").val();
}
if ($("#lng").val()){
  lng = $("#lng").val();
}
/* script */
function initialize() {
   var latlng = new google.maps.LatLng(lat,lng);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
    
        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);
       
    });
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
              bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });
}
function bindDataToForm(address,lat,lng){
   document.getElementById('address').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>