<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
    $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
    setTimeout(function(){
        document.getElementById('current_password').value = "";
        document.getElementById('password').value = "";
    }, 10);
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='editUser']").validate({
    // Specify validation rules
    rules: {
        email: "required",
        password : { 
                    required : true,
                    minlength : 5
                },
        confirmPassword : {
            minlength : 5,
            equalTo : "#password"
        }
      
    },
    // Specify validation error messages
    messages: {
      email: "Email is required.",
      password: {
        required: "Password is required",
        minlength: "Please enter minimum 5 character.", 
        },  
      confirmPassword:{
        minlength: "Please enter minimum 5 character.",
        equalTo: "Passwords does not match. Please check."
      }   
    },
    submitHandler: function(form) {
        var password    = $("#current_password").val();

        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/users/checkAdminpassword' ?>",
           data: { password: password}, // serializes the form's elements.
           success: function(response)
            {
                if(response=== "unmatched"){
                    if($("#current_password-error").length === 0){ 
                        $("#current_password").after('<div id="current_password-error" class="form-control-feedback" style="color:#f4516c">Current password is not correct. Please enter correct password.</div>');
                    }else{             
                        /*$("#current_password-error").html("Current password is not correct. Please enter correct password.");*/
                        $("#current_password").after('<div id="current_password-error" class="form-control-feedback" style="color:#f4516c">Current password is not correct. Please enter correct password.</div>');
                    }
                }else{
                    form.submit();
                }
            }
        });
    }
  });
}); 

</script>
<div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Edit User') ?>
            </h3>
        </div>
        <!-- <div class="m-portlet__head-title" style="float: right"> 
            <h3 class="m-portlet__head-text">
               <a href="<?php echo HTTP_ROOT.'admin/users'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <?php echo __('Back') ?>
               </a>
            </h3>
        </div> -->
    </div>
</div>
<!--begin::Form-->

<?= $this->Form->create($user,['class'=>'m-form m-form--fit m-form--label-align-right', 'name'=> 'editUser']) ?>
    <input type="hidden" name="id" value="<?php echo @$user['id']; ?>">
    <div class="m-portlet__body">

        <div class="form-group m-form__group">            
            <?php echo $this->Form->input('email',['class' => 'form-control m-input m-input--square','type'=> 'email']);?>             
        </div>
        <div class="form-group m-form__group">
            <?php echo $this->Form->input('current_password',['value'=>' ','type'=>'password', 'id'=>'current_password','class' => 'form-control m-input m-input--square']);
            ?>            
        </div>
        <div class="form-group m-form__group">
            <?php echo $this->Form->input('password',['id'=>'password','class' => 'form-control m-input m-input--square']);
            ?>            
        </div>
        <div class="form-group m-form__group">
            <?php echo $this->Form->input('confirmPassword',['type' =>'password','class' => 'form-control m-input m-input--square']);
            ?>            
        </div>
        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
 <?= $this->Form->end() ?>
<!--end::Form-->
</div>


    
</div>
