 <script type="text/javascript">
     // Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='resetpassword']").validate({
    // Specify validation rules
    rules: {      
        password: {
            required: true,
            minlength: 5
        },
        repassword: {
            required : {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
            equalTo: "#password",
            minlength: 5,
            maxlength: 20
        },
    },
    // Specify validation error messages
    messages: {
        password: {
        required: "Please enter password.",
        minlength: "Your password must be at least 5 characters long"
        },
        repassword: {
            required: "Please enter password.",
            minlength: "Your password must be at least 5 characters long",
            equalTo:  "Your passwords doesn't match."
        },
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

 </script>
 <div class="users form large-9 medium-8 columns content">
<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?php echo __('Reset password') ?>
            </h3>
        </div>
    </div>
</div>
<!--begin::Form-->
<?php
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$seg  = count($uri_segments);
$id= $uri_segments[$seg-1];
?>
<form method="post" accept-charset="utf-8" class="m-form m-form--fit m-form--label-align-right" action="<?php echo HTTP_ROOT.'users/resetpassword' ?>" name="resetpassword">
    <input type="hidden" value="<?php echo $id; ?>" name="token">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">            
            <div class="input text">
                <label for="password">Enter Password</label>
                <input name="password" class="form-control m-input m-input--square" id="password" type="password" required="" value="" placeholder="Enter Password">
            </div>             
        </div>  
        <div class="form-group m-form__group">            
            <div class="input text">
                <label for="password">Re-enter Password</label>
                <input name="repassword" class="form-control m-input m-input--square" id="repassword" type="password" required="" value="" placeholder="Enter Password">
            </div>             
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->button('Login', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
</form>
<!--end::Form-->
</div>


    
</div>
