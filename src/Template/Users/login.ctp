<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='loginForm']").validate({
    // Specify validation rules
    rules: {
      email: "required",
      password: "required",
    },
    // Specify validation error messages
    messages: {
      email: "Please enter Email.",
      password: "Please enter password.",
    },
    submitHandler: function(form) {
                form.submit();   
    }
  });
}); 

</script>
<div class="users form large-9 medium-8 columns content login_bx">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?php echo __('Please enter your email and password') ?>
            </h3>
        </div>
    </div>
</div>
<!--begin::Form-->

<?= $this->Form->create('',['name'=>'loginForm','class'=>'m-form m-form--fit m-form--label-align-right']) ?>
    <div class="m-portlet__body">


        <div class="form-group m-form__group">
            
            <?php echo $this->Form->input('email',['label'=>'Email' ,'class' => 'form-control m-input m-input--square']);?> 
            
        </div>


        
        <div class="form-group m-form__group">
            <?php echo $this->Form->input('password',['class' => 'form-control m-input m-input--square']);
            ?>
            
        </div>
        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            
            <?php echo $this->Form->button('Login', array('class' => 'btn btn-dark'));?>
            <?php /*echo   $this->Html->link(
 'Login with Facebook',
 ['controller' => 'Users', 'action' => 'login', '?' => ['provider' => 'Facebook']],['class' => 'btn btn-default']
);*/ ?>
            <?php echo   $this->Html->link(
             'Forgot Password',
             ['controller' => 'Users', 'action' => 'forgotpassword'],['class' => 'btn btn-default']
            ); ?>

        </div>
    </div>
 <?= $this->Form->end() ?>
<!--end::Form-->
</div>


    
</div>
