 
<div class="users view large-9 medium-8 columns content">
    
<div class="m-portlet">
    
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                     
                     <thead>
                     <tbody>
                        <tr>
                            <th><?= __('Group') ?></th>
                            <td><?= $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?>
                            </td>
                        </tr>


                        <tr>
                            <th><?= __('Role') ?></th>
                            <td><?= $user->has('role') ? $this->Html->link($user->role->name, ['controller' => 'Roles', 'action' => 'view', $user->role->id]) : '' ?></td> 
                        </tr>


                        <tr>
                            <th><?= __('Username') ?></th>
                            <td><?= h($user->username) ?></td>  
                        </tr>

                        

                        <tr>
                            <th><?= __('Email') ?></th>
                            <td><?= h($user->email) ?></td>
                        </tr>
                        
                        
                    </tbody>
                    </thead>    
                        
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>
</div>
