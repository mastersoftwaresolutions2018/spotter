<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CoreConfig $coreConfig
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Core Config'), ['action' => 'edit', $coreConfig->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Core Config'), ['action' => 'delete', $coreConfig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $coreConfig->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Core Configs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Core Config'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="coreConfigs view large-9 medium-8 columns content">
    <h3><?= h($coreConfig->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Config Id') ?></th>
            <td><?= h($coreConfig->config_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($coreConfig->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Config Data') ?></h4>
        <?= $this->Text->autoParagraph(h($coreConfig->config_data)); ?>
    </div>
</div>
