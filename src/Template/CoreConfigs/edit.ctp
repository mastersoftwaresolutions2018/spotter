<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CoreConfig $coreConfig
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $coreConfig->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $coreConfig->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Core Configs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="coreConfigs form large-9 medium-8 columns content">
    <?= $this->Form->create($coreConfig) ?>
    <fieldset>
        <legend><?= __('Edit Core Config') ?></legend>
        <?php
            echo $this->Form->control('config_id');
            echo $this->Form->control('config_data');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
