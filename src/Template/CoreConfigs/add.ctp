<div class="users form large-9 medium-8 columns content">
  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                    <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= __('Add Notification Configuration') ?>
                </h3>
            </div>
            <div class="m-portlet__head-title" style="float: right"> 
                    <h3 class="m-portlet__head-text">                      
                       <!-- <a href="<?php echo HTTP_ROOT.'admin/faqs'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        Back
                        </a> -->
                    </h3>
                </div> 
        </div>
    </div>    
    <!--begin::Form-->
    <?php //echo "<pre>"; print_r($add_workout_schedule); die(); ?> 
    <form method="post" action="<?php echo HTTP_ROOT ?>core_configs/add" enctype="multipart/form-data" name="addNotification" class="m-form m-form--fit m-form--label-align-right">
      <div class="m-portlet__body">
          <div class="form-group m-form__group">  
            <div class="input text required">
              <label for="name">Add Workout Schedule</label> 
              <textarea rows="4" class ='form-control m-input m-input--square' name="add_workout_schedule"><?php echo $add_workout_schedule['config_data']; ?></textarea>
              <p>
              Note : {username}, {gymname} and {time} are the dynamic fields. If you want to display, <br>
              1. Scheduled time of workout then you can use {time}.<br>
              2. Gym name then you can use {gymname}.<br>
              3. Username who scheduled workout then you can use {username}.
            </p> 
            </div>              
          </div>
                
          <div class="form-group m-form__group">
            <div class="input text required">
              <label for="name">Send Invite</label>   
              <textarea rows="4" class ='form-control m-input m-input--square' name="send_invite"><?php echo $send_invite['config_data']; ?></textarea>
              <p>
              Note : {username} is a dynamic field. If you want to display username who sent you friend request then you can use {username}.
            </p> 
            </div>        
          </div>
          
          <div class="form-group m-form__group">
            <div class="input text required">
              <label for="name">Accept Friend Request</label>   
              <textarea rows="4" class ='form-control m-input m-input--square' name="accept_friend_request"><?php echo $accept_friend_request['config_data']; ?></textarea>
              Note : {username} is a dynamic field. If you want to display username who accepted your friend request then you can use {username}.
            </div>        
          </div>

          <div class="form-group m-form__group">
            <div class="input text required">
              <label for="name">Recieve Message</label>   
              <textarea rows="4" class ='form-control m-input m-input--square' name="recieve_message"><?php echo $recieve_message['config_data']; ?></textarea>
              Note : {username} is a dynamic field. If you want to display username who sent you message then you can use {username}.
            </div>        
          </div>
      </div>
      <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions">            
              <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>              
          </div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("form[name='addNotification']").validate({
      // Specify validation rules
      rules: {
         add_workout_schedule: "required",
         send_friend_request: "required",
         accept_friend_request: "required",
         recieve_message: "required",
      },
      // Specify validation error messages
      messages: {
        add_workout_schedule: "Notification for add workout schedule is required.",
        send_friend_request: "Notification for send friend request is required.",
        accept_friend_request: "Notification for accept friend request is required.",
        recieve_message: "Notification for recieve unread messages is required.",
      }
    });
  });
</script>