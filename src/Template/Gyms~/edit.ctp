<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gym $gym
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gym->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gym->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Gyms'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="gyms form large-9 medium-8 columns content">
    <?= $this->Form->create($gym) ?>
    <fieldset>
        <legend><?= __('Edit Gym') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('owner');
            echo $this->Form->control('description');
            echo $this->Form->control('cover_photo');
            echo $this->Form->control('long');
            echo $this->Form->control('lat');
            echo $this->Form->control('location');
            echo $this->Form->control('country');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('rating');
            echo $this->Form->control('review_count');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
