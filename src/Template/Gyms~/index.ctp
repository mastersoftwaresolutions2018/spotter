<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gym[]|\Cake\Collection\CollectionInterface $gyms
 */
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.limit').on('change',function(){
            $('#limit').submit();
        });
    });
</script>
<div class="users index large-9 medium-8 columns content">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Manage Gyms
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">           
            <i class="fas fa-dumbbell" style="font-size: 24px">  </i> &nbsp;&nbsp; 
            <i style="font-size: 18px"><?= $this->Paginator->counter('{{count}}'); ?></i>  Gyms 
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">

                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('name', 'Name'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('owner', 'Gym Owner'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('location', 'Location'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>                        
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
                <?php $i=0; foreach ($gyms as $gym): ?>
                <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= $this->Number->format($gym->id) ?></span>
                    </td>
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= h($gym->name) ?></span>
                    </td>
                    <td class="m-datatable__cell"><span style="width: 110px;"><?= h($gym->owner) ?></span></td>
                    <td class="m-datatable__cell"><span style="width: 110px;"><?= h($gym->location) ?></span></td>            
                    <td class="m-datatable__cell"><span style="width: 110px;">
                        <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $gym->id), array('escape' => false)) ?>
                            <a href="<?php echo HTTP_ROOT.'admin/books/delete/'.$gym->id; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a>
                            </span>
                        </td>  
                    </tr>
                <?php $i++; endforeach; ?> 
                </tbody>
            </table>
            <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                <ul class="m-datatable__pager-nav">
                    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
                    <?= $this->Paginator->numbers(); ?>
                    <?= $this->Paginator->next(__('Next') . ' >'); ?>
                </ul>
                <div class="m-datatable__pager-info">
                    <div class="btn-group bootstrap-select m-datatable__pager-size" style="width: 70px;">
                                              
                        <!-- <form method="get" action="<?php echo HTTP_ROOT.'admin/categories/index' ?>" id="limit">
                        <select class="selectpicker m-datatable__pager-size limit" title="Select page size" data-width="70px" tabindex="-98" name="limit" id="dropdown" data-selected="<?php if(@$_GET['limit']){ echo $_GET['limit'];}else{ echo "10"; } ?>">
                            <option value="10" <?php if(@$_GET['limit'] == 10){ echo "selected";} ?> >10</option>
                            <option value="20" <?php if(@$_GET['limit'] == 20){ echo "selected";} ?> >20</option>
                            <option value="30" <?php if(@$_GET['limit'] == 30){ echo "selected";} ?> >30</option>
                            <option value="50" <?php if(@$_GET['limit'] == 50){ echo "selected";} ?> >50</option>
                            <option value="100" <?php if(@$_GET['limit'] == 100){ echo "selected";} ?> >100</option>
                        </select>
                        </form> -->
                        <?php echo $this->Paginator->limitControl(); ?>
                    </div>
                    <span class="m-datatable__pager-detail"><?= $this->Paginator->counter('Displaying {{start}} - {{end}} of {{count}} records'); ?></span>
                </div>
            </div>
        </div>
        <!--end: Datatable -->
    </div>
</div>
</div>











