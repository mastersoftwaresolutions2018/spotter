<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>


 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add New Gym') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                    <a href="<?php echo HTTP_ROOT.'admin/gyms'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Back
                    </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT ?>gyms/add" enctype="multipart/form-data" name="addBook" class="m-form m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Name</label> 
            <input type="text" name="name" class="form-control m-input m-input--square" id="name" value="<?php echo @$data['name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="author_name">Gym Owner</label> 
            <input type="text" name="owner" class="form-control m-input m-input--square" id="owner" value="<?php echo @$data['owner']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Description</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="description"><?php echo @$data['description']; ?></textarea>
          </div>        
        </div>
        <div class="form-group m-form__group">
          <label for="name">Cover Image</label>
            <input type="file" class ='form-control m-input m-input--square' name="cover_photo">
        </div>
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="country">Country</label> 
            <input type="text" name="country" class="form-control m-input m-input--square" id="country" value="<?php echo @$data['country']; ?>">
          </div>   
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
