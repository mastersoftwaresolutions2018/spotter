<?php echo $this->element('fdashboardmenu'); ?>
<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="profile-section">
				<div class="col-sm-12 noppading">
<?php echo $this->element('profilesidebar', [
    "userdata" => $userinfo
]);
 //echo "<pre>";print_r($userinfo);die;

 ?>					<div class="col-sm-9">
						<div class="profile-right-content clearfix">
							<div class="heading-text-profile-right">
							<?= $this->Flash->render() ?>

								<h3>Your profile photo</h3>
									<p>Add your photo now! Other members will be reassured to see who they'll be travelling with, and you'll find your boat share much more easily. Photos also help members to recognise each other</p>
									<div class="photo-profile clearfix">
										<div class="verification-blocks clearfix">
											<div class="col-sm-9 noppading">
												<div class="img-blocks-s">
													<img src="<?php echo $this->request->webroot; ?>images/verificastion-icon1.png">
												</div>
												<div class="right-text-tt">
													<h4>Email verified</h4>
													<p>Your email address is: <span class="text-red"><?php echo $userinfo['email']  ?></span><br>
													Now that you've verified your email address, we can easily contact you if needed. 
													</p>
												</div>
											</div>
											<div class="col-sm-3 noppading text-right">
												<button class="edit" onclick="edit_email(<?php echo $userinfo['id'];?>)" ><i class="fa fa-pencil" aria-hidden="true"></i> edit</button>

												 


												

												<?php if ($userinfo['email_verified']=='1') { ?>
												<p class="text-green"><i class="fa fa-check" aria-hidden="true"></i> Verified</p>

 												<?php }else{ ?>
 												<p class="text-green"><li ><?= $this->Html->link('Verify Email',['controller'=>'profile','action'=>'sendverify']);?> </li></p>

													<?php }?>
												
											</div>
										</div>
										<hr>
										<div class="verification-blocks clearfix">
											<div class="col-sm-9 noppading">
												<div class="img-blocks-s">
													<img src="<?php echo $this->request->webroot; ?>images/verificastion-icon2.png">
												</div>
												<div class="right-text-tt">
													<h4>Phone verified </h4>
													<p>Your phone number is: <span class="text-red"><?php echo $userinfo['phone']  ?></span><br>
													Now that you've verified your phone number, you can arrange the details of your rides more easily. 
													</p>
												</div>
											</div>
											<div class="col-sm-3 noppading text-right">
												<button class="edit" onclick="edit_phone(<?php echo $userinfo['id'];?>)" ><i class="fa fa-pencil" aria-hidden="true"></i> edit</button>

                                                <?php if ($userinfo['phone_verified']=='1') { ?>
                                                <p class="text-green"><i class="fa fa-check" aria-hidden="true"></i> Verified</p>

                                                <?php } ?>

											</div>
										</div>
										<hr>
										<div class="verification-blocks clearfix">
											<div class="col-sm-9 noppading">
												<div class="img-blocks-s">
													<img src="<?php echo $this->request->webroot; ?>images/verificastion-icon3.png">
												</div>
												<div class="right-text-tt">
													<h4>Please connect your Facebook account</h4>
													<p>Connect your account so other members can see how many Facebook friends you have, and build trust on your profile.  
													</p>
												</div>
											</div>
											<div class="col-sm-3 noppading text-right">
												<button class="connect-myacc">Connect my account</button>
											</div>
										</div>
										<hr>
										<div class="verification-blocks clearfix">
											<div class="col-sm-9 noppading">
												<div class="img-blocks-s">
													<img src="<?php echo $this->request->webroot; ?>images/verificastion-icon4.png">
												</div>
												<div class="right-text-tt">
													<h4>Please read and accept the Member's Agreement </h4>
													<p>Accept our agreement and show the community you're a trusted member. 
													</p>
												</div>
											</div>
											<div class="col-sm-3 noppading text-right">
												<button class="read-accp">Read and accept</button>
											</div>
										</div>
									</div> 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	    function edit_email(id) {
        //$("#insertid_update").val(id);
        $.ajax({
            url: 'edgetuserdetail',
            type: 'get',
            data: 'id=' + id,
            success: function(data) {
                var response = JSON.parse(data);
                console.log(response);
                $("#email").val(response.email);
                $("#id").val(response.id);
               // $("#email_verified").val(response.email_verified);
                $('#modal_form_edit').modal('show');
            }
        });
    }

     function edit_phone(id) {
        //$("#insertid_update").val(id);
        $.ajax({
            url: 'edgetuserdetail',
            type: 'get',
            data: 'id=' + id,
            success: function(data) {
                var response = JSON.parse(data);
                ///console.log(response);
               // alert(response.id);
                $("#phone").val(response.phone);
                $("#idphone").val(response.id);
                $('#modal_form_edit_phone').modal('show');
 
            }
        });
    }

     

</script>
<div class="modal fade" id="modal_form_edit" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Personal Detail</h5>
            </div>
               <center><div id="update_lans"  class="isa_success" style="display:none " >
                <i class="fa fa-check"></i>
                </div></center>
            <div id='checkloading' style='display: none; position: relative; height: 70%; z-index: 999;text-align: center;  margin: 0px auto; padding: 0 0 20px 0'>
                <img src='http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif' width='50' height='50' style='vertical-align: middle; margin-right: 20px;' />Loading...
            </div>
            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="form_verificationemail" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">User Email</label>
                        <div class="col-xs-5">
                            <input type="text" id="email" class="form-control" name="email" />
                        </div>
                    </div>
                    <input type="hidden" name="id_of_user" id="id">
                    <input type="hidden" name="email_verified"  value="0">
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="display:none;">
    <a href="javascript:void(0)" id="callinsertbanner_update"></a>
</div>


<div class="modal fade" id="modal_form_edit_phone" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Personal Detail</h5>
            </div>
               <center><div id="update_lans"  class="isa_success" style="display:none " >
                <i class="fa fa-check"></i>
                </div></center>
            <div id='checkloading' style='display: none; position: relative; height: 70%; z-index: 999;text-align: center;  margin: 0px auto; padding: 0 0 20px 0'>
                <img src='http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif' width='50' height='50' style='vertical-align: middle; margin-right: 20px;' />Loading...
            </div>
            <div class="modal-body">

            <?php echo $this->element('phoneverify'); ?>


                <!-- The form is placed inside the body of modal -->
                <!-- <form id="form_verificationphone" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">User Phone</label>
                        <div class="col-xs-5">
                            <input type="text" id="phone" class="form-control" name="phone" />
                        </div>
                    </div>
                    <input type="hidden" name="id_of_user" id="idphone">
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form> -->
            </div>
        </div>
    </div>
</div>
<div style="display:none;">
    <a href="javascript:void(0)" id="callinsertbanner_update"></a>
</div>


 