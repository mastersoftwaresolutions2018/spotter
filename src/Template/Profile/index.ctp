<?php echo $this->element('fdashboardmenu'); 


if(empty($userinfo['gender'])){
	$checked='';
	$checkedfemale='';

}elseif ($userinfo['gender']=='male') {

$checked='checked="checked"';	
$checkedfemale='';	
 
}elseif ($userinfo['gender']=='female') {
	$checkedfemale='checked="checked"';	
	$checked='';	

}


?>
<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="profile-section">
				<div class="col-sm-12 noppading">
				<?php echo $this->element('profilesidebar', ["userdata" => $userinfo]); ?>
					<div class="col-sm-9">
						<div class="profile-right-content clearfix">
							<div class="heading-text-profile-right">
								<h3>Personal Information</h3>
								<form class="funkyradio" id="profileupdate">
									<div class="col-sm-12 noppading">
										<p>Gender</p>
										<div class="funkyradio-danger">
								            <input name="radio" value="female" id="radio1" <?php echo $checkedfemale;  ?>   type="radio">
								            <label for="radio1">Female</label>
								        </div>
								        <div class="funkyradio-danger">
								            <input name="radio" value="male" id="radio2"  <?php echo $checked;  ?>  type="radio">
								            <label for="radio2">Male</label>
								        </div>
									</div>
									<div class="col-sm-6 noppading right-t">
										<p>First Name</p>
										<input type="text" name="firstname" placeholder="Silvia" value="<?php echo $userinfo['firstname']  ?>">

										<input name="id_of_user" id="radio1" value="<?php echo $userinfo['id']  ?>" type="hidden">

									</div>
									<div class="col-sm-6 noppading left-t">
										<p>Last Name</p>
										<input type="text" name="lastname" placeholder="Tegorr" value="<?php echo $userinfo['lastname'] ?>">
									</div>
									<!-- <div class="col-sm-6 noppading right-t">
										<p>Email Address</p>
										<input type="text" name="email" placeholder="silvia123@kiboat.com" value="<?php //echo $userinfo['email'] ?>">
									</div> -->
									<!-- <div class="col-sm-6 noppading left-t mm-t">
										<p>Phone Number</p>
										<input type="text" name="phone" placeholder="+01 123-4567-890" value="<?php //echo $userinfo['phone'] ?>">
										<div class="checkkk">
											<div class="funkyradio-danger">
								            <input name="checkbox" id="checkbox99" checked="" type="checkbox">
								            <label for="checkbox99">Never show my phone number publicly </label>
								        </div>
										</div>
									</div> -->
									<div class="col-sm-6 noppading">
									<p>Birth Year</p>
										<!-- <p>Birth Year</p>
										<select class="">
										    <option>1994</option>
										    <option>1995</option>
										    <option>3</option>
										    <option>4</option>
										  </select> -->
										  										<input  name ="dob" class="form-control" type="text" placeholder="click to show datepicker"   id="example1">
										  										<div class="input-calendericons"><i class="fa fa-calendar" aria-hidden="true"></i></div>

									</div>
									<div class="col-sm-12 noppading biodescrip">
										<!-- <h4 class="text-red">Bio</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum massa eu accumsan malesuada. Cras non aliquet ligula. Vivamus urna eros, porta nec ornare eget, euismod eget felis. Vivamus commodo placerat justo eu efficitur. Proin lobortis aliquam orci, et imperdiet urna pulvinar eu. Sed ut ipsum erat. Praesent orci arcu, sagittis consectetur mi in, commodo varius orci. Proin lacinia turpis magna, at semper nisl finibus quis. Vivamus quis facilisis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum massa eu accumsan malesuada. Cras non aliquet ligula. tur mi in, commodo varius orci. nibh. </p>
										<hr> -->
										<div class="col-sm-12 noppading textar">
													<p>What could we improve?</p>
												 <textarea name="bio" rows="5" placeholder="Enter your text"><?php echo $userinfo['bio'] ?></textarea>
											</div>


										<div class="col-cen-d">
											<p class="text-red">Please do not incluide</p>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon1.png"> Phone number</p>
												</div>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon2.png"> Facebook account details</p>
												</div>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon3.png"> Details about specific rides</p>
												</div>
										</div>
									</div>
									<div class="col-sm-12 noppading clearfix text-right">
							<button class="savebutton">save</button>
							</div>
								</form>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function () {
$("#example1").datepicker({ format: "yyyy-mm-dd"}).datepicker("setDate","<?php echo date('Y-m-d',strtotime($userinfo['dob']))  ?>");
});
</script>

 