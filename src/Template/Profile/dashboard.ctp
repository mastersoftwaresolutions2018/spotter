<?php echo $this->element('fdashboardmenu'); ?>

<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="col-sm-9">
				<div class="thirdorder clearfix">
					<div class="single-owner-boat">
						<div class="col-sm-2 noppading">
							<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="owner-img img-responsive">
						</div>
						<div class="col-sm-7 noppading">
							<div class="content-owner-imd">
								<h3>Hello Silvia T.</h3>
								<p>22 years old<br>  Member since November 2016</p>
								<p class="icon-green pad-lrg"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
								<p class="seemap"><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i> Edit your profile</a></p>
							</div>
						</div>
						<div class="col-sm-3 noppading text-right">
							<div class="text-red"><a href="profile.html">View your public profile <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3 noppading">
				<div class="right-bar-side">
					<div class="sisthorder clearfix">
						<div class="saftey-travler">
							<p>Member Verification</p>
							<ul class="viled-check">
									<li><i class="fa fa-check" aria-hidden="true"></i> Phone verified</li>
									<li><i class="fa fa-check" aria-hidden="true"></i> Email Address verified</li>
									<li class="text-dif"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Verify your ID </li>
								</ul>
								<button class="contact-boat">Contact the Boat owner</button>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<div class="notifi-messag">
					<div class="head-con-notifi">
						<h3 class="head-not">Notifications (6)</h3>
						<div class="text-red"><a href="profile.html">View your public profile <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
					</div>
					<hr>
					<div class="add-notifi clearfix">
						<ul>
							<li class="clearfix"> 
								<div class="col-sm-7 noppading"><h5>Add your Boat</h5> <p>Co-travellers like to know what boat they'll be travelling in.</p></div>
								<div class="col-sm-5 noppading">
									<div class="button-add-cross">
										<button class="addyourboat">Add your boat</button>
										<button class="cross-cen"><i class="fa fa-times" aria-hidden="true"></i></button>
									</div>
								</div>
							</li>
							<li class="clearfix">
								<div class="col-sm-7 noppading"><h5>Add your Boat</h5> <p>Co-travellers like to know what boat they'll be travelling in.</p></div>
								<div class="col-sm-5 noppading">
									<div class="button-add-cross hide">
										<button class="addyourboat">Add your boat</button>
										<button class="cross-cen"><i class="fa fa-times" aria-hidden="true"></i></button>
									</div>
								</div>
							</li>
							<li class="clearfix">
								<div class="col-sm-7 noppading"><h5>Add your Boat</h5> <p>Co-travellers like to know what boat they'll be travelling in.</p></div>
								<div class="col-sm-5 noppading">
									<div class="button-add-cross hide">
										<button class="addyourboat">Add your boat</button>
										<button class="cross-cen"><i class="fa fa-times" aria-hidden="true"></i></button>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-6 noppading">
								<div class="notifi-messag">
					<div class="head-con-notifi">
						<h3 class="head-not">Notifications (6)</h3>
						<div class="text-red"><a href="profile.html">View your public profile <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></div>
					</div>
					<hr>
					<div class="add-notifi clearfix rigt">
						<ul>
							<li class="clearfix"> 
								<div class="col-sm-1 noppading round-img-mess">
									<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="img-roung">
								</div>
								<div class="col-sm-11 noppading"><h5>marco</h5> <p>Hello Silvia, i am intersted to trip.</p></div>
							</li>
							<li class="clearfix">
								<div class="col-sm-1 noppading round-img-mess">
									<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="img-roung">
								</div>
								<div class="col-sm-11 noppading"><h5>Gieussep</h5> <p>Wooo hooo...... greate Trip !</p></div>
							</li>
							<li class="clearfix">
								<div class="col-sm-1 noppading round-img-mess">
									<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="img-roung">
								</div>
								<div class="col-sm-11 noppading"><h5>John Steve</h5> <p>Co-travellers like to know what boat they'll be travelling in. </p></div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
