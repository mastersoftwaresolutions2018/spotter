				<?php echo $this->element('fdashboardmenu'); ?>
<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="profile-section">
				<div class="col-sm-12 noppading">
					<?php echo $this->element('profilesidebar', [
    "userdata" => $userinfo
]); ?>
<div class="col-sm-9">
						<div class="profile-right-content clearfix">
							<div class="heading-text-profile-right">
								<h3>Your profile photo</h3>
									<p>Add your photo now! Other members will be reassured to see who they'll be travelling with, and you'll find your boat share much more easily. Photos also help members to recognise each other</p>
									<div class="photo-profile clearfix">
										<div class="col-sm-5 redcheck-photo">
											<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="owner-img img-responsive"  >
											<i class="fa fa-check" aria-hidden="true"></i>
											<h3 class="text-green">Example of a good photo.</h3>
										</div>
										<div class="col-sm-7">
											<h4 class="right-photo-heading">How to choose the right photo</h4>
											<ul>
												<li><img src="<?php echo $this->request->webroot; ?>images/photo-profile1.png"> No sunglasses</li>
												<li><img src="<?php echo $this->request->webroot; ?>images/photo-profile2.png"> Facing the camera</li>
												<li><img src="<?php echo $this->request->webroot; ?>images/photo-profile3.png"> You alone </li>
												<li><img src="<?php echo $this->request->webroot; ?>images/photo-profile4.png"> Clear and bright </li>
											</ul>
										</div>
										<div class="col-sm-12 text-center">
											<div class="upload-icon-profile-photo">
												<form class=""   method="post" id="profileup" enctype=multipart/form-data >
												<img src="<?php echo $this->request->webroot; ?>images/profile-upload.png" class="img-responsive" id="uploaded_image" >
												<p>Upload your profile photo</p>
												<div class="fileUpload">
												<span>Choose a file</span>
												<input id ="imgInp" type="file" name="profilepic" class="upload savebutton">
												</div>
												<p class="ddee">PNG, JPG or GIF, max. 3MB</p>
												<input type="submit" value="Upload Picture" class="btn btn-default">

											</div>
											</form>
										</div>
									</div>
							</div>
							
						</div>
					</div>		 
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
