<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addBook']").validate({
    // Specify validation rules
    rules: {
       question: "required",
       answer: "required",

    },
    // Specify validation error messages
    messages: {
      question: "question is required.",
      answer: "Answer description is required.",

    },
    submitHandler: function(form) {
        form.submit();
        var question    = $("#question").val();
        var answer = $("#answer").val();
        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/books/getifnameExist1' ?>",
           data: { name: name },
           success: function(response)
            {
                if(response > 0){
                    if($("#name-error").length == 0){ 
                        $("#name").after('<div id="name-error" class="form-control-feedback" style="color:#f4516c">Book Name alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#name-error").html("Book Name alreay exists. Please choose another.");
                    }
                }else{
                    $.ajax({
                      type: "POST",
                      url: "<?php echo HTTP_ROOT.'admin/books/validateIsbn1' ?>",
                      data: { isbn_no: isbn_no },
                      success: function(response1)
                      {
                        if(response1 > 0){
                          if($("#isbn_no-error").length == 0){ 
                              $("#isbn_no").after('<div id="isbn_no-error" class="form-control-feedback" style="color:#f4516c">ISBN no. alreay exists. Please choose another.</div>');
                          }else{            
                              $("#isbn_no-error").html("ISBN no. alreay exists. Please choose another.");
                          }
                        }else{
                          form.submit();
                        }
                      }
                    });
                    
                }
            }
        });
    }
  });
}); 

</script>
 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Edit Faq') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <a href="<?php echo HTTP_ROOT.'admin/faqs'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Back
                    </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT ?>faqs/edit/<?php echo $faq['id'];?>" enctype="multipart/form-data" name="addBook" class="m-form m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Question</label> 
            <input type="text" name="question" class="form-control m-input m-input--square" id="question" value="<?php echo @$faq['question']; ?>">
          </div>   
        </div>
              
        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Answer</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="answer">
            <?php echo @$faq['answer']; ?></textarea>
          </div>        
        </div>
    
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
