<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gym[]|\Cake\Collection\CollectionInterface $gyms
 */
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.limit').on('change',function(){
            $('#limit').submit();
        });
    });
</script>
<div class="users index large-9 medium-8 columns content">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Manage Faqs
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <i class="fas fa-dumbbell" style="font-size: 24px">  </i> &nbsp;&nbsp; 
            <i style="font-size: 18px"><?= $this->Paginator->counter('{{count}}'); ?></i>  Faqs 
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">

                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('question', 'Questions'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
                <?php if($faqs->count() > 0):?>
                <?php $i=0; foreach ($faqs as $faq): ?>
                <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= $this->Number->format($faq->id) ?></span>
                    </td>
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= h($faq->question) ?></span>
                    </td>
                    <td class="m-datatable__cell"><span style="width: 110px;">
                        <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $faq->id), array('escape' => false)) ?>
                            <a href="<?php echo HTTP_ROOT.'admin/faqs/delete/'.$faq->id; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a>
                            </span>
                    </td>
                    </tr>
                <?php $i++; endforeach; ?> 
                <?php else:?>
                    <tr>
                    <td class="m-datatable__cell">
                    There are no record.
                        </td>
                    </tr>
                <?php endif;?>
                </tbody>
            </table>
        </div>
        <!--end: Datatable -->
    </div>
</div>
</div>











