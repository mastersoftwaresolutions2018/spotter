<?php echo $this->element('fdashboardmenu'); ?>

<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="profile-section">
				<div class="col-sm-12 noppading">
					<div class="col-sm-3">
						<div class="profile-left-side">
							<div class="profile-imgblock">
								<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="owner-img img-responsive">
								<h3>Silvia</h3>
								<p>St. Peter-Ording, Rendsburg, Germany</p>
							</div>
							<div class="list-profile-left">
								<ul>
									<p>Profile information</p>
									<li class="active"><a href="profile.html">Personal information</a></li>
									<li><a href="profile-photo.html">Photo</a></li>
									<li><a href="verification.html">Verification</a></li>
									<li><a href="boat.html">Boat</a></li>
								</ul>
								<ul>
									<p>Ratings</p>
									<li><a href="rating-received.html">Ratings received</a></li>
									<li><a href="rating-given.html">Ratings given</a></li>
								</ul>
								<ul>
									<p>Account</p>
									<li><a href="payment-status.html">Payment Status</a></li>
									<li><a href="notification.html">Notifications</a></li>
									<li><a href="password-setting.html">Password</a></li>
									<li><a href="close-account.html">Close my account</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="profile-right-content clearfix">
							<div class="heading-text-profile-right">
								<h3>Personal Information</h3>
								<form class="funkyradio">
									<div class="col-sm-12 noppading">
										<p>Gender</p>
										<div class="funkyradio-danger">
								            <input name="radio" id="radio1" checked="" type="radio">
								            <label for="radio1">Female</label>
								        </div>
								        <div class="funkyradio-danger">
								            <input name="radio" id="radio2" checked="" type="radio">
								            <label for="radio2">Male</label>
								        </div>
									</div>
									<div class="col-sm-6 noppading right-t">
										<p>First Name</p>
										<input type="text" placeholder="Silvia">
									</div>
									<div class="col-sm-6 noppading left-t">
										<p>Last Name</p>
										<input type="text" placeholder="Tegorr">
									</div>
									<div class="col-sm-6 noppading right-t">
										<p>Email Address</p>
										<input type="text" placeholder="silvia123@kiboat.com">
									</div>
									<div class="col-sm-6 noppading left-t mm-t">
										<p>Phone Number</p>
										<input type="text" placeholder="+01 123-4567-890">
										<div class="checkkk">
											<div class="funkyradio-danger">
								            <input name="checkbox" id="checkbox99" checked="" type="checkbox">
								            <label for="checkbox99">Never show my phone number publicly </label>
								        </div>
										</div>
									</div>
									<div class="col-sm-6 noppading">
										<p>Birth Year</p>
										<select class="">
										    <option>1994</option>
										    <option>1995</option>
										    <option>3</option>
										    <option>4</option>
										  </select>
									</div>
									<div class="col-sm-12 noppading biodescrip">
										<h4 class="text-red">Bio</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum massa eu accumsan malesuada. Cras non aliquet ligula. Vivamus urna eros, porta nec ornare eget, euismod eget felis. Vivamus commodo placerat justo eu efficitur. Proin lobortis aliquam orci, et imperdiet urna pulvinar eu. Sed ut ipsum erat. Praesent orci arcu, sagittis consectetur mi in, commodo varius orci. Proin lacinia turpis magna, at semper nisl finibus quis. Vivamus quis facilisis nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed bibendum massa eu accumsan malesuada. Cras non aliquet ligula. tur mi in, commodo varius orci. nibh. </p>
										<hr>
										<div class="col-cen-d">
											<p class="text-red">Please do not incluide</p>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon1.png"> Phone number</p>
												</div>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon2.png"> Facebook account details</p>
												</div>
												<div class="col-sm-4 noppading">
													<p><img src="<?php echo $this->request->webroot; ?>images/profile-imh-icon3.png"> Details about specific rides</p>
												</div>
										</div>
									</div>
								</form>
							</div>
							
						</div>
						<div class="col-sm-12 noppading clearfix text-right">
							<button class="savebutton">save</button>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
