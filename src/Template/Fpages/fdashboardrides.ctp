<?php echo $this->element('fdashboardmenu'); ?>

<section class="serach-result-main dashboard-main rides-offer">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="booking-tab">
				  <ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#current-booking">Upcoming rides</a></li>
				    <li><a data-toggle="tab" href="#booking-history"> Past rides</a></li>
				  </ul>

				  <div class="tab-content">
				    <div id="current-booking" class="tab-pane fade in active">
				     	<p class="top-tab-head">This page lists your upcoming ride offers</p>
				     	<div class="current-book">
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="pull-right approvl-seats">
										<p><span class="large-text">3</span>Seats left</p>
										<p><span class="large-text">20 €</span> per co-traveller</p>
										<p><span class="automatic text-red">Automatic</span>Approval</p>
									</div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p class="text-blue"><i class="fa fa-exchange" aria-hidden="true"></i> Offer ride on return trip </p>
											 <p class="text-blue"><i class="fa fa-clone" aria-hidden="true"></i> Duplicate</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png"> See your co-travellers</button>
													<a href="#" class="text-green"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
													<a href="#" class="text-red"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

								
				     	</div>
				    </div>
				    <div id="booking-history" class="tab-pane fade">
				      <h3>Menu 1</h3>
				      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				    </div>
				  </div>
			</div>
		</div>
	</div>
</section>
