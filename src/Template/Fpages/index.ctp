<!DOCTYPE html>
<html lang="en">
<head>
  <title>Kiboat</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">  
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">   
  <link href="css/font-awesome.min" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <!--link rel="stylesheet" href="css/slick.css"-->
  <!--link rel="stylesheet" href="css/slick-theme.css"-->
  <!--link rel="stylesheet" href="css/animate.css"-->
  <link rel="stylesheet" href="css/bootstrap-responsive-tabs.css">

</head>
<body>
<section class="navigation">
	<div class="container-fluid">
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>                        
		  </button>
		  <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="img-responsive"></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		  <!--ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Projects</a></li>
			<li><a href="#">Contact</a></li>
		  </ul-->
		   <!--button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button-->

		  <ul class="nav navbar-nav navbar-right">
			<li class="sign"><a href="signup.html">sign up</a></li>
			<li class="sign"><a href="#" data-toggle="modal" data-target="#login">Log in</a></li>
			<li><a href="boat.html"><button class="find-boat">Find A Boat</button></a></li>
			<li><a href="boat-offer.html"><button class="offer-boat">offer A Boat</button></a></li>
			<li class="dropdown sig"><button class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="images/flag1.png"><span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="#">Engligh</a></li>
					<li><a href="#">Hindi</a></li>
					<li><a href="#">Bengali</a></li>
					<li><a href="#">Gujarati</a></li>
					<li><a href="#">Kannada</a></li>
					<li><a href="#">Punjabi</a></li>
					<li><a href="#">Sanskrit</a></li>
					<li><a href="#">Telugu</a></li>
					<li><a href="#">Kashmiri</a></li>
				</ul>
			</li>
		  </ul>
		</div>
	  </div>
	</nav>
	</div>
</section>

<section class="banner-home">
<div class="baner-overlay"></div>
	<div class="container">
		<div class="shared-trip">
		<h1 class="text-center">A shared trip is a better trip</h1>
			<form>
				<ul>
					<li><input type="text" placeholder="St. Peter-Ording, Germany" class="add1 text-colr"></li>
					<li><input type="text" placeholder="Ending point" class="add2 text-colr"></li>
					<li><select class="add3 text-colr">
							<option>Type of Boat</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><select class="add4 text-colr">
							<option>Type of Trips</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><input type="text" placeholder="09/21/2017" class="add5 text-colr"></li>
					<li class="searchbtn"><a href="search-result.html"><button class="search"><i class="fa fa-search" aria-hidden="true"></i></button></a></li>
				</ul>
			</form>
		</div>
	</div>
</section>

<section class="many-boast-section">
	<div class="container">
	<div class="row">
		<div class="col-sm-12 noppading">
			<div class="col-sm-4">
				<p class="strong-title">Too many Boats. <br>So many empty seats.</p>
				<p class="strong-title text-green">Let's fix that.</p>
				<a href="boat-offer.html"><button class="offer-icons"><img src="images/add-icon21.png" class="blck1"><img src="images/add-icon22.png" class="blck2">offer a boat</button></a>
			</div>
			<div class="col-sm-8">
				<div class="five-boat-box">
					<ul>
						<li><a href="#"><div class="img-heig"><img src="images/boast1.png" class="sh"><img src="images/boast11.png" class="hi"></div> Fishing</a></li>
						<li><a href="#"><div class="img-heig"><img src="images/boast2.png" class="sh"><img src="images/boast21.png" class="hi"></div> Celebrating</a></li>
						<li><a href="#"><div class="img-heig"><img src="images/boast3.png" class="sh"><img src="images/boast31.png" class="hi"></div> Sailing </a></li>
						<li><a href="#"><div class="img-heig"><img src="images/boast4.png" class="sh"><img src="images/boast41.png" class="hi"></div> Watersports </a></li>
						<li class="smlimg"><a href="#"><div class="img-heig"><img src="images/boast5.png" class="sh"><img src="images/boast51.png" class="hi"></div> Cruising</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="boatimag"><img src="images/bg-boat.png"></div>
		</div>
	</div>
</section>

<section class="thousand-boats">
	<div class="container">
		<h1>Thousands of boats available every day.<br> Where do you want to go?</h1>
		<h4>See our most popular rides</h4>
		<div class="box-thousand">
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="three-things">
	<div class="container">
		<div class="tabs-respon">
			      <ul class="nav nav-tabs responsive-tabs clearfix">
					<li class="active"><a href="#home1">
						<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img1.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div></a>
					</li>
					<li><a href="#profile1">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img2.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a></li>
					<li><a href="#messages1">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img3.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a></li>
				  </ul>

			  <div class="tab-content">
				<div class="tab-pane active" id="home1">
				  <h1>3 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

				<div class="tab-pane" id="profile1">
				  <h1>2 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

				<div class="tab-pane" id="messages1">
				 <h1>1 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

			  </div>
		</div>
	</div>
</section>

<section class="thousand-boats howitwork">
	<div class="container">
		<h1>How it works</h1>
		<h4>Find out more</h4>
		<div class="box-thousand">
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="images/how-work1.png">
						<h3 class="text-green">Going somewhere?</h3>
						<p>Find people who travel on the same route.</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="images/how-work2.png">
						<h3 class="text-green">Get in touch</h3>
						<p>Select with whom you want to travel</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="images/how-work3.png">
						<h3 class="text-green">Off you go!</h3>
						<p>Travel together and give a rating after the ride.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="vedio-section">
	<div class="container">
		<div class="text-center text-vedio-content">
			<img src="images/vedio-icon.png" class="img-responsive">
			<h4>Round up your crew and</h4>
			<h1>Plan a better vacation</h1>
			<p class="text-red">Watch Video!</p>
		</div>
	</div>
</section>

<section class="thousand-boats howitwork">
	<div class="container-fluid">
		<h1>Boatpooling with Kiboat</h1>
		<h4>Visit Kiboat Life</h4>
		<div class="box-thousand">
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="images/boat-poll1.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="images/boat-poll2.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="images/boat-poll3.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="images/boat-poll4.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="footer-section">
	<div class="container">
		<div class="foter-cotent">
			<div class="col-sm-3">
				<div class="cont1">
					<img src="images/logo.png" class="img-responsive">
					<div class="social-cion">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						</ul>
						<p>Kiboat, 2017 ©</p>
					</div>
				</div>
			</div>
			<div class="col-sm-9 noppading">
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Using Kiboat</h4>
						<ul>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Trust & Safety</a></li>
							<li><a href="#">Experience levels</a></li>
							<li><a href="#">Ratings</a></li>
							<li><a href="#">Ladies only</a></li>
							<li><a href="#">Member's Agreement</a></li>
							<li><a href="#">Insurance</a></li>
							<li><a href="#">Frequently Asked Questions</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Our company</h4>
						<ul>
							<li><a href="#">About Us</a></li>
							<li><a href="#">KiboatLife</a></li>
							<li><a href="#">Press</a></li>
							<li><a href="#">Partners</a></li>
							<li><a href="#">We're Hiring!</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Destinations</h4>
						<ul>
							<li><a href="#">Sylt</a></li>
							<li><a href="#">Bay of Lübeck</a></li>
							<li><a href="#">Usedom</a></li>
							<li><a href="#">St. Peter-Ording</a></li>
							<li><a href="#">Amrum</a></li>
							<li><a href="#">Hiddensee</a></li>
							<li><a href="#">Juist</a></li>
							<li><a href="#">Rügen</a></li>
							<li><a href="#">Rostock</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Legal</h4>
						<ul>
							<li><a href="#">Terms & Conditions</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Cookies Policy</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="copuright">
					<p class="text-left">Language <span>English</span></p>
					<p class="text-right">Additional insurance does not replace driver's mandatory Boat insurance. Conditions apply.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!------model popup---------------->
 <div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content clearfix">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        	<div class="login-popup">
        		<div class="col-sm-5 noppading">
        			<div class="bg-img">
        				<div class="bg-contnt-m">
	        				<p>Not a member yet?</p>
	        				<button class="signbtn">Sign-up in 30 seconds</button>
        				</div>
        			</div>
        		</div>
        		<div class="col-sm-7 noppading">
        			<from>
	        			<div class="content-right">
	        				<h1>login</h1>
	        				<hr>
	        				<button class="signbtn-blue">Connect With Facebook</button>
	        			
	        					<div class="orimg"><img src="images/orimg.png" class="img-responsive"></div>

	        			    <div class="group">      
						      <input type="text" required>
						      <span class="highlight"></span>
						      <span class="bar"></span>
						      <label>Username</label>
						    </div>
						    <div class="group">      
						      <input type="text" required>
						      <span class="highlight"></span>
						      <span class="bar"></span>
						      <label>Passwors</label>
						    </div>
						    <div class="col-sm-12 noppading">
						    	<p><input type="checkbox"> Remember me</p>
						    </div>
						    <button class="signbtn">secure Login</button>
						    <p class="frgt">I've forgotten my password</p>
						</div>
        			</from>
        		</div>
        	</div>
      </div>      
    </div>
  </div>
     <div class="modal fade" id="shareride" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content clearfix">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        	<div class="sharehide-popup">
        		<div class="links-head">
        			<h1>Sharing is caring</h1>
        		</div>
        		<div class="pad-lef">
        			<p>Copy and paste this  link into your website <a href="#">Learn More</a></p>
        			<div class="inputs-links">
        				<input type="text" placeholder="https://www.kiboat.com/&!%Profile&%Silvia-d1678h6" class="ab1">
        				<button class="copylink">Copy Link</button>
        			</div>
        		</div>
        		<hr>
        		<div class="pad-lef">
        			<p>Share with friends Via Email</p>
        			<div class="inputs-links">
        				<input type="text" placeholder="Enter email address" class="ab2">
        				<button class="copylink">Send</button>
        			</div>
        		</div>
        		<hr>
        		<div class="pad-lef">
        			<p>Share this on your favorite social networks</p>
        			<div class="social-icos clearfix">
        				<ul>
        					<li class="text-drkblue"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-blue"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-drkblue"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-red"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-bluegr"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        				</ul>
        			</div>
        		</div>
        		<div class="pad-lef bottmbtnss">
        			<button class="sharenowt">Share Now</button>
        			<button class="cancle">cancel</button>
        		</div>
        	</div>       	
      </div>      
    </div>
  </div>
<!-----end model popup----------->





 <script src="js/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
 <!--script src="js/wow.min.js"></script-->
 <script src="js/custom.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
 <!--script src="js/modernizr.js"></script-->
 <script src="js/jquery.bootstrap-responsive-tabs.min.js"></script>
 <!--script type="text/javascript" src="js/slick.min.js"></script-->
<script>
$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $(".header").addClass("active");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".header").removeClass("active");
    }
});
</script>
<script>
$('.eco-mindness ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
<script>
$('.tab-navigation ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
 <script>
var slider = document.getElementById("myRange");
var output = document.getElementById("bars1");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>
 <script>
var slider = document.getElementById("myRange1");
var output = document.getElementById("bars2");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>
<script>
$('.list-profile-left ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
<!--script type="text/javascript"> 
$('.autoslick').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script-->
<!--script type="text/javascript"> 
$('.autoslick1').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script-->
<!--script>
 $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 2,
  slidesToScroll: 1,
  vertical: true,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});
</script-->
<script>
$('.responsive-tabs').responsiveTabs({
  accordionOn: ['xs', 'sm']
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>