<section class="banner-home">
<div class="baner-overlay"></div>
	<div class="container">
		<div class="shared-trip">
		<h1 class="text-center">
		A shared trip is a better trip</h1>
			<form>
				<ul>
					<li><input type="text" placeholder="St. Peter-Ording, Germany" class="add1 text-colr"></li>
					<li><input type="text" placeholder="Ending point" class="add2 text-colr"></li>
					<li><select class="add3 text-colr">
							<option>Type of Boat</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><select class="add4 text-colr">
							<option>Type of Trips</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><input type="text" placeholder="09/21/2017" class="add5 text-colr"></li>
					<li class="searchbtn"><a href="search-result.html"><button class="search"><i class="fa fa-search" aria-hidden="true"></i></button></a></li>
				</ul>
			</form>
		</div>
	</div>
</section>

<section class="many-boast-section">
	<div class="container">
	<div class="row">
		<div class="col-sm-12 noppading">
			<div class="col-sm-4">
				<p class="strong-title">Too many Boats. <br>So many empty seats.</p>
				<p class="strong-title text-green">Let's fix that.</p>
				<a href="boat-offer.html"><button class="offer-icons"><img src="<?php echo $this->request->webroot; ?>images/add-icon21.png" class="blck1"><img src="<?php echo $this->request->webroot; ?>images/add-icon22.png" class="blck2">offer a boat</button></a>
			</div>
			<div class="col-sm-8">
				<div class="five-boat-box">
					<ul>
						<li><a href="#"><div class="<?php echo $this->request->webroot; ?>-heig"><img src="<?php echo $this->request->webroot; ?>images/boast1.png" class="sh"><img src="images/boast11.png" class="hi"></div> Fishing</a></li>
						<li><a href="#"><div class="img-heig"><img src="<?php echo $this->request->webroot; ?>images/boast2.png" class="sh"><img src="images/boast21.png" class="hi"></div> Celebrating</a></li>
						<li><a href="#"><div class="img-heig"><img src="<?php echo $this->request->webroot; ?>images/boast3.png" class="sh"><img src="images/boast31.png" class="hi"></div> Sailing </a></li>
						<li><a href="#"><div class="img-heig"><img src="<?php echo $this->request->webroot; ?>images/boast4.png" class="sh"><img src="images/boast41.png" class="hi"></div> Watersports </a></li>
						<li class="smlimg"><a href="#"><div class="img-heig"><img src="<?php echo $this->request->webroot; ?>images/boast5.png" class="sh"><img src="images/boast51.png" class="hi"></div> Cruising</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="boatimag"><img src="<?php echo $this->request->webroot; ?>images/bg-boat.png"></div>
		</div>
	</div>
</section>

<section class="thousand-boats">
	<div class="container">
		<h1>Thousands of boats available every day.<br> Where do you want to go?</h1>
		<h4>See our most popular rides</h4>
		<div class="box-thousand">
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="content-in clearfix">
					<div class="left-con">
						<p>Lumaha'i Beach</p>
						<p>Kailua, Oahu</p>
					</div>
					<div class="right-con text-green">
						<p>Price From</p>
						<h2>$500</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="three-things">
	<div class="container">
		<div class="tabs-respon">
			      <div class="responsive-tabs-container accordion-xs accordion-sm"><ul class="nav nav-tabs responsive-tabs clearfix">
					<li class="active"><a href="#home1">
						<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img1.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div></a>
					</li>
					<li class=""><a href="#profile1">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img2.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a></li>
					<li class=""><a href="#messages1">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img3.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a></li>
				  </ul><div class="tab-content">
				<a href="#home1" class="accordion-link first active">
						<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img1.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div></a><div class="tab-pane active" id="home1">
				  <h1>3 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

				<a href="#profile1" class="accordion-link second">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img2.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a><div class="tab-pane" id="profile1">
				  <h1>2 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

				<a href="#messages1" class="accordion-link last">
					<div class="inutstab clearfix">
							<div class="img-tab">
								<img src="images/tab-img3.png">
							</div>
							<div class="text-head-tab">
								<h4>You're in control</h4>
								<p>Verified member profiles and ratings mean you know exactly who you're travelling with. </p>
							</div>							
						</div>
						</a><div class="tab-pane" id="messages1">
				 <h1>1 things</h1>
				  <h3>you'll love about <span class="text-red">KiBoat</span></h3>
				</div>

			  </div></div>

			  
		</div>
	</div>
</section>

<section class="thousand-boats howitwork">
	<div class="container">
		<h1>How it works</h1>
		<h4>Find out more</h4>
		<div class="box-thousand">
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="<?php echo $this->request->webroot; ?>images/how-work1.png">
						<h3 class="text-green">Going somewhere?</h3>
						<p>Find people who travel on the same route.</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="<?php echo $this->request->webroot; ?>images/how-work2.png">
						<h3 class="text-green">Get in touch</h3>
						<p>Select with whom you want to travel</p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="content-in clearfix">
					<div class="work-how">
						<img src="<?php echo $this->request->webroot; ?>images/how-work3.png">
						<h3 class="text-green">Off you go!</h3>
						<p>Travel together and give a rating after the ride.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="vedio-section">
	<div class="container">
		<div class="text-center text-vedio-content">
			<img src="<?php echo $this->request->webroot; ?>images/vedio-icon.png" class="img-responsive">
			<h4>Round up your crew and</h4>
			<h1>Plan a better vacation</h1>
			<p class="text-red">Watch Video!</p>
		</div>
	</div>
</section>

<section class="thousand-boats howitwork">
	<div class="container-fluid">
		<h1>Boatpooling with Kiboat</h1>
		<h4>Visit Kiboat Life</h4>
		<div class="box-thousand">
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="<?php echo $this->request->webroot; ?>images/boat-poll1.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="<?php echo $this->request->webroot; ?>images/boat-poll2.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="<?php echo $this->request->webroot; ?>images/boat-poll3.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="boatpolling clearfix">
					<img src="<?php echo $this->request->webroot; ?>images/boat-poll4.png" class="img-responsive">
					<div class="boatpollingdes">
						<h4>Boating Tips: How to Use a tiller Steer Outboard</h4>
						<p>Lenny Rudow     <span class="text-red">Sep 21, 2017</span></p>
						<p class="des1">Many small boats have tiller steer outboards instead of a steering wheel—this short video shows us the basics of how to use them. </p>
						<p><a href="#" class="text-red">Read More</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


