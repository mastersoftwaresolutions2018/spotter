<?php echo $this->element('fdashboardmenu'); ?>

<section class="serach-result-main dashboard-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="booking-tab">
				  <ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#current-booking">Current bookings</a></li>
				    <li><a data-toggle="tab" href="#booking-history">Bookings history</a></li>
				  </ul>

				  <div class="tab-content">
				    <div id="current-booking" class="tab-pane fade in active">
				     	<p class="top-tab-head">Here you can find your current bookings. Past bookings can be found in your booking history.</p>
				     	<div class="current-book">
							<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="text-red"><a href="#" class="text-green"><i class="fa fa-check" aria-hidden="true"></i>Booked</a></div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix"> 
											<div class="col-sm-7 noppading"><p>Your booking request was approved by <strong>Marco</strong>. Your seat is booked.</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img1.png">Marco</button>
													<button class="addyourboat">+1 234 5678 901</button>
												</div>
											</div>
										</li>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p><span class="big-tect">1</span> Seats</p> <p><span class="big-tect"> 20€</span> Cash</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png">See the offer</button>
													<a href="#" class="text-green"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> View this booking</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

														<div class="notifi-messag">
								<div class="head-con-notifi clearfix">
									<div class="pull-left">
										<h3 class="head-not text-green">St. Peter-Ording<i class="fa fa-long-arrow-right" aria-hidden="true"></i>Ording Beach</h3>
										<p>Saturday,  30 Sep 2017, 12:45</p>
									</div>
									<div class="text-red"><a href="#" class="text-green"><i class="fa fa-check" aria-hidden="true"></i>Booked</a></div>
								</div>
								<hr>
								<div class="add-notifi clearfix">
									<ul>
										<li class="clearfix"> 
											<div class="col-sm-7 noppading"><p>Your booking request was approved by <strong>Marco</strong>. Your seat is booked.</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img1.png">Marco</button>
													<button class="addyourboat">+1 234 5678 901</button>
												</div>
											</div>
										</li>
										<li class="clearfix cashseat"> 
											<div class="col-sm-7 noppading"><p><span class="big-tect">1</span> Seats</p> <p><span class="big-tect"> 20€</span> Cash</p></div>
											<div class="col-sm-5 noppading">
												<div class="button-add-cross">
													<button class="man-auth"><img src="<?php echo $this->request->webroot; ?>images/current-img2.png">See the offer</button>
													<a href="#" class="text-green"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> View this booking</a>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
				     	</div>
				    </div>
				    <div id="booking-history" class="tab-pane fade">
				      <h3>Menu 1</h3>
				      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				    </div>
				  </div>
			</div>
		</div>
	</div>
</section>
