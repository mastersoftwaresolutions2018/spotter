 <?php echo $this->element('fdashboardmenu'); ?>

<section class="serach-result-main dashboard-main">
<div class="container noppading">
<div class="main-content-result col-sm-12 noppading">
<div class="profile-section boat">
<div class="col-sm-12 noppading">
<div class="col-sm-3">
	<div class="profile-left-side">
		<div class="profile-imgblock">
			<img src="<?php echo $this->request->webroot; ?>images/owner-img.png" class="owner-img img-responsive">
			<h3>Silvia</h3>
			<p>St. Peter-Ording, Rendsburg, Germany</p>
		</div>
		<div class="list-profile-left">
			<ul>
				<p>Profile information</p>
				<li><a href="profile.html">Personal information</a></li>
				<li><a href="profile-photo.html">Photo</a></li>
				<li><a href="verification.html">Verification</a></li>
				<li class="active"><a href="boat.html">Boat</a></li>
			</ul>
			<ul>
				<p>Ratings</p>
				<li><a href="rating-received.html">Ratings received</a></li>
				<li><a href="rating-given.html">Ratings given</a></li>
			</ul>
			<ul>
				<p>Account</p>
				<li><a href="payment-status.html">Payment Status</a></li>
				<li><a href="notification.html">Notifications</a></li>
				<li><a href="password-setting.html">Password</a></li>
				<li><a href="close-account.html">Close my account</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="col-sm-9">
<div class="booking-tab">
<ul class="nav nav-tabs">
<li><a data-toggle="tab" href="#inbox-tab" aria-expanded="true">My Boats</a></li>
<li class="active"><a data-toggle="tab" href="#archive-tab" aria-expanded="false">Add A Boat</a></li>
</ul>

<div class="tab-content">
<div id="archive-tab" class="tab-pane fade active in">
	<div class="profile-right-content clearfix">
		<div class="heading-text-profile-right">
			<h3>Add Tour Boat Details</h3>

			<form class="funkyradio" action="<?php echo BASE_URL; ?>/boats/saveData" method="post" enctype=multipart/form-data >
				<div class="col-sm-6 noppading right-t">
					<p>Boat name</p>
					<input placeholder="" type="text" name="name">
				</div>
				<div class="col-sm-6 noppading left-t">
					<p>Boat Modal</p>
					<input placeholder="" type="text" name="modal">
				</div>
				<div class="col-sm-6 noppading right-t">
					<p>What year was it registered?</p>
					<input placeholder="Enter Year" type="text" name="year">
				</div>
				<div class="col-sm-6 noppading left-t">
					<p>What type of Boat is it?</p>
					<input placeholder="" type="text" name="type">
				</div>
				<div class="col-sm-6 noppading">
					<p>Number of Seats max.</p>
					<select class="" name="seats_max">
					    <option value="100">100</option>
					    <option value="200">200</option>
					    <option value="3">3</option>
					    <option value="4">4</option>
					  </select>
				</div>

				<div class="col-sm-12 noppading biodescrip septr">
				<div class="col-sm-3">
				<p>Upload your profile photo</p>
					<div class="upload-icon-profile-photo">
							<img src="<?php echo $this->request->webroot; ?>images/profile-upload.png" class="img-responsive" id="uploaded_image">
							
						</div>
						</div>
						<div class="col-sm-9">
							<div class="fileUpload">
								<span>Choose a file</span>
								<input id ="imgInp" class="upload savebutton" type="file" name="pic">
								</div>
								<p class="ddee">PNG, JPG or GIF, max. 3MB</p>
							</div>
				</div>

				<div class="col-sm-12 noppading biodescrip septr">
				<div class="col-sm-3">
				<input type="submit" value="Add Boat" class="btn btn-default">
				</div>
				</div>
			</form>
		</div>
		
	</div></div>
<div id="inbox-tab" class="tab-pane fade">
  
   <h3>Organiser: <?php echo $loginname;?></h3>
  
<div class="m-portlet">

<div class="m-portlet__body">
<!--begin::Section-->
<div class="m-section">
	<div class="m-section__content">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					
					<th>
						Name
					</th>
					<th>
						Modal
					</th>
					<th>
						Year
					</th>
					<th>
						Type
					</th>
					<th>
						Seats
					</th>

					<th>
						Pic
					</th>
				</tr>
			</thead>
			
			<?php

			//echo  "<pre>";print_r($allboatbaseduponid);

		if (empty($allboatbaseduponid)) { ?>
<td>
					
				 
						<?php echo "No Record Found";?>
					 

			</td>
		 <tbody>
		<?php }else{


			
		


  foreach ($allboatbaseduponid as $key => $value) {

  	$name = $value->name;
  	$modal = $value->modal;
  	$year = $value->year;
  	$type = $value->type;
  	$seats_max = $value->seats_max;
  	$boat_pic = $value->boat_pic;
  	//echo "<pre>";print_r($value);
  	# code...
  
  ?>
				<tr>
					
					<td>
						<?php echo $name;?>
					</td>
					<td>
						<?php echo $modal;?>
					</td>
					<td>
						<?php echo $year;?>
					</td>
					<td>
						<?php echo $type;?>
					</td>
					<td>
						<?php echo $seats_max;?>
					</td>
					
					<td>
						<img src="<?php echo $this->request->webroot; ?>boat/<?php echo $boat_pic;?>" class="img-responsive">
					</td>
					
				</tr>
				
<?php }}
?>		
			</tbody>
		</table>
	</div>
</div>
<!--end::Section-->
</div>
<!--end::Form-->
</div>


	</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
</section>

<script type="text/javascript">
	function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#uploaded_image').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>