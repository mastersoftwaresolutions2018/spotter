 
<section class="serach-page">
	<div class="container">
		<div class="shared-trip">
			<form>
				<ul>
					<li><input type="text" placeholder="St. Peter-Ording, Germany" class="add1 text-colr"></li>
					<li><input type="text" placeholder="Ending point" class="add2 text-colr"></li>
					<li><select class="add3 text-colr">
							<option>Type of Boat</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><select class="add4 text-colr">
							<option>Type of Trips</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</li>
					<li><input type="text" placeholder="09/21/2017" class="add5 text-colr"></li>
					<li class="searchbtn"><button class="search"><i class="fa fa-search" aria-hidden="true"></i></button></li>
				</ul>
			</form>
		</div>
	</div>
</section>

<section class="serach-result-main">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="col-sm-3 noppading">
				<div class="left-sidebar">
					<div class="sidediv1">
						<div class="span12">
				    		<table class="table-condensed table-bordered">
				                <thead>
				                    <tr>
				                      <th colspan="7">
				                        <span class="btn-group">
				                            <a class="btn"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
				                        	<a class="btn active">September 2017</a>
				                        	<a class="btn"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
				                        </span>
				                      </th>
				                    </tr>
				                    <tr>
				                        <th>Sun</th>
				                        <th>Mon</th>
				                        <th>Tue</th>
				                        <th>Wed</th>
				                        <th>Thr</th>
				                        <th>Fri</th>
				                        <th>Sat</th>
				                    </tr>
				                </thead>
				                <tbody>
				                    <tr>
				                        <td class="muted">29</td>
				                        <td class="muted">30</td>
				                        <td class="muted">31</td>
				                        <td>1</td>
				                        <td>2</td>
				                        <td>3</td>
				                        <td>4</td>
				                    </tr>
				                    <tr>
				                        <td>5</td>
				                        <td>6</td>
				                        <td>7</td>
				                        <td>8</td>
				                        <td>9</td>
				                        <td>10</td>
				                        <td>11</td>
				                    </tr>
				                    <tr>
				                        <td>12</td>
				                        <td>13</td>
				                        <td>14</td>
				                        <td>15</td>
				                        <td>16</td>
				                        <td>17</td>
				                        <td>18</td>
				                    </tr>
				                    <tr>
				                        <td>19</td>
				                        <td class="text-red"><strong>20</strong></td>
				                        <td>21</td>
				                        <td>22</td>
				                        <td>23</td>
				                        <td>24</td>
				                        <td>25</td>
				                    </tr>
				                    <tr>
				                        <td>26</td>
				                        <td>27</td>
				                        <td>28</td>
				                        <td>29</td>
				                        <td class="muted">1</td>
				                        <td class="muted">2</td>
				                        <td class="muted">3</td>
				                    </tr>
				                </tbody>
				            </table>
				        </div>
					</div>
					<hr>
					<div class="sidediv2 funkyradio">
						<div class="funkyradio-danger">
				            <input type="checkbox" name="checkbox" id="checkbox4" checked/>
				            <label for="checkbox4">Free Trips</label>
				        </div>
					</div>
					<hr>
					<div class="sidediv3 funkyradio">					
					<h4>Type of Boat</h4>
						<div class="height-men">
							<div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox1" checked/>
					            <label for="checkbox1">All</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox2" checked/>
					            <label for="checkbox2">Boat</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox3" checked/>
					            <label for="checkbox3">Yacht</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox4" checked/>
					            <label for="checkbox4">Carry</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox5" checked/>
					            <label for="checkbox5">Sail</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox11" checked/>
					            <label for="checkbox11">All</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox21" checked/>
					            <label for="checkbox21">Boat</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox31" checked/>
					            <label for="checkbox31">Yacht</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox41" checked/>
					            <label for="checkbox41">Carry</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox51" checked/>
					            <label for="checkbox51">Sail</label>
					        </div>
				        </div>
					</div>
					<hr>
					<div class="sidediv4 funkyradio">					
					<h4>Type of Trips</h4>
						<div class="height-men">
							<div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox6" checked/>
					            <label for="checkbox6">All</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox7" checked/>
					            <label for="checkbox7">Fishing</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox8" checked/>
					            <label for="checkbox8">Exclusive</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox9" checked/>
					            <label for="checkbox9">Sailing Charter</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox10" checked/>
					            <label for="checkbox10">Celebrating</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox61" checked/>
					            <label for="checkbox61">All</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox71" checked/>
					            <label for="checkbox71">Fishing</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox81" checked/>
					            <label for="checkbox81">Exclusive</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox91" checked/>
					            <label for="checkbox91">Sailing Charter</label>
					        </div>
					        <div class="funkyradio-danger">
					            <input type="checkbox" name="checkbox" id="checkbox101" checked/>
					            <label for="checkbox101">Celebrating</label>
					        </div>
				        </div>
					</div>
					<hr>
					<div class="sidediv5 funkyradio">
					<h4>Duration Range</h4>
						<div class="range-bar-cus">
							<div id="slidecontainer">
							<p>2<span id="bars1"></span>hrs</p>
							  <input type="range" min="1" max="100" value="50" class="slider" id="myRange">							  
							</div>
						</div>
					</div>
					<hr>
					<div class="sidediv6 funkyradio">
					<h4>Price</h4>
						<div class="range-bar-cus">
							<div id="slidecontainer1">
							<p>$<span id="bars2"></span></p>
							  <input type="range" min="1" max="100" value="50" class="slider" id="myRange1">							  
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="right-cntent-result">
	              <p class="heading-result">St. Peter-Ording <sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn> Ording Beach</p>
				</div>
				<p class="duration-text">Duration: <span class="duration-tim">1 h 56 min</span></p>
				<div class="box-today-big col-sm-12 noppading">
					<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

					<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

										<div class="box-today clearfix">
						<div class="col-sm-3">
							<div class="text-content-b">
								<img src="<?php echo $this->request->webroot; ?>images/round-img.png">
								<div class="right-dash-content">
									<h4>Silvia</h4>
									<p>30 years old</p>
								</div>
								<div class="clearfix"></div>
								<div class="descri-star-face">
									<p><i class="fa fa-star" aria-hidden="true"></i><strong>4.9/5</strong>- 8 rating</p>
									<p><i class="fa fa-facebook" aria-hidden="true"></i>431 Friends</p>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="text-content-b-midle clearfix">
								<div class="col-sm-12 noppading box-upper">
									<div class="col-sm-8 noppading">
										<h2>Today -02.00</h2>
										<p>St. Peter-Ording<sapn class="next-descri"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></sapn>Ording Beach</p>
										<P class="text-smal">Type of Boat <span class="text-red">Boat</span></P>
									</div>
									<div class="col-sm-4 noppading">
										<div class="text-right-co">
											<p><i class="fa fa-user-o" aria-hidden="true"></i><i class="fa fa-user-o" aria-hidden="true"></i>
											<i class="fa fa-bolt" aria-hidden="true"></i></p>
											<p class="text-smal">Duration Range <span>5hrs</span></p>
											<p class="text-smal">Type of Trip <span class="text-green">Excursion</span></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr>
									<div class="add-des-bot">
										<p class="icon-red"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>St. Peter-Ording, Rendsburg, Germany</span></p>
										<p class="icon-green"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Ording Beach, Strandweg 18-20, 25826 St. Peter-Ording, Germany</span></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="text-content-bright">
								<h2>20 €</h2>
								<p>per co-traveller</p>
								<hr>
								<p class="left-who"><span class="text-green">3</span> Seats left</p>
								<button class="moredetail">More Details</button>
							</div>
						</div>
					</div>

					<div class="pagination-bottm">
						<p class="pull-left">1 to 10 of 23 results</p>
						<p class="pull-right"><sapn>1</sapn> <sapn>2</sapn> <sapn>3</sapn> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></p>
					</div>
					<div class="dont-mis text-center clearfix">
						<div class="ocntent-r">
							<h1>Don't miss out on new rides!</h1>
							<p>Get a notification via email when a new ride matches your search.</p>
							<button class="create-ride">Create a ride alert!</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
