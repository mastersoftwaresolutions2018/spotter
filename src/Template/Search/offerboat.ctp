<section class="serach-result-main boat-offer">
	<div class="container noppading">
		<div class="main-content-result col-sm-12 noppading">
			<div class="col-sm-12">
			<h1 class="page-headd">Offer a ride on your next long journey</h1>
			<form>
				<div class="firstorder clearfix">
					<h3 class="offer-boat-heading">Pick-up and drop-off points</h3>
					<hr>
					<div class="col-sm-8 noppading pad-15">
						<ul class="funkyradio">
							<div class="funkyradio-danger">
									<input name="checkbox" id="checkbox98" checked="" type="checkbox">
									 <label for="checkbox98">Same address pick-up location and drop-off location</label>
							</div>
							<li class="icon-red clearfix">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
								<span>
									<h5>Pick-up location</h5>
										<input placeholder="Enter Location" type="text">
								</span>
							</li>
							<hr>
							<li class="icon-green clearfix">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
								<span>
									<h5>Drop-off location</h5>
										<input placeholder="Enter Location" type="text">
								</span>
							</li>
							<li class="icon-green clearfix">
								<i class="fa fa-circle" aria-hidden="true"></i>
								<span>
									<h5>Starting Place Details</h5>
										<input placeholder="Enter Details" type="text">
								</span>
							</li>
						</ul>
							<div class="clearfix"></div>
								<div class="details-block">
									<div class="shedule-time pad-15 clearfix">
									<h4 class="inner-boat-heading">Date and time</h4>
									<div class="col-sm-4 noppading right-ttxt-inputs">
										<div class="date1">
											<div class="img-wid">
												<img src="images/add-icon4.png">
											</div>
											<p>Departure Date:</p>
										</div>
									</div>
									<div class="right-ttxt-inputs">
										<div class="col-sm-6 noppading">
											<div class="date2">
												<input placeholder="Enter Departure Date" type="text">
											</div>
										</div>
										<div class="col-sm-2 noppading">
											<div class="at-bgg">At</div> 
										</div>
										<div class="col-sm-4 noppading">
											<div class="date3">
												<input placeholder="Time" type="text">
											</div>
										</div>
									</div>
									</div>
								</div>

					</div>
					<div class="col-sm-4 noppading text-center">
						<div class="map-offer-bato">
							<img src="images/map-boat-offer.png" class="img-responsive">
							<p class="text-red text-small">You can move marker on map for more accuracy</p>
						</div>
					</div>

				</div>
				<div class="thirdorder clearfix">
					<div class="boatowner">
						<h3 class="offer-boat-heading">Duration of trip</h3>
					</div>
					<hr>
					<div class="shedule-time pad-15 clearfix">
									<div class="col-sm-4 noppading right-ttxt-inputs">
										<div class="date1">
											<div class="img-wid">
												<img src="images/clock-img.png">
											</div>
											<p>Duration Time</p>
										</div>
									</div>
									<div class="right-ttxt-inputs">
										<div class="col-sm-12 noppading">
											<div class="date2">
												<input placeholder="Time" type="text">
												<div class="toll-tip">
													<div class="tooltip"><a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i></a>
													    <span class="tooltiptext tooltip-right">Enter here an approximate estimate of duration trip</span>
													 </div>
												</div>
											</div>
										</div>
									</div>
					</div>
				</div>
				<div class="thirdorder clearfix">
					<div class="boatowner">
						<h3 class="offer-boat-heading">Type of Trip</h3>
					</div>
					<hr>
					<div class="shedule-time pad-15 clearfix">
									<div class="fott-inputs-round">
										<ul class="funkyradio">
											<li>
												<div class="funkyradio-danger">
													<input name="checkbox" id="checkbox95" checked="" type="checkbox">
													 <label for="checkbox95">Fishing</label>
												</div>
											</li>
											<li>
												<div class="funkyradio-danger">
													<input name="checkbox" id="checkbox94" checked="" type="checkbox">
													 <label for="checkbox94">Celebrating</label>
												</div>
											</li>
											<li>
												<div class="funkyradio-danger">
													<input name="checkbox" id="checkbox93" checked="" type="checkbox">
													 <label for="checkbox93">Sailing</label>
												</div>
											</li>
											<li>
												<div class="funkyradio-danger">
													<input name="checkbox" id="checkbox92" checked="" type="checkbox">
													 <label for="checkbox92">Watersports</label>
												</div>
											</li>
											<li>
												<div class="funkyradio-danger">
													<input name="checkbox" id="checkbox91" checked="" type="checkbox">
													 <label for="checkbox91">Cruising</label>
												</div>
											</li>
										</ul>
									</div>
					</div>
				</div>
				<div class="thirdorder clearfix type-Boat">
					<div class="boatowner">
						<h3 class="offer-boat-heading">Type of Boat</h3>
					</div>
					<hr>
						<div class="shedule-time pad-15 clearfix">
										<div class="col-sm-5 noppading">
										<div class="col-sm-4 noppading right-ttxt-inputs">
											<div class="date1">
												<div class="img-wid">
													<img src="images/boat-tripimg.png">
												</div>
												<p>Select Boat</p>
											</div>
										</div>
										<div class="right-ttxt-inputs">
											<div class="col-sm-12 noppading">
												<div class="date2">
													<select>
													    <option>Select Your Boat</option>
													    <option>2</option>
													    <option>3</option>
													    <option>4</option>
													</select>

												</div>
											</div>
										</div>
										</div>
										<div class="col-sm-7 noppading wid-set">
											<div class="col-sm-4 noppading right-ttxt-inputs">
											<div class="date1">
												<p>Number Of seats Max</p>
											</div>
										</div>
										<div class="right-ttxt-inputs">
											<div class="col-sm-12 noppading">
												<div class="date2">
													<select>
													    <option>Enter Seats</option>
													    <option>2</option>
													    <option>3</option>
													    <option>4</option>
													</select>

												</div>
											</div>
										</div>
										</div>
										<div class="price-confr-boat funkyradio">
											<div class="col-sm-5 noppading">
										<div class="col-sm-4 noppading right-ttxt-inputs">
											<div class="date1">
												<div class="img-wid">
													<!--img src="images/boat-tripimg.png"-->
												</div>
												<p>Price</p>
											</div>
										</div>
										<div class="right-ttxt-inputs pprice-right">
											<div class="col-sm-12 noppading">
												<div class="date2">
													<input type="text" placeholder="Enter Trip Price / Person">
												</div>
												<div class="posi-right-alignment">
													<div class="at-bgg">OR</div>
													<div class="funkyradio-danger">
														<input name="checkbox" id="checkbox89" checked="" type="checkbox">
														 <label for="checkbox89">Fishing</label>
													</div>
												</div>
											</div> 
										</div>
										</div>
										</div>
						</div>
				</div>
				<div class="thirdorder clearfix">
					<div class="boatowner">
						<h3 class="offer-boat-heading">Description of Offer</h3>
					</div>
					<hr>
					<div class="shedule-time pad-15 clearfix">
									<div class="fott-inputs-round">
										<textarea rows="5" placeholder="If you describe your trip well, it will be easier to find new passengers. Please enter as many details as possible"></textarea>
										<p class="max-word">max 500 word</p>
									</div>
					</div>
				</div>
				</form>
				<button class="savebutton pull-right">Publish The Offer</button>
			</div>
		</div>
	</div>
</section>