<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<style>
@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
#rating-error { position: absolute; margin-top: 3%; }
</style>
<?php if(@$data['rating']){ ?>
<script type="text/javascript">
  var value = "<?php echo @$data['rating']; ?>";  
  setTimeout(function(){
        $("input[name=rating][value='"+value+"']").attr('checked', 'checked');
    }, 10);
</script>
<?php
} ?>
<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text,textarea"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addBook']").validate({
    // Specify validation rules
    rules: {
      user_id: "required",
      book_id: "required",
      rating: "required",
      description: "required",
    },
    // Specify validation error messages
    messages: {
      user_id: "User name is required.",
      book_id: "Book name is required",
      rating: "Book name is required",
      description: "Book description is required.",
    },
    submitHandler: function(form) {
        form.submit();
    }
  });
}); 

</script>
 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add Review') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <a href="<?php echo HTTP_ROOT.'admin/reviews'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Back
                    </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT ?>reviews/add" enctype="multipart/form-data" name="addBook" class="m-form m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">

        <div class="form-group m-form__group">  
          <div class="input select required">
            <label for="category_id">Select User</label> 
            <select class="form-control m-input m-input--square" name="user_id">
              <option value="">Select User</option>
              <?php foreach ($Users as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?php if($key==@$data['user_id']){ echo "selected"; } ?>><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>   
        </div>
        <div class="form-group m-form__group">  
          <div class="input select required">
            <label for="category_id">Select Book</label> 
            <select class="form-control m-input m-input--square" name="book_id">
              <option value="">Select Book</option>
              <?php foreach ($Books as $key => $value) { ?>
                <option value="<?php echo $key; ?>" <?php if($key==@$data['book_id']){ echo "selected";} ?> ><?php echo $value; ?></option>
              <?php } ?>
            </select>
          </div>   
        </div>

        <div class="form-group m-form__group" style="padding-bottom: 0">
          <div class="input text required">
            <label for="name">Rating</label>
          </div>  
        </div>
        <div class="form-group m-form__group" style="padding-top: 0; overflow: hidden;">
            <fieldset class="rating">
              <input type="radio" id="star7" name="rating" value="7" /><label class = "full" for="star7" title="Awesome - 7 stars"></label>
              <input type="radio" id="star6half" name="rating" value="6.5" /><label class="half" for="star6half" title="Pretty good - 6.5 stars"></label>
              <input type="radio" id="star6" name="rating" value="6" /><label class = "full" for="star6" title="Awesome - 6 stars"></label>
              <input type="radio" id="star5half" name="rating" value="5.5" /><label class="half" for="star5half" title="Pretty good - 5.5 stars"></label>
              <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
              <input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
              <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
              <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
              <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
              <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
              <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
              <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
              <input type="radio" id="star1" name="rating" value="1" checked="checked" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
              <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset>
        </div>

        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Description</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="description"><?php echo @$data['description']; ?></textarea>
          </div>        
        </div>

               
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
