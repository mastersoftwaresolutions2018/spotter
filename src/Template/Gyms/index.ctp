<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gym[]|\Cake\Collection\CollectionInterface $gyms
 */
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.limit').on('change',function(){
            $('#limit').submit();
        });
    });
</script>
<div class="users index large-9 medium-8 columns content">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Manage Gyms
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">           
            <i class="fas fa-dumbbell" style="font-size: 24px">  </i> &nbsp;&nbsp; 
            <i style="font-size: 18px"><?= $this->Paginator->counter('{{count}}'); ?></i>  Gyms 
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Upload CSV</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Upload Gyms using CSV</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form action="<?php echo HTTP_ROOT;?>/admin/gyms/uploadCsv/" method="POST" role="form">
                                            <div class="form-group">
                                                <label for="">Select a csv file</label>
                                                <input type="file" class="form-control" id="" placeholder="Input field">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="https://web.staging02.com/spotter/gyms.csv">Download Sample CSV</a>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('name', 'Name'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('owner', 'Gym Owner'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('location', 'Location'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
                <?php if($gyms->count() > 0):?>
                <?php $i=0; foreach ($gyms as $gym): ?>
                <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= $this->Number->format($gym->id) ?></span>
                    </td>
                    <td class="m-datatable__cell">
                        <span style="width: 110px;"><?= h($gym->name) ?></span>
                    </td>
                    <td class="m-datatable__cell"><span style="width: 110px;"><?= h($gym->owner) ?></span></td>
                    <td class="m-datatable__cell"><span style="width: 110px;"><?= h($gym->location) ?></span></td>            
                    <td class="m-datatable__cell"><span style="width: 110px;">
                        <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $gym->id), array('escape' => false)) ?>
                            <a href="<?php echo HTTP_ROOT.'admin/gyms/delete/'.$gym->id; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a>
                            </span>
                        </td>  
                    </tr>
                <?php $i++; endforeach; ?> 
                <?php else:?>
                    <tr>
                    <td class="m-datatable__cell">
                    There are no record.
                        </td>
                    </tr>
                    
                <?php endif;?>
                </tbody>
            </table>
            <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                <?php if($gyms->count() > 0):?>
                    <ul class="m-datatable__pager-nav">
                        <?= $this->Paginator->prev('< ' . __('Previous')); ?>
                        <?= $this->Paginator->numbers(); ?>
                        <?= $this->Paginator->next(__('Next') . ' >'); ?>
                    </ul>
                <?php endif;?>
                <div class="m-datatable__pager-info">
                    <div class="btn-group bootstrap-select m-datatable__pager-size" style="width: 70px;">
                         <?php echo $this->Paginator->limitControl(); ?>
                    </div>
                    <span class="m-datatable__pager-detail"><?= $this->Paginator->counter('Displaying {{start}} - {{end}} of {{count}} records'); ?></span>
                </div>
            </div>
        </div>
        <!--end: Datatable -->
    </div>
</div>
</div>











