<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Gym $gym
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gym'), ['action' => 'edit', $gym->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gym'), ['action' => 'delete', $gym->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gym->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gyms'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gym'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gyms view large-9 medium-8 columns content">
    <h3><?= h($gym->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($gym->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Owner') ?></th>
            <td><?= h($gym->owner) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cover Photo') ?></th>
            <td><?= h($gym->cover_photo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Long') ?></th>
            <td><?= h($gym->long) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lat') ?></th>
            <td><?= h($gym->lat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Location') ?></th>
            <td><?= h($gym->location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= h($gym->country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($gym->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($gym->is_deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rating') ?></th>
            <td><?= $this->Number->format($gym->rating) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Review Count') ?></th>
            <td><?= $this->Number->format($gym->review_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($gym->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($gym->description)); ?>
    </div>
</div>
