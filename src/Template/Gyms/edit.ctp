<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script> -->


 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Edit Gym') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                    <a href="<?php echo HTTP_ROOT.'admin/gyms'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Back
                    </a>
                </h3>
            </div> 
    </div>
</div>
<!--begin::Form-->

    <div class="m-portlet__body">
        <div class="gyms form large-9 medium-8 columns content">
    <?= $this->Form->create($gym) ?>
    <fieldset>
        <legend><?= __('Edit Gym') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('owner');
            echo $this->Form->control('description');
            echo $this->Form->control('cover_photo');
            echo $this->Form->control('long');
            echo $this->Form->control('lat');
            echo $this->Form->control('location');
            echo $this->Form->control('country');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('rating');
            echo $this->Form->control('review_count');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
    </div>

<!--end::Form-->
</div>
</div>
