<!DOCTYPE html>
<html lang="en">
<head>
  <title>Kiboat</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/style.css">  
  <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">   
  <link href="<?php echo $this->request->webroot; ?>css/font-awesome.min" rel="stylesheet">
  <link href="<?php echo $this->request->webroot; ?>css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <!--link rel="stylesheet" href="css/slick.css"-->
  <!--link rel="stylesheet" href="css/slick-theme.css"-->
  <!--link rel="stylesheet" href="css/animate.css"-->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
  <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>css/bootstrap-responsive-tabs.css">

  

   <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>


  <script src="<?php echo $this->request->webroot; ?>js/jquery.min.js"></script>
 <script src="<?php echo $this->request->webroot; ?>js/bootstrap.min.js"></script>
   <script src='//ksylvest.github.io/jquery-growl/javascripts/jquery.growl.js' type='text/javascript'></script><!-- Latest compiled and minified CSS -->
 
<!-- Optional theme -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>


 <script src="<?php echo $this->request->webroot; ?>js/wow.min.js"></script>
 <script src="<?php echo $this->request->webroot; ?>js/custom.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <script src="<?php echo $this->request->webroot; ?>js/modernizr.js"></script>
 <script src="<?php echo $this->request->webroot; ?>js/jquery.bootstrap-responsive-tabs.min.js"></script>
 <script type="text/javascript" src="<?php echo $this->request->webroot; ?>js/slick.min.js"></script>

</head>
<body>
<section class="navigation">
	<div class="container-fluid">
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>                        
		  </button>
		  <a class="navbar-brand" href="<?php echo $this->request->webroot; ?>">
		  	<!-- <img src="<?php echo $this->request->webroot; ?>images/logo.png" class="img-responsive"> -->
			  FITNESS
		  </a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		  <!--ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Projects</a></li>
			<li><a href="#">Contact</a></li>
		  </ul-->
		   <!--button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button-->

		  <ul class="nav navbar-nav navbar-right">
			<!-- <li class="sign"><a href="signup.html">sign up</a></li> -->


<?php

$auth= $this->request->session()->read('Auth');
 

if ($this->request->session()->read('Auth')) { ?>


	<li class="sign" > <?=   $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));?>  </li>

<!-- 4 Dec harun-->
			
			<!-- <li><a href="#"><button class="find-boat">Find A Boat</button></a></li> -->

			<!-- 4 Dec harun-->

 <?php }else{ ?>



			<li class="sign" ><?= $this->Html->link('Register',['controller'=>'users','action'=>'fregister']);?> </li>
			<li class="sign"><a href="#" data-toggle="modal" data-target="#login">Log in</a></li>


<?php }

?>


 



			<li><?= $this->Html->link('Find A Boat',['controller'=>'Search','action'=>'searchboat'],['class'=>'find-boat']);?></li>
<!-- 			<li><a href="#"><button class="offer-boat">offer A Boat</button></a></li>
 -->						<li><?= $this->Html->link('offer A Boat',['controller'=>'Search','action'=>'offerboat'],['class'=>'offer-boat']);?></li>

			<li class="dropdown sig"><button class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="<?php echo $this->request->webroot; ?>images/flag1.png"><span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="#">Engligh</a></li>
					<li><a href="#">Hindi</a></li>
					<li><a href="#">Bengali</a></li>
					<li><a href="#">Gujarati</a></li>
					<li><a href="#">Kannada</a></li>
					<li><a href="#">Punjabi</a></li>
					<li><a href="#">Sanskrit</a></li>
					<li><a href="#">Telugu</a></li>
					<li><a href="#">Kashmiri</a></li>
				</ul>
			</li>
		  </ul>
		</div>
	  </div>
	</nav>
	</div>
</section>

<?= $this->fetch('content') ?>

<section class="footer-section">
	<div class="container">
		<div class="foter-cotent">
			<div class="col-sm-3">
				<div class="cont1">
					<img src="<?php echo $this->request->webroot; ?>images/logo.png" class="img-responsive">
					<div class="social-cion">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
						</ul>
						<p>Kiboat, 2017 ©</p>
					</div>
				</div>
			</div>
			<div class="col-sm-9 noppading">
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Using Kiboat</h4>
						<ul>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Trust & Safety</a></li>
							<li><a href="#">Experience levels</a></li>
							<li><a href="#">Ratings</a></li>
							<li><a href="#">Ladies only</a></li>
							<li><a href="#">Member's Agreement</a></li>
							<li><a href="#">Insurance</a></li>
							<li><a href="#">Frequently Asked Questions</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Our company</h4>
						<ul>
							<li><a href="#">About Us</a></li>
							<li><a href="#">KiboatLife</a></li>
							<li><a href="#">Press</a></li>
							<li><a href="#">Partners</a></li>
							<li><a href="#">We're Hiring!</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Destinations</h4>
						<ul>
							<li><a href="#">Sylt</a></li>
							<li><a href="#">Bay of Lübeck</a></li>
							<li><a href="#">Usedom</a></li>
							<li><a href="#">St. Peter-Ording</a></li>
							<li><a href="#">Amrum</a></li>
							<li><a href="#">Hiddensee</a></li>
							<li><a href="#">Juist</a></li>
							<li><a href="#">Rügen</a></li>
							<li><a href="#">Rostock</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="cont1">
						<h4>Legal</h4>
						<ul>
							<li><a href="#">Terms & Conditions</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Cookies Policy</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="copuright">
					<p class="text-left">Language <span>English</span></p>
					<p class="text-right">Additional insurance does not replace driver's mandatory Boat insurance. Conditions apply.</p>
				</div>
			</div>
		</div>
	</div>
</section>
 
 





  <div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content clearfix">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        	<div class="login-popup">
        		<div class="col-sm-5 noppading">
        			<div class="bg-img">
        				<div class="bg-contnt-m">
	        				<p>Not a member yet?</p>
	        				<!-- <button class="signbtn">Sign-up in 30 seconds</button> -->

	        				<?php echo $this->Html->link("Sign-up in 30 seconds", array('controller' => 'users','action'=> 'fregister'), array( 'class' => 'signbtn')); ?>

        				</div>
        			</div>
        		</div>
        		<div class="col-sm-7 noppading">
                        <form id="formlogin"  role="form">


	        			<div class="content-right">
	        				<h1>login</h1>
	        				<hr>

 
	        				<!-- <a class="btn" href="<?php echo $this->request->webroot; ?>users/login?provider=Facebook"><button class="signbtn-blue btn" >Connect With Facebook</button></a> -->
<button class="signbtn-blue btn" onclick="window.location.href='<?php echo $this->request->webroot; ?>users/login?provider=Facebook'">Connect With Facebook</button>





 
 	        				<div class="response-msg"></div>
	        			
	        					<div class="orimg"><img src="<?php echo $this->request->webroot; ?>images/orimg.png" class="img-responsive"></div> 

	        			    <div class="group">      
						      <input name="username" type="text" required>
						      <span class="highlight"></span>
						      <span class="bar"></span>
						      <label>Username</label>
						    </div>
						    <div class="group">      
						      <input name="password" type="password" required>
						      <span class="highlight"></span>
						      <span class="bar"></span>
						      <label>Passwors</label>
						    </div>
 						    <div class="col-sm-12 noppading">
 						    <p class="remembertext">Remember Me


						    <?php echo $this->Form->checkbox('remember_me'); ?></p>


						   
						    </div>
						    <button class="signbtn">secure Login</button>
						    <p class="frgt">I've forgotten my password</p>
						</div>
        			</form>
        		</div>
        	</div>
      </div>      
    </div>
  </div>
     <div class="modal fade" id="shareride" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content clearfix">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        	<div class="sharehide-popup">
        		<div class="links-head">
        			<h1>Sharing is caring</h1>
        		</div>
        		<div class="pad-lef">
        			<p>Copy and paste this  link into your website <a href="#">Learn More</a></p>
        			<div class="inputs-links">
        				<input type="text" placeholder="https://www.kiboat.com/&!%Profile&%Silvia-d1678h6" class="ab1">
        				<button class="copylink">Copy Link</button>
        			</div>
        		</div>
        		<hr>
        		<div class="pad-lef">
        			<p>Share with friends Via Email</p>
        			<div class="inputs-links">
        				<input type="text" placeholder="Enter email address" class="ab2">
        				<button class="copylink">Send</button>
        			</div>
        		</div>
        		<hr>
        		<div class="pad-lef">
        			<p>Share this on your favorite social networks</p>
        			<div class="social-icos clearfix">
        				<ul>
        					<li class="text-drkblue"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-blue"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-drkblue"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-red"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        					<li class="text-bluegr"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <span class="check-li"><i class="fa fa-check" aria-hidden="true"></i></span></li>
        				</ul>
        			</div>
        		</div>
        		<div class="pad-lef bottmbtnss">
        			<button class="sharenowt">Share Now</button>
        			<button class="cancle">cancel</button>
        		</div>
        	</div>       	
      </div>      
    </div>
  </div>
 
</body>
</html>
  <script>

/*$('#example1').datepicker({
	autoclose: true,  
	format: "yyyy-mm-dd"
});*/
       </script> 

      <script>
    $(document).ready(function() {
         $("#formlogin").validate({

            rules: {
                username: "required",
                password: "required"
                 
            },
            messages: {
                username: "Please enter your username",
                password: "Please enter your password"
                
            }  ,
              submitHandler: function (event) {
                var fdata = $('#formlogin').serializeArray();
                console.log(fdata);
            $.ajax({
				type: 'post',
				url : '<?php echo $this->request->webroot; ?>users/login?ajax=1',
				data: fdata,
                success: function (responseData) {
					if(responseData.result == "success") {
						if (responseData.returnurl=='fdasboard') {
							//alert('fdasboard');
						window.location.href = '<?php echo $this->request->webroot; ?>profile/dashboard';
						}else{
							//alert('user')
							window.location.href = '<?php echo $this->request->webroot; ?>profile/dashboard';
						//window.location.href = '<?php echo $this->request->webroot; ?>users/index';
						}
					} 
					else if(responseData.result === "error") {
					$(".response-msg").html(responseData.result);
					}
                },
                error: function (responseData) {
                	 $(".response-msg").html(responseData.result);
                             
                    console.log('Ajax request not recieved!');
                }
            });
 		  }
        });
        $("#form1").validate({
 
            rules: {
                firstname: "required",
                lastname: "required",

                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },

            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",

                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
            }  ,

              submitHandler: function (event) {
            var fdata = $('#form1').serializeArray();

            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>users/fregister?ajax=1',
                data: fdata,
                success: function (responseData) {
                	 $(".response-msg").html(responseData.result);
                	 if (responseData.result=='success') {
                	 	 window.location.href = '<?php echo $this->request->webroot; ?>users/index';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.result);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });
        $("#profileadd").validate({
 
            rules: {
                name: "required",
                modal: "required",
                year: "required",
                type: "required",
                seats_max: "required",
				pic: {
					required: true,
					extension: "jpg|jpeg|png|ico|bmp"
				},

            },
            messages: {
                name: "Please enter your name",
                modal: "Please enter your modal",
                year: "Please enter your modal",
                type: "Please enter your type",
                seats_max: "Please enter your seats",
                 pic: {
					required: "Please upload file.",
					extension: "Please upload file in these format only (jpg, jpeg, png, ico, bmp)."
        		}
             }  ,

              submitHandler: function (event) {
 

				var fd = new FormData();
				var file_data = $('input[type="file"]')[0].files; // for multiple files

				for(var i = 0;i<file_data.length;i++){
				fd.append("file_"+i, file_data[i]);
				}

				var other_data = $('#profileadd').serializeArray();
				$.each(other_data,function(key,input){
				fd.append(input.name,input.value);
				});

            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>profile/saveData',
                data: fd,
               processData: false, //prevent jQuery from converting your FormData into a string
                contentType: false,  
                 success: function (responseData) {
                	 $(".response-msg").html(responseData.message);
                	 if (responseData.result=='success') {
                	  window.location.href = '<?php echo $this->request->webroot; ?>profile/add';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.message);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });

        $("#profileupdate").validate({
 
            rules: {
                firstname: "required",
                lastname: "required",
                phone: "required",
                email: {
                    required: true,
                    email: true
                },

            },
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                 phone: "Please enter your phone",
                email: "Please enter a valid email address",
            }  ,

              submitHandler: function (event) {
            var fdata = $('#profileupdate').serializeArray();
 
            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>users/edit?id=',
                data: fdata,
                success: function (responseData) {
                	 $(".response-msg").html(responseData.result);
                	 alert(responseData.result);
                	 if (responseData.result=='success') {
                	  window.location.href = '<?php echo $this->request->webroot; ?>profile/index';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.result);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });

$("#form_verificationemail").validate({
 
            rules: {
 
                email: {
                    required: true,
                    email: true
                },

            },
            messages: {
                email: "Please enter a valid email address",
            }  ,

              submitHandler: function (event) {
            var fdata = $('#form_verificationemail').serializeArray();
 
            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>users/edit?id=',
                data: fdata,
                success: function (responseData) {
                	 $(".response-msg").html(responseData.result);
                	 alert(responseData.result);
                	 if (responseData.result=='success') {
                	  window.location.href = '<?php echo $this->request->webroot; ?>profile/verification';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.result);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });


$("#form_verificationphone").validate({
 
            rules: {
 
                phone: {
                    required: true
                 },

            },
            messages: {
                phone: "Please enter a valid phone address",
            }  ,

              submitHandler: function (event) {
            var fdata = $('#form_verificationphone').serializeArray();
 
            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>users/edit?id=',
                data: fdata,
                success: function (responseData) {
                	 $(".response-msg").html(responseData.result);
                	 alert(responseData.result);
                	 if (responseData.result=='success') {
                	  window.location.href = '<?php echo $this->request->webroot; ?>profile/verification';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.result);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });



        

        $("#profileup").validate({
 
            rules: {
                
				profilepic: {
					required: true,
					extension: "jpg|jpeg|png|ico|bmp"
				},

            },
            messages: {
                
                 profilepic: {
					required: "Please upload file.",
					extension: "Please upload file in these format only (jpg, jpeg, png, ico, bmp)."
        		}
             }  ,

              submitHandler: function (event) {
 

				var fd = new FormData();
				var file_data = $('input[type="file"]')[0].files; // for multiple files
				
				for(var i = 0;i<file_data.length;i++){
				fd.append("file_"+i, file_data[i]);
				}

				 

            $.ajax({
                 type: 'post',
                 url : '<?php echo $this->request->webroot; ?>profile/profpic',
                data: fd,
               processData: false, //prevent jQuery from converting your FormData into a string
                contentType: false,  
                 success: function (responseData) {
                	 $(".response-msg").html(responseData.message);
                	 if (responseData.result=='success') {
                	  window.location.href = '<?php echo $this->request->webroot; ?>profile/profpic';
                	 } 
                },
                error: function (responseData) {
                	$(".response-msg").html(responseData.message);
                    console.log('Ajax request not recieved!');
                }
            });
 			}
        });
    });
   </script>
   <script type="text/javascript">
	function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#uploaded_image').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});


</script>

   <script type="text/javascript">
	function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#uploaded_image').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});


</script>


<script>
$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $(".header").addClass("active");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".header").removeClass("active");
    }
});
</script>
<script>
$('.eco-mindness ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
<script>
$('.tab-navigation ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
 <script>
/*var slider = document.getElementById("myRange");
var output = document.getElementById("bars1");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}*/
</script>
 <script>
/*var slider = document.getElementById("myRange1");
var output = document.getElementById("bars2");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}*/
</script>
<script>
$('.list-profile-left ul li a').click(function() {
    $('ul li.active').removeClass('active');
    $(this).closest('li').addClass('active');
});
</script>
 
<script>
$('.responsive-tabs').responsiveTabs({
  accordionOn: ['xs', 'sm']
});
</script> 

