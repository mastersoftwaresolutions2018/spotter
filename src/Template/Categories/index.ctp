<script type="text/javascript">
    $(document).ready(function(){
        $('.limit').on('change',function(){
            $('#limit').submit();
        });
    });
</script>
<div class="users index large-9 medium-8 columns content">
    

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Categories
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools"> 
        <i class="fa fa-book" style="font-size: 24px">  </i> &nbsp;&nbsp; 
            <i style="font-size: 18px"><?= $this->Paginator->counter('{{count}}'); ?></i>  Categories           
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <!-- <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <form class="m-input-icon m-input-icon--left" method="get" action="<?php echo HTTP_ROOT.'categories/index' ?>" style="float: left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch" name="query" value="<?php echo @$_GET['query'] ?>" required>
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <button type="submit" class="serch"><i class="la la-search"></i></button>
                                    </span>
                                </form>
                                <a href="<?php echo HTTP_ROOT.'admin/categories' ?>" style="position: absolute; padding-left: 5%">
                                    <button type="button" class="btn btn-primary">
                                        Reset
                                    </button>
                                </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="<?php echo HTTP_ROOT.'admin/categories/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">                    
                    Add Categories
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div> -->


            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <form method="get" action="<?php echo HTTP_ROOT.'categories/index' ?>">
                    <div class="form-group m-form__group row align-items-center">
                        
                        <div class="col-md-4">
                            <div class="m-form__group m-form__group--inline">
                                <!-- <div class="m-form__label">
                                    <label class="m-label m-label--single">
                                    Search Users:
                                    </label>
                                </div> -->
                                <div class="m-form__control">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch" name="query" value="<?php echo @$_GET['query'] ?>" required>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <button type="submit" class="btn btn-success">
                                    Filter
                                </button>
                                <a href="<?php echo HTTP_ROOT.'admin/categories' ?>" style="position: absolute; padding-left: 5%">
                                <button type="button" class="btn btn-primary">
                                        Reset
                                    </button> </a>
                                
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>                     
                        
                        </div>
                        <div class="col-md-4 order-1 order-xl-2 m--align-right">
                            <a href="<?php echo HTTP_ROOT.'admin/categories/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            Add Category
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>                 
                    </form>

                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <!-- <th title="Field #1" class="m-datatable__cell m-datatable__cell--sort" data-field="
                            Order ID
                            "><span style="width: 110px;">
                            Category ID
                            </span>
                        </th> -->
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('name', 'Name'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('status', 'Status'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>                        
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
            <?php
            if(count($categories)){
                $i = 1;
                foreach ($categories as $category){ ?>

                    <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo $i; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo h($category->name); ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php if($category->status == 1){ echo '<span class="m-badge  m-badge--success m-badge--wide">Enable</span>'; }else{ echo '<span class="m-badge  m-badge--primary m-badge--wide">Disable</span>';} ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            <?php /*echo $this->Html->link(__('View'), ['action' => 'view', $category->id],['class' => 'btn btn-default'])*/ ?>
                            
                            <?php /*echo $this->Html->link(__('<i class="fa fa-edit"></i>'), ['action' => 'edit', $category->id],['class' => 'btn btn-default'])*/ ?>
                            <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $category->id), array('escape' => false)) ?>
                            
                            <?php /*echo $this->Html->link(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete', $category->id],['confirm' => __('Are you sure you want to delete?', $category->id),'class'=>'btn btn-default'])*/ ?>
                            <?php /*echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-trash-o')), array('action' => 'delete', $category->id), array('escape' => false),['confirm' => __('Are you sure you want to delete?', $category->id),'class'=>'btn btn-default'])*/ ?>
                            <a href="<?php echo HTTP_ROOT.'admin/categories/delete/'.$category->id; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a>
                            </span>
                        </td>                      
                    </tr>
    <?php       $i++;
                }
            }else{ ?>
                <tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
                        <td data-field="
                            Order ID
                            " class="m-datatable__cell"><span style="width: 100%;">
                            No category found.
                            </span>
                        </td>
                
                </tr>
    <?php   } ?>      
                </tbody>
            </table>
            <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                <ul class="m-datatable__pager-nav">
                    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
                    <?= $this->Paginator->numbers(); ?>
                    <?= $this->Paginator->next(__('Next') . ' >'); ?>
                </ul>
                <div class="m-datatable__pager-info">
                    <div class="btn-group bootstrap-select m-datatable__pager-size" style="width: 70px;">
                                              
                        <!-- <form method="get" action="<?php echo HTTP_ROOT.'admin/categories/index' ?>" id="limit">
                        <select class="selectpicker m-datatable__pager-size limit" title="Select page size" data-width="70px" tabindex="-98" name="limit" id="dropdown" data-selected="<?php if(@$_GET['limit']){ echo $_GET['limit'];}else{ echo "10"; } ?>">
                            <option value="10" <?php if(@$_GET['limit'] == 10){ echo "selected";} ?> >10</option>
                            <option value="20" <?php if(@$_GET['limit'] == 20){ echo "selected";} ?> >20</option>
                            <option value="30" <?php if(@$_GET['limit'] == 30){ echo "selected";} ?> >30</option>
                            <option value="50" <?php if(@$_GET['limit'] == 50){ echo "selected";} ?> >50</option>
                            <option value="100" <?php if(@$_GET['limit'] == 100){ echo "selected";} ?> >100</option>
                        </select>
                        </form> -->
                        <?php echo $this->Paginator->limitControl(); ?>
                    </div>


                    <span class="m-datatable__pager-detail"><?= $this->Paginator->counter('Displaying {{start}} - {{end}} of {{count}} records'); ?></span>
                </div>
            </div>
        </div>
        <!--end: Datatable -->
    </div>
</div>
<?php /* ?>
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next(__('Next') . ' >'); ?>

  </ul>

  
    <p><?= $this->Paginator->counter(); ?></p>
</nav>
<?php */ ?>

</div>
