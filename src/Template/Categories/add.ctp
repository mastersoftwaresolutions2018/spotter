 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
    $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addCategory']").validate({
    // Specify validation rules
    rules: {
      name: "required",
      description: "required",
    },
    // Specify validation error messages
    messages: {
      name: "Please enter Category name.",
      description: "Please enter category description.",
    },
    submitHandler: function(form) {
        var name    = $("#name").val();
        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/categories/getifnameExist1' ?>",
           data: { name: name}, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#name-error").length == 0){ 
                        $("#name").after('<div id="name-error" class="form-control-feedback" style="color:#f4516c">Category Name alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#name-error").html("Category Name alreay exists. Please choose another.");
                    }
                }else{
                    form.submit();
                }
            }
        });
    }
  });
}); 

</script>
 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add Category') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
            <h3 class="m-portlet__head-text">
               <a href="<?php echo HTTP_ROOT.'admin/categories'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <?php echo __('Back') ?>
               </a>
            </h3>
        </div> 
    </div>
</div> 
<!--begin::Form-->
  <form method="post" action="<?php echo HTTP_ROOT ?>categories/add" enctype="multipart/form-data" name="addCategory" class="m-form m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">
        <div class="form-group m-form__group">  
          <div class="input text required">
            <label for="name">Name</label> 
            <input type="text" name="name" class="form-control m-input m-input--square" id="name" value="<?php echo @$data['name']; ?>">
          </div>   
        </div>
        <div class="form-group m-form__group">
          <div class="input text required">
            <label for="name">Description</label>   
            <textarea rows="5" class ='form-control m-input m-input--square' name="description"><?php echo @$data['description']; ?></textarea>
          </div>        
        </div>
        <div class="form-group m-form__group">
          <label for="name">Image</label>
            <input type="file" class ='form-control m-input m-input--square' name="category_image">
        </div>
        <div class="form-group m-form__group">
          <div class="input select">
            <label for="name">Status</label>
            <select class ='form-control m-input m-input--square' name="status">
              <option value="0">Disable</option>
              <option value="1">Enable</option>
            </select>
          </div>
        </div>        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
  </form>
<!--end::Form-->
</div>


    
</div>
