 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='editCategory']").validate({
    // Specify validation rules
    rules: {
      name: "required",
      description: "required",
    },
    // Specify validation error messages
    messages: {
      name: "Please enter Category name.",
      description: "Please enter category description.",
    },
    submitHandler: function(form) {
        var id      = "<?php echo @$category['id']; ?>";
        var name    = $("#name").val();

        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/categories/getifnameExistTwice1' ?>",
           data: { name: name, id : id}, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#name-error").length == 0){ 
                        $("#name").after('<div id="name-error" class="form-control-feedback" style="color:#f4516c">Category Name alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#name-error").html("Category Name alreay exists. Please choose another.");
                    }
                }else{
                    form.submit();
                }
            }
        });
    }
  });
}); 

</script>
 <div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Edit Category') ?> 
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
            <h3 class="m-portlet__head-text">
               <a href="<?php echo HTTP_ROOT.'admin/categories'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <?php echo __('Back') ?>
               </a>
            </h3>
        </div> 
    </div>
</div>
<!--begin::Form-->
<div class="portlet-body form">
    <form method="post" action="<?php echo HTTP_ROOT.'admin/categories/edit/'.@$category['id'] ?>" enctype="multipart/form-data" name="editCategory" class="m-form m-form--fit m-form--label-align-right">
        <div class="m-portlet__body">
            <input type="hidden" name="id" value="<?php echo @$category['id']; ?>">
            <div class="form-group m-form__group">
                <div class="input text">
                    <label for="name">Name</label>
                    <input name="name" class="form-control m-input m-input--square" id="name" type="text" value="<?php if(@$data['name']){ echo @$data['name']; }else{ echo @$category['name']; } ?>">
                </div>
            </div>

            <div class="form-group m-form__group">
                <div class="input text">
                    <label for="name">Description</label>
                    <textarea rows="5" class="form-control m-input m-input--square" name="description" value="<?php if(@$data['description']){ echo @$data['description']; }else{ echo @$category['description']; } ?>"><?php if(@$data['description']){ echo @$data['description']; }else{ echo @$category['description']; } ?></textarea>
                </div>
            </div>
            <div class="form-group m-form__group">
                <img src="<?php echo HTTP_ROOT.''.@$category['category_image']; ?>" style="max-width: 150px; max-height: 150px;">
            </div>
            <div class="form-group m-form__group">
                <div class="input text">
                    <label for="name">Category Image</label>
                    <input type="file" name="category_image">
                </div>
            </div>
            
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                
                <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
            </div>
        </div>
    </form>
</div>
<!--end::Form-->
</div>


    
</div>
