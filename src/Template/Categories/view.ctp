 
<div class="users view large-9 medium-8 columns content">
    
<div class="m-portlet">
    
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                     
                     <thead>
                     <tbody>
                        <tr>
                            <th><?= __('Name') ?></th>
                            <td><?php echo $category->name; ?>
                            </td>
                        </tr>


                        <tr>
                            <th><?= __('Description') ?></th>
                            <td><?= $category->description; ?></td> 
                        </tr>


                        <tr>
                            <th><?= __('Status') ?></th>
                            <td><?php if($category->status == 1){ echo "Enable"; }else{ echo "Disable";} ?></td>  
                        </tr>                        

                        <tr>
                            <th><?= __('Category Image') ?></th>
                            <td><img src="<?php echo HTTP_ROOT."webroot/".$category->category_image; ?>"></td>
                        </tr>
                        
                        
                    </tbody>
                    </thead>    
                        
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>
</div>
