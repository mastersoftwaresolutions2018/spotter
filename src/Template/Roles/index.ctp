
<div class="users index large-9 medium-8 columns content">

    <div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Roles
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">            
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                            <form method="get" action="<?php echo HTTP_ROOT.'roles/index' ?>" style="float: left">
                                    <input type="text" class="form-control m-input m-input--solid" placeholder="Search Roles" id="generalSearch" name="query" value="<?php echo @$_GET['query'] ?>" required>
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                        <button type="submit" style="border: none; position: absolute; margin-top: -25px"><i class="la la-search"></i></button>  
                                        </span>
                                    </span>
                                </form>
                                <a href="<?php echo HTTP_ROOT.'admin/roles' ?>" style="position: absolute; padding-left: 5%">
                                <button type="button" class="btn btn-primary">
                                        Reset
                                    </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="<?php echo HTTP_ROOT.'admin/roles/add'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                    Add Group
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <table class="m-datatable__table" id="m-datatable--244818357975" width="100%" style="display: block; position: static; zoom: 1; height: auto; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="height: 53px;">
                        <th class="m-datatable__cell m-datatable__cell--sort" >
                            <span style="width: 110px;">
                            <?php  echo $this->Paginator->sort('id', 'Sr. No.'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                                <?php  echo $this->Paginator->sort('name', 'Name'); ?>
                            </span>
                        </th>
                        <th class="m-datatable__cell m-datatable__cell--sort">
                            <span style="width: 110px;">
                            Actions
                            </span>
                        </th>                        
                    </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
            <?php
            if(count($roles)){
                $i = 1;
                foreach ($roles as $role){ ?>

                    <tr data-row="0" class="m-datatable__row <?php if ($i % 2 == 0) { echo "m-datatable__row--even"; } ?> " style="height: 64px;">
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo $i; ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell">
                            <span style="width: 110px;">
                                <?php echo h($role->name); ?>
                            </span>
                        </td>
                        <td class="m-datatable__cell"><span style="width: 110px;">
                            
                            <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('action' => 'edit', $role->id), array('escape' => false)) ?>
                           
                            <!-- <a href="<?php //echo HTTP_ROOT.'admin/roles/delete/'.$group->id; ?>"><i class="fa fa-trash-o" onclick="return confirm('Are you sure you want to delete?')"></i></a> -->
                            </span>
                        </td>                      
                    </tr>
    <?php       $i++;
                }
            }else{ ?>
                <tr data-row="0" class="m-datatable__row m-datatable__row--even" style="height: 64px;">
                        <td data-field="
                            Order ID
                            " class="m-datatable__cell"><span style="width: 100%;">
                            No role found.
                            </span>
                        </td>
                
                </tr>
    <?php   } ?>      
                </tbody>
            </table>
            <div class="m-datatable__pager m-datatable--paging-loaded clearfix">
                <ul class="m-datatable__pager-nav">
                    <?= $this->Paginator->prev('< ' . __('Previous')); ?>
                    <?= $this->Paginator->numbers(); ?>
                    <?= $this->Paginator->next(__('Next') . ' >'); ?>
                </ul>
                <div class="m-datatable__pager-info">
                    <div class="btn-group bootstrap-select m-datatable__pager-size" style="width: 70px;">                      
                        <?php echo $this->Paginator->limitControl(); ?>
                    </div>
                    <span class="m-datatable__pager-detail"><?= $this->Paginator->counter('Displaying {{start}} - {{end}} of {{count}} records'); ?></span>
                </div>
            </div>
        </div>
        <!--end: Datatable -->
    </div>
</div>

</div>

