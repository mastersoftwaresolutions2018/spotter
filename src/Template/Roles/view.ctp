 <div class="roles view large-9 medium-8 columns content">
  <div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                   <?= h($role->name) ?>
                </h3>
            </div>
            <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <a href="<?php echo HTTP_ROOT.'admin/roles'; ?>">
                    <?php echo __('Back') ?>
                   </a>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                     
                     <thead>
                     <tbody>
                      <tr>
                            <th><?= __('Group') ?></th>
                            <td><?= $role->has('group') ? $this->Html->link($role->group->name, ['controller' => 'Groups', 'action' => 'view', $role->group->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Name') ?></th>
                            <td><?= h($role->name) ?></td>
                        </tr>
                        
                        
                        
                    </tbody>
                    </thead>    
                        
                </table>






            </div>
        </div>
        <!--end::Section-->
    </div>
</div> 

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= __('Related Users') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
            <?php if (!empty($role->users)): ?>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            
                            <th><?= __('Group Id') ?></th>
                            <th><?= __('Role Id') ?></th>
                            <th><?= __('Username') ?></th>
                            <th><?= __('Email') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
              <?php foreach ($role->users as $users): ?>
            <tr>
                
                <td><?= h($users->group_id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <td>
                    
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id],['class' => 'btn btn-default']) ?>
                  
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id],['class' => 'btn btn-default']) ?>
                  
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id),'class' => 'btn btn-default']) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
                        
                        
                    </tbody>
                </table>
            <?php endif; ?>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>





</div>
