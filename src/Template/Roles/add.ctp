<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script type="text/javascript">
  // Wait for the DOM to be ready
$(function() {
  $('input[type="text"]').change(function(){
        this.value = $.trim(this.value);
    });
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='addRole']").validate({
    // Specify validation rules
    rules: {
      group_id: "required",
      name: "required",
    },
    // Specify validation error messages
    messages: {
      group_id: "Group is required.",
      name: "Name is required",      
    },
    submitHandler: function(form) {
       /* var username    = $("#username").val();

        $.ajax({
           type: "POST",
           url: "<?php echo HTTP_ROOT.'admin/users/getifusernameexist1' ?>",
           data: { username: username}, // serializes the form's elements.
           success: function(response)
            {
                if(response > 0){
                    if($("#username-error").length == 0){ 
                        $("#username").after('<div id="username-error" class="form-control-feedback" style="color:#f4516c">Username alreay exists. Please choose another.</div>');
                    }else{                   
                        $("#username-error").html("Username alreay exists. Please choose another.");
                    }
                }else{*/
                    form.submit();
               /* }*/
    }
  });
}); 

</script>
<div class="users form large-9 medium-8 columns content">

<div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                <?= __('Add Role') ?>
            </h3>
        </div>
        <div class="m-portlet__head-title" style="float: right"> 
            <h3 class="m-portlet__head-text">
               <a href="<?php echo HTTP_ROOT.'admin/roles'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <?php echo __('Back') ?>
               </a>
            </h3>
        </div>
    </div>
</div>
<!--begin::Form-->

<?= $this->Form->create($role,['class'=>'m-form m-form--fit m-form--label-align-right', 'name'=> 'addRole', 'id'=>'addRole']) ?>
    <div class="m-portlet__body">

        <div class="form-group m-form__group">
            
            <?php 
            echo $this->Form->input('group_id', ['options' => $groups,'empty' => true,'class'=>'form-control m-input m-input--square','id'=>'group_id']);
            ?>
            
        </div>       

        <div class="form-group m-form__group">
            
            <?php echo $this->Form->input('name',['class' => 'form-control m-input m-input--square','required'=>false]);?> 
            
        </div>
        
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            
            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
        </div>
    </div>
 <?= $this->Form->end() ?>
<!--end::Form-->
</div>


    
</div>
