<div class="groups view large-9 medium-8 columns content">
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                   <?= h($group->name) ?>
                </h3>
            </div>
            <div class="m-portlet__head-title" style="float: right"> 
                <h3 class="m-portlet__head-text">
                   <a href="<?php echo HTTP_ROOT.'admin/groups'; ?>">
                    <?php echo __('Back') ?>
                   </a>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered table-hover">
                     
                     <thead>
                     <tbody>
                        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($group->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($group->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($group->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($group->modified) ?></td>
        </tr>
                    </tbody>
                    </thead>    
                        
                </table>






            </div>
        </div>
        <!--end::Section-->
    </div>





</div>

<div class="m-portlet">
<div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                   <?= __('Related Roles') ?>
                </h3>
            </div>
        </div>
    </div>

<div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">

            <?php if (!empty($group->roles)): ?>
                <table class="table table-bordered table-hover">
                     
                     <thead>
                     <tbody>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Group Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th ><?= __('Actions') ?></th>
                    </tr>
        <?php foreach ($group->roles as $roles): ?>
            <tr>
                <td><?= h($roles->id) ?></td>
                <td><?= h($roles->group_id) ?></td>
                <td><?= h($roles->name) ?></td>
                <td><?= h($roles->created) ?></td>
                <td><?= h($roles->modified) ?></td>
                <td>
                    
                    <?= $this->Html->link(__('View'), ['controller' => 'Roles', 'action' => 'view', $roles->id],['class' => 'btn btn-default']) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Roles', 'action' => 'edit', $roles->id],['class' => 'btn btn-default']) ?>
                   
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Roles', 'action' => 'delete', $roles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $roles->id),'class'=>'btn btn-default']) ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
                    </tbody>
                    </thead>    
                        
                </table>
 <?php endif; ?>





            </div>
        </div>
        <!--end::Section-->
    </div>
  </div> 


<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= __('Related Users') ?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
            <?php if (!empty($group->users)): ?>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            
                            <th><?= __('Group Id') ?></th>
                            <th><?= __('Role Id') ?></th>
                            <th><?= __('Username') ?></th>
                            <th><?= __('Email') ?></th>
                            <th><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                <?php foreach ($group->users as $users): ?>
            <tr>
              
                <td><?= h($users->group_id) ?></td>
                <td><?= h($users->role_id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <td class="actions">
                
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id],['class' => 'btn btn-default']) ?>
                
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id],['class' => 'btn btn-default']) ?>
               
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id),'class' => 'btn btn-default']) ?>
               
                </td>
            </tr>
            <?php endforeach; ?>
                        
                        
                    </tbody>
                </table>
            <?php endif; ?>
            </div>
        </div>
        <!--end::Section-->
    </div>
    <!--end::Form-->
</div>





</div>