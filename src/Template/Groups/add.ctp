 
<div class="groups form large-9 medium-8 columns content">
    
    <div class="m-portlet m-portlet--tab">
<div class="m-portlet__head">
<div class="m-portlet__head-caption">
	<div class="m-portlet__head-title">
		<span class="m-portlet__head-icon m--hide">
			<i class="la la-gear"></i>
		</span>
		<h3 class="m-portlet__head-text">
			<?= __('Add Group') ?>
		</h3>
	</div>
	<div class="m-portlet__head-title" style="float: right"> 
        <h3 class="m-portlet__head-text">
           <a href="<?php echo HTTP_ROOT.'admin/groups'; ?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
            <?php echo __('Back') ?>
           </a>
        </h3>
    </div> 
</div>
</div>
<!--begin::Form-->

<?= $this->Form->create($group,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
<div class="m-portlet__body">
	<div class="form-group m-form__group">
		
		<?php echo $this->Form->input('name',['class' => 'form-control m-input m-input--square']);?>
		
	</div>
	
	
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
	<div class="m-form__actions">
		<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-metal'));?>
	
	</div>
</div>
<?= $this->Form->end() ?>
<!--end::Form-->
</div>
</div>
