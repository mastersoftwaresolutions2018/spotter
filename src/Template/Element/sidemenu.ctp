<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
	<li class="m-menu__item">
		<?= $this->Html->link(
				'<i class="m-menu__link-icon flaticon-users">
					<span></span>
				</i><span class="m-menu__link-text">
					Admin
				</span>', 
				['controller' => 'Users','action' => 'updateAdmin', 'plugin' => false],
				[ 'escape' => false, "class"=>"m-menu__link "]) 
		?>
	</li>
	<li class="m-menu__section">
		<h4 class="m-menu__section-text">
			Components
		</h4>
		<i class="m-menu__section-icon flaticon-more-v3"></i>
	</li>
	
	<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
		<a  href="#" class="m-menu__link m-menu__toggle">
			<i class="m-menu__link-icon flaticon-share"></i>
			<span class="m-menu__link-text">
				All Users
			</span>
			<i class="m-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="m-menu__submenu">
			<span class="m-menu__arrow"></span>
			<ul class="m-menu__subnav">
				<li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							All Users
						</span>', 
						['controller' => 'Users','action' => 'index', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li>

				<li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							Add User
						</span>', 
						['controller' => 'Users','action' => 'add', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li>
				
			</ul>
		</div>
	</li>
	
	<!-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
		<a  href="#" class="m-menu__link m-menu__toggle">
			<i class="m-menu__link-icon flaticon-share"></i>
			<span class="m-menu__link-text">
				Gyms
			</span>
			<i class="m-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="m-menu__submenu">
			<span class="m-menu__arrow"></span>
			<ul class="m-menu__subnav">
				<li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							All Gyms
						</span>', 
						['controller' => 'Gyms','action' => 'index', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li>

				<li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							Add New Gym
						</span>', 
						['controller' => 'Gyms','action' => 'add', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li>
			</ul>
		</div>
	</li> -->

	<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
		<a  href="#" class="m-menu__link m-menu__toggle">
			<i class="m-menu__link-icon flaticon-share"></i>
			<span class="m-menu__link-text">
				Configuration
			</span>
			<i class="m-menu__ver-arrow la la-angle-right"></i>
		</a>
		<div class="m-menu__submenu">
			<span class="m-menu__arrow"></span>
			<ul class="m-menu__subnav">
				<li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							Notification Configuration
						</span>', 
						['controller' => 'CoreConfigs','action' => 'add', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li>

				<!-- <li class="m-menu__item " aria-haspopup="true" >
				<?= $this->Html->link(
						'<i class="m-menu__link-bullet m-menu__link-bullet--dot">
							<span></span>
						</i><span class="m-menu__link-text">
							Add New Gym
						</span>', 
						['controller' => 'Gyms','action' => 'add', 'plugin' => false],
						[ 'escape' => false, "class"=>"m-menu__link "]) 
				?>
				</li> -->
			</ul>
		</div>
	</li>

	<li class="m-menu__item" aria-haspopup="true">
		<?= $this->Html->link(
				'<i class="m-menu__link-icon flaticon-logout">
					<span></span>
				</i><span class="m-menu__link-text">
					Logout
				</span>', 
				['controller' => 'Users','action' => 'logout', 'plugin' => false],
				[ 'escape' => false, "class"=>"m-menu__link "]) 
		?>
	</li>
</ul>