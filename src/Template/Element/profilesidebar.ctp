<?php $profilepic = $userdata['profilepic'];
 $username = $userdata['username'];
 $action = $this->request->params['action'];
$controller = $this->request->params['controller'];
?>
<div class="col-sm-3">
						<div class="profile-left-side">
							<div class="profile-imgblock">
								<img src="<?php echo $this->request->webroot; ?>user/<?php echo  $profilepic; ?>" class="owner-img img-responsive">
								<h3><?php echo $username ;?></h3>
								<p>St. Peter-Ording, Rendsburg, Germany</p>
							</div>
							<div class="list-profile-left">
								<ul>
									<p>Profile information</p>
									<li <?php if($action == 'index'){ ?> class="active" <?php } ?> ><?php echo $this->Html->link('Personal information', array('controller'=>'profile', 'action'=>'index'));?>  </li>
									<li <?php if($action == 'profpic'){ ?> class="active" <?php } ?> ><?php echo $this->Html->link('Photo', array('controller'=>'profile', 'action'=>'profpic'));?></li>
  									<li <?php if($action == 'verification'){ ?> class="active" <?php } ?> ><?php echo $this->Html->link('Verification', array('controller'=>'profile', 'action'=>'verification'));?></li>
									<li <?php if($action == 'add'){ ?> class="active" <?php } ?> ><?php echo $this->Html->link('Boat', array('controller'=>'profile', 'action'=>'add'));?></li>
								</ul>
								<ul>
									<p>Ratings</p>
									<li><a href="rating-received.html">Ratings received</a></li>
									<li><a href="rating-given.html">Ratings given</a></li>
								</ul>
								<ul>
									<p>Account</p>
									<li><a href="payment-status.html">Payment Status</a></li>
									<li><a href="notification.html">Notifications</a></li>
									<li><a href="password-setting.html">Password</a></li>
									<li><a href="close-account.html">Close my account</a></li>
								</ul>
							</div>
						</div>
					</div>