<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Firebase Phone Authentication</title>
 


<!-- <script src="https://www.gstatic.com/firebasejs/4.7.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDrzscGUSuNrOzHuN76PEnMtkYCgJ2SSAM",
    authDomain: "kiboatq.firebaseapp.com",
    databaseURL: "https://kiboatq.firebaseio.com",
    projectId: "kiboatq",
    storageBucket: "kiboatq.appspot.com",
    messagingSenderId: "1044855113879"
  };
  firebase.initializeApp(config);
</script> -->

<script src="https://www.gstatic.com/firebasejs/4.7.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCesB5Z1mQ4l6Xbn5_jLT7G8q8vsj9Bys8",
    authDomain: "kiboatnew.firebaseapp.com",
    databaseURL: "https://kiboatnew.firebaseio.com",
    projectId: "kiboatnew",
    storageBucket: "",
    messagingSenderId: "15431953369"
  };
  firebase.initializeApp(config);
</script>



  <script src="https://cdn.firebase.com/libs/firebaseui/2.3.0/firebaseui.js"></script>
  <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/2.3.0/firebaseui.css" />
  <link href="<?php echo $this->request->webroot; ?>css/style.css" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>
    <div id="container">
    <div class="hidecontainer">
      <h3> Phone Number Auth.</h3>
      <div id="loading">Loading...</div>
      <div id="loaded" class="hidden2">
        <div id="main">
          <div id="user-signed-in" class="hidden2">
            <div id="user-info">
              <div id="photo-container">
                <img id="photo">
              </div>
              <div id="name"></div>
              <div id="email"></div>
              <div id="phone"></div>
              <div class="clearfix"></div>
            </div>
            <p>
              <button id="sign-out">Sign Out</button>
              <button id="delete-account">Delete account</button>
            </p>
          </div>
          <div id="user-signed-out" class="hidden2">
            <h4>You are signed out.</h4>
            <div id="firebaseui-spa">
              <h3>Single Page App mode:</h3>
              <div id="firebaseui-container"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <script src="<?php echo $this->request->webroot; ?>js/app.js"></script>
</body>
</html>