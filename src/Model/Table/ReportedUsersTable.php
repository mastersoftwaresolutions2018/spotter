<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ReportedUsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('reported_users');
        $this->displayField('reason');
        $this->primaryKey('id');


        $this->belongsTo('Reporters', [ 
           'className' => 'Users', 
           'foreignKey' => 'reported_by_id',
            'propertyName' => 'reporter'
        ]);

        $this->belongsTo('Reporteds', [ 
            'className' => 'Users', 
            'foreignKey' => 'reported_user_id', 
            'propertyName' => 'reported'
        ]);

    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('reported_by_id', 'create')
                ->notEmpty('reported_by_id');
        $validator
                ->requirePresence('reported_user_id', 'create')
                ->notEmpty('reported_user_id');
        $validator
                ->requirePresence('reason', 'create')
                ->notEmpty('reason');
        $validator
                ->requirePresence('description', 'create')
                ->notEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['reported_user_id'], 'Reporteds'));
        $rules->add($rules->existsIn(['reported_by_id'], 'Reporters'));
        return $rules;
    }

}
