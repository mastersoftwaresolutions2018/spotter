<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SharedBooksTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('shared_books');
        $this->displayField('id');
        $this->primaryKey('id');


        $this->belongsTo('Books', [
            'foreignKey' => 'book_id'
        ]);
        $this->belongsTo('tousers', [ 
           'className' => 'Users', 
           'foreignKey' => 'to_user_id',
            'propertyName' => 'to'
        ]);

        $this->belongsTo('Fromusers', [ 
            'className' => 'Users', 
            'foreignKey' => 'from_user_id', 
            'propertyName' => 'from'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['book_id'], 'Books'));
        $rules->add($rules->existsIn(['to_user_id'], 'tousers'));
        $rules->add($rules->existsIn(['from_user_id'], 'Fromusers'));
        $rules->add($rules->isUnique(
                    ['to_user_id', 'from_user_id', 'book_id'],
                    'This book already shared with this user.'
                ));
        return $rules;
    }

}
