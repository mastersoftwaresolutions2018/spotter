<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Gyms Model
 *
 * @method \App\Model\Entity\Gym get($primaryKey, $options = [])
 * @method \App\Model\Entity\Gym newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Gym[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Gym|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Gym patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Gym[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Gym findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class GymsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
       

        $this->setTable('gyms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        // $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('owner')
            ->maxLength('owner', 255)
            ->requirePresence('owner', 'create')
            ->notEmpty('owner');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        // $validator
        //     ->scalar('cover_photo')
        //     ->maxLength('cover_photo', 255)
        //     ->requirePresence('cover_photo', 'create')
        //     ->notEmpty('cover_photo');

        // $validator
        //     ->scalar('long')
        //     ->maxLength('long', 255)
        //     ->requirePresence('long', 'create')
        //     ->notEmpty('long');

        // $validator
        //     ->scalar('lat')
        //     ->maxLength('lat', 255)
        //     ->requirePresence('lat', 'create')
        //     ->notEmpty('lat');

        // $validator
        //     ->scalar('location')
        //     ->maxLength('location', 255)
        //     ->requirePresence('location', 'create')
        //     ->notEmpty('location');

        // $validator
        //     ->scalar('country')
        //     ->maxLength('country', 255)
        //     ->requirePresence('country', 'create')
        //     ->notEmpty('country');

        // $validator
        //     ->integer('is_deleted')
        //     ->requirePresence('is_deleted', 'create')
        //     ->notEmpty('is_deleted');

        // $validator
        //     ->numeric('rating')
        //     ->requirePresence('rating', 'create')
        //     ->notEmpty('rating');

        // $validator
        //     ->integer('review_count')
        //     ->requirePresence('review_count', 'create')
        //     ->notEmpty('review_count');

        return $validator;
    }
}
