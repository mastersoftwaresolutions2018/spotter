<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CoreConfigs Model
 *
 * @property \App\Model\Table\ConfigsTable|\Cake\ORM\Association\BelongsTo $Configs
 *
 * @method \App\Model\Entity\CoreConfig get($primaryKey, $options = [])
 * @method \App\Model\Entity\CoreConfig newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CoreConfig[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CoreConfig|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CoreConfig patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CoreConfig[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CoreConfig findOrCreate($search, callable $callback = null, $options = [])
 */
class CoreConfigsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('core_configs');
        //$this->setDisplayField('id');
        $this->setPrimaryKey('config_id');

       
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
/*
        $validator
            ->scalar('config_data')
            ->requirePresence('config_data', 'create')
            ->notEmpty('config_data');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    /*public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['config_id'], 'Configs'));

        return $rules;
    }*/
}
