<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;


/**
 * Book Entity
 *
 * @property int $id
 * @property int $group_id
 * @property int $role_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\Role $role
 */
class Book extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    /*protected $_virtual = [
        'ReviewCount'
    ];   

    protected function _getReviewCount()
    {
        $reviews = TableRegistry::get('Reviews');
        $query = $reviews->find()->where(['book_id'=> $this->_properties['id']])->count();
        return $query;
    } */
 
}
