<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CoreConfig Entity
 *
 * @property int $id
 * @property string $config_id
 * @property string $config_data
 *
 * @property \App\Model\Entity\Config $config
 */
class CoreConfig extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'config_id' => true,
        'config_data' => true        
    ];
}
