<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Gym Entity
 *
 * @property int $id
 * @property string $name
 * @property string $owner
 * @property string $description
 * @property string $cover_photo
 * @property string $long
 * @property string $lat
 * @property string $location
 * @property string $country
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $created
 * @property float $rating
 * @property int $review_count
 */
class Gym extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'owner' => true,
        'description' => true,
        'cover_photo' => true,
        'long' => true,
        'lat' => true,
        'location' => true,
        'country' => true,
        'is_deleted' => true,
        'created' => true,
        'rating' => true,
        'review_count' => true
    ];
}
