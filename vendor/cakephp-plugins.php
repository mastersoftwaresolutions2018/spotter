<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'ADmad/JwtAuth' => $baseDir . '/vendor/admad/cakephp-jwt-auth/',
        'Acl' => $baseDir . '/vendor/cakephp/acl/',
        'AclManager' => $baseDir . '/vendor/ivanamat/cakephp3-aclmanager/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Crud' => $baseDir . '/vendor/friendsofcake/crud/',
        'CrudJsonApi' => $baseDir . '/vendor/friendsofcake/crud-json-api/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Documents' => $baseDir . '/vendor/ivanamat/cakephp3-documents/',
        'Markdown' => $baseDir . '/vendor/ivanamat/cakephp3-markdown/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];